r"""
Functions for generating and counting dessins d'enfants

EXAMPLES::

    sage: from dessins import passports_of_degree_and_genus
    sage: list(passports_of_degree_and_genus(5, 2))
    [[[5], [5], [5]]]
    sage: from dessins import count_constellations_of_degree
    sage: count_constellations_of_degree(6)
    Counter({1: 70, 0: 63, 2: 16})

AUTHORS:

- Jacob Bond: initial implementation
"""

import itertools as it
from collections import Counter
from typing import Optional

# isort: split
########## Sage imports ##########
from sage.arith.misc import divisors, euler_phi, factor
from sage.combinat.partition import Partitions
from sage.functions.other import factorial, floor
from sage.libs.gap.libgap import libgap
from sage.misc.all import prod

################################

# isort: split
########## this package ##########
from .constellations.generation import passport_to_constellations
from .passport import Passport
from .utils import coerce_perm, generic_permutation

##################################


def passports_of_degree_and_genus(deg, gen, as_passports=True):
    r"""
    Creates an iterator consisting of passports of a given degree and genus.

    INPUT:

    - ``deg`` -- Integer; the degree of the passports in the iterator

    - ``gen`` -- Integer; the genus of the passports in the iterator

    - ``as_passports`` -- ``bool``; whether to return the passports as
      :class:`.Passport` objects instead of as tuples

    OUTPUT: an iterator of passports with the given degree and genus

    EXAMPLES::

        sage: from dessins import passports_of_degree_and_genus
        sage: list(passports_of_degree_and_genus(5, 2))
        [[[5], [5], [5]]]
        sage: list(passports_of_degree_and_genus(6, 2))
        [[[3, 3], [6], [6]], [[2, 4], [6], [6]], [[1, 5], [6], [6]]]
        sage: sum(p.num_constellations() for p in passports_of_degree_and_genus(6, 2))
        16
    """
    triples = passports_of_degree(deg, as_passports=as_passports)
    if as_passports is False:
        return (triple for triple in triples if passport_genus(triple, deg) == gen)
    else:
        return (
            Passport(triple) for triple in triples if passport_genus(triple, deg) == gen
        )


def passports_of_degree(deg, as_passports=True):
    r"""
    Creates an iterator consisting of passports of a given degree.

    INPUT:

    - ``deg`` -- Integer; the degree of the passports in the iterator

    - ``as_passports`` -- ``bool``; whether to return the passports as
      :class:`.Passport` objects instead of as tuples

    OUTPUT: an iterator of passports with the given degree

    EXAMPLES::

        sage: from dessins import passports_of_degree
        sage: list(passports_of_degree(3))
        [[[1, 1, 1], [3], [3]], [[1, 2], [1, 2], [3]], [[3], [3], [3]]]
        sage: sum(p.num_constellations() for p in passports_of_degree(6))
        149
    """
    # reversed ensures that long cycles will be in t_inf;
    # also, count_constellations seems to run faster when reversed is used
    triples = it.combinations_with_replacement(reversed(Partitions(deg)), 3)
    if as_passports is False:
        return (triple for triple in triples if nonnegative_integral_genus(triple, deg))
    else:
        return (
            Passport(triple)
            for triple in triples
            if nonnegative_integral_genus(triple, deg)
        )


def nonnegative_integral_genus(passport, deg):
    r"""
    Determines whether the given passport would result in a dessin with a genus that is
    a nonnegative integer.
    """
    genus = passport_genus(passport, deg)
    return genus > -1 / 2 and floor(genus) == genus


def passport_genus(passport, deg):
    r"""
    Returns the genus of the given ``passport``, assuming a degree of ``deg``.
    """
    return (sum(map(len, passport)) - deg - 2) / (-2)


def count_constellations_of_degree_and_genus(deg, gen):
    r"""
    Counts the number of 3-constellations with a given degree and genus.

    INPUT:

    - ``deg`` -- Integer; the degree of the 3-constellations being considered

    - ``gen`` -- Integer; the genus of the 3-constellations being considered

    OUTPUT: the number of 3-constellations with degree ``deg`` and genus ``gen``

    EXAMPLES::

        sage: from dessins import count_constellations_of_degree_and_genus
        sage: count_constellations_of_degree_and_genus(3, 1)
        1
        sage: count_constellations_of_degree_and_genus(6, 2)
        16
    """
    return sum(
        1
        for triple in passports_of_degree_and_genus(deg, gen, as_passports=False)
        for _ in passport_to_constellations(*triple, as_tuple=True)
    )


# fixing a genus and filtering inside passports_of_degree_and_genus
# seems to be faster than getting all passports with passports_of_degree
# and then calculating the passport's genus
def count_constellations_of_degree(deg):
    r"""
    Counts the number of 3-constellations of each possible genus for a given degree.

    INPUT:

    - ``deg`` -- Integer; the degree of the 3-constellations being considered

    OUTPUT: a ``Counter`` object containing the the number of 3-constellations of each
    possible genus for degree ``deg``

    EXAMPLES::

        sage: from dessins import count_constellations_of_degree
        sage: count_constellations_of_degree(4)
        Counter({0: 6, 1: 2})
        sage: count_constellations_of_degree(6)
        Counter({1: 70, 0: 63, 2: 16})
        sage: sum( count_constellations_of_degree(6).values() )
        149
    """
    return Counter(
        genus
        for genus in range(floor((deg - 1) / 2) + 1)
        for triple in passports_of_degree_and_genus(deg, genus, as_passports=False)
        for _ in passport_to_constellations(*triple, as_tuple=True)
    )


##############################################
#                                            #
#  Counting Dessins with Passport [n, n, n]  #
#                                            #
##############################################


def conj_class_position(t, sym_size=None, G=None):
    if sym_size is None and G is None:
        sym_size = sum(t)
    if G is None:
        G = libgap.SymmetricGroup(sym_size)
    cycle_t = generic_permutation(t).CycleStructurePerm()
    has_type_t = libgap.eval(
        "f := x->CycleStructurePerm(Representative(x))=" + str(cycle_t)
    )
    return G.ConjugacyClasses().PositionsProperty(has_type_t)


def class_mult_coeff_sn(type1, type2, type3, sym_size=None, G=None, T=None):
    if sym_size is None and G is None:
        sym_size = max(map(sum, [type1, type2, type3]))
    if G is None:
        G = libgap.SymmetricGroup(sym_size)
    if T is None:
        T = G.CharacterTable()
    num_classes = T.ConjugacyClasses().Length()
    T_classes = T.IdentificationOfConjugacyClasses()
    pos1, pos2, pos3 = [conj_class_position(t, G=G)[0] for t in [type1, type2, type3]]
    if T_classes != libgap.eval("[1.." + str(num_classes) + "]"):
        pos1, pos2, pos3 = [T_classes.Position(pos) for pos in [pos1, pos2, pos3]]
    return T.ClassMultiplicationCoefficient(pos1, pos2, pos3)


def class_multiplication_coefficient(
    g1, g2, g3, sym_size: Optional[int] = None, G=None, T=None
) -> int:
    r"""
    Returns the class multiplication coefficient for the conjugacy classes containing
    ``g1``, ``g2``, and ``g3``.

    See `<https://docs.gap-system.org/doc/ref/chap71.html#X7E2EA9FE7D3062D3>`_ for more information.

    INPUT:

    - ``g1`` -- ``GapElement_Permutation``; a representative of the first
      conjugacy class

    - ``g2`` -- ``GapElement_Permutation``; a representative of the second
      conjugacy class

    - ``g3`` -- ``GapElement_Permutation``; a representative of the third
      conjugacy class

    - ``sym_size`` -- ``Optional[int]`` (default: ``None``); the size of the
      ``SymmetricGroup`` being considered.  If none of ``g1``, ``g2``, or ``g3``
      move the integer ``sym_size``, this is required in order to return the
      correct result.

    - ``G`` -- ``Optional[Group]`` (default: ``None``); the group the class
      multiplication coefficient is being computed for.  This is required if the
      group being considered is not a ``SymmetricGroup``.

    - ``T`` -- ``Optional[CharacterTable]`` (default: ``None``); a precomputed
      character table to use in the computation.  When computing several class
      multiplication coefficients for the same group, it can be more efficient
      to compute the character table once and provide it to ``class_multiplication_coefficient``,
      rather than recomputing the character table every time.

    OUTPUT: the class multiplication coefficient for the conjugacy classes represented
    by ``g1``, ``g2``, and ``g3``

    EXAMPLES::

        sage: from dessins import class_multiplication_coefficient
        sage: g1, g2, g3 = (libgap.eval(p) for p in ["(1,2,3)", "(1,2)", "(2,3)"])
        sage: class_multiplication_coefficient(g1, g2, g3)
        2
        sage: import itertools as it
        sage: G = libgap.SymmetricGroup(4)
        sage: g = libgap.eval("(1,2,3,4)")
        sage: T = G.CharacterTable()
        sage: perms = it.product(G.ConjugacyClass(g).List(), repeat=3)
        sage: set(class_multiplication_coefficient(*triple, T=T) for triple in perms)
        {0}
    """
    g1, g2, g3 = (coerce_perm(g, "gap") for g in (g1, g2, g3))
    if sym_size is None and G is None:
        sym_size = max(g.LargestMovedPoint() for g in (g1, g2, g3))
    if G is None:
        G = libgap.SymmetricGroup(sym_size)
    if T is None:
        T = G.CharacterTable()
    classes = T.ConjugacyClasses()
    pos1, pos2, pos3 = (classes.Position(G.ConjugacyClass(g)) for g in [g1, g2, g3])
    return T.ClassMultiplicationCoefficient(pos1, pos2, pos3)


def num_dessins_type_nnn(m: int) -> int:
    r"""
    Returns the number of dessins with passport [n, n, n].

    EXAMPLES::

        sage: from dessins import num_dessins_type_nnn
        sage: num_dessins_type_nnn(3)
        1
        sage: num_dessins_type_nnn(4)
        0
        sage: num_dessins_type_nnn(5)
        4

    REFERENCES::

        [Bon2024] J. Bond, On the number of totally ramified dessins d'enfants. 2024.
    """
    if m % 2 == 0:
        return 0
    out = sum(
        euler_phi(m / d)
        * schemmel_phi(m / d)
        * (m / d) ** (d - 1)
        * factorial(d - 1)
        / (d + 1)
        for d in divisors(m)
    ) / (m / 2)
    return int(out)


def schemmel_phi(n: int) -> int:
    r"""
    Returns the number of pairs of consecutive integers relatively prime to ``n``.

    INPUT:

    - ``n`` -- Integer; upper bound on the integers being considered

    OUTPUT: the number of pairs of consecutive integers relatively prime to ``n``

    EXAMPLES::

        sage: from dessins import schemmel_phi
        sage: schemmel_phi(1)
        1
        sage: schemmel_phi(5)
        3
        sage: schemmel_phi(12)
        0
        sage: schemmel_phi(27)
        9
        sage: schemmel_phi(0)
        Traceback (most recent call last):
        ...
        ArithmeticError: factorization of 0 is not defined

    REFERENCES:

        [Sch1869] V. Schemmel, "Ueber relative Primzahlen", Journal für die reine und angewandte Mathematik, Vol. 70 (1869), pp. 191-192.

        [OEIS2024] OEIS Foundation Inc. (2024), Pairs of integers relatively prime to n, Entry A058026 in The On-Line Encyclopedia of Integer Sequences, https://oeis.org/A058026
    """
    factors = list(factor(n))
    return prod(p ** (e - 1) * (p - 2) for p, e in factors)
