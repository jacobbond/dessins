r"""
Functions for computing the extended monodromy group and other related groups for a dynamical Belyi map

EXAMPLES::

    sage: from dessins import BelyiMap, Constellation3
    sage: beta = BelyiMap(x**3)
    sage: gamma = BelyiMap(3*x**2 - 2*x**3)
    sage: emon_beta = beta.extended_monodromy()
    sage: mon_beta_gamma = emon_beta.apply_to_extended_monodromy(
    ....:     gamma.monodromy_representation()
    ....: )
    sage: mon_beta_gamma.StructureDescription()
    "(S3 x S3 x S3) : C3"

AUTHORS:

- Jacob Bond: initial implementation
"""

from numbers import Number
from typing import Optional

import numpy as np

# check if this is being imported by a Sage or Python script
import sys  # isort:skip

if ".py" in sys.argv[0] or ".sage" in sys.argv[0]:
    import sage.all  # noqa: F401

# isort: split
########## Sage imports ##########
from sage.misc.sage_eval import sage_eval

##################################

# isort: split
########## this package ##########
from .belyi_map import BelyiMap
from .monodromy import ID, A, B, loops_from_odes

##################################


def extending_pattern(
    beta: BelyiMap,
    pts_per_path: Number = 10,
    dynamical_check: bool = True,
    algorithm: str = "odes",
    cache: bool = True,
):
    r"""
    Computes the extending pattern of :code:`beta`.

    INPUT:

    - ``beta`` -- :class:`BelyiMap<.belyi_map.BelyiMap>`; a dynamical Belyi map

    - ``pts_per_path`` -- :code:`Number`; the number of points to compute along each loop lifting

    - ``dynamical_check`` -- :code:`bool`; whether to check if :code:`beta` is a dynamical Belyi map

    - ``algorithm`` -- :code:`str` (default: :code:`"odes"`); the algorithm to use for computing the loop liftings, either using differential equations with :code:`zvode` or by stepping between nearby preimages.  See :func:`.monodromy.monodromy` for more information.

    - ``cache`` -- :code:`bool` (default: :code:`True`); whether to use any cached values for monodromy or extending pattern

    OUTPUT: the extending pattern :code:`(f_{0}, f_{1})` of :code:`beta`, given as lists :code:`f`
      so that :code:`f[i] =`:math:`f(i)`

    EXAMPLES::

        sage: from dessins import extending_pattern, BelyiMap
        sage: beta = BelyiMap(x**3)
        sage: beta.edge_labelling()
        {(-0.396850262992049900 - 0.687364818499301300*I,): 0,
         (-0.396850262992049900 + 0.687364818499301300*I,): 1,
         (0.79370052598409970,): 2}
        sage: # specify a particular edge labelling to ensure doctest is consistent:
        sage: beta.relabel_edges({0: 2, 2: 0})
        sage: extending_pattern(beta)
        ([<identity ...>, a, <identity ...>], [b, <identity ...>, <identity ...>])
        sage: xi = BelyiMap(27*x**2*(x-1)**2/(4*(x**2-x+1)**3))
        sage: xi.edge_labelling()
        {(-2.7320508075688770,): 0,
         (-0.366025403784438600,): 1,
         (0.26794919243112270,): 2,
         (0.73205080756887730,): 3,
         (1.366025403784439,): 4,
         (3.732050807568877,): 5}
        sage: xi.relabel_edges({0: 4, 1: 5, 2: 0, 3: 1, 4: 2, 5: 3})
        sage: extending_pattern(xi, algorithm="preimages")
        ([<identity ...>, b, <identity ...>, b^-1*a^-1, <identity ...>, a], [<identity ...>, <identity ...>, <identity ...>, <identity ...>, <identity ...>, <identity ...>])
    """
    if cache is False and hasattr(beta, "_extending_pattern"):
        del beta._extending_pattern

    try:
        return beta._extending_pattern
    except AttributeError:
        pass

    if dynamical_check and (beta.genus != 0 or not beta.is_dynamical()):
        raise TypeError("Belyi map is not dynamical.")

    if cache is False and hasattr(beta, "_loop_lifts"):
        del beta._loop_lifts

    # TODO: there should be a check for whether infinity is on
    # the lifting of the path
    try:
        # use cache of the Belyi map's liftings of loops
        paths0, paths1 = beta._loop_lifts
    except AttributeError:
        # TODO:  add option to use preimages instead of ODEs for computing loops
        (paths0, paths1) = loops_from_odes(beta, pts_per_path)

    preimage_dict = beta.edge_labelling()

    path_dict0 = {tuple(path[0][1]): path for path in paths0}
    path_dict1 = {tuple(path[0][1]): path for path in paths1}

    from .lifting import _pop_nearest_key

    ext_pattern = [[ID] * len(path_dict0), [ID] * len(path_dict0)]

    for pt, label in preimage_dict.items():
        ext_pattern[0][label] *= _path_to_extending_pattern(
            _pop_nearest_key(path_dict0, pt)
        )
        ext_pattern[1][label] *= _path_to_extending_pattern(
            _pop_nearest_key(path_dict1, pt)
        )

    beta._extending_pattern = ExtendingPattern(*ext_pattern, preimage_dict)
    return beta._extending_pattern


def _path_to_extending_pattern(path):  # , crossing_sample_pts=10):
    path_action = ID

    # format for numpy:
    # samples = np.array([(time, complex(value[0])) for time, value in path])
    samples = np.array([complex(value[0]) for _, value in path])
    # path_func = interp1d(*zip(*path), fill_value="extrapolate", axis=0)
    # samples = path_func(np.linspace(0, 1, num=crossing_sample_pts))

    # chop off infinitesimal imaginary parts:
    to_real_indices = np.where(abs(samples.imag) < 10**-10)
    samples[to_real_indices] = samples[to_real_indices].real

    # crossings will give indices in samples where path
    # crosses the real axis; sheet_changes will give indices
    # in crossings where a sheet change occurs
    # samples[i]*samples[i+1];
    crossings = np.where(samples.imag[:-1] * samples.imag[1:] <= 0)[0]
    # index in crossings, so crossings[sheet_changes] gives
    # the indices in samples where a sheet change occurs
    sheet_changes = np.where(
        np.logical_or(samples[crossings].real < 0, samples[crossings].real > 1)
    )[0]
    if len(sheet_changes) == 0:
        sheet_changes = []
    else:
        sheet_changes = crossings[sheet_changes]

    for change in sheet_changes:
        re_ims = [(samples[change + a].real, samples[change + a].imag) for a in [0, 1]]
        real_pair, imag_pair = zip(*re_ims)
        if imag_pair[0] >= 0 and imag_pair[1] < 0:
            if real_pair[0] < 0:
                path_action *= A
            elif real_pair[0] > 1:
                path_action *= B.Inverse()
        elif imag_pair[0] < 0 and imag_pair[1] >= 0:
            if real_pair[0] < 0:
                path_action *= A.Inverse()
            elif real_pair[0] > 1:
                path_action *= B

    return path_action


class ExtendingPattern:
    r"""
    The extending pattern :math:`(f_{0}, f_{1})` of a Belyi map, describing how transitioning
    between edges of a Belyi map causes transitions between sheets over :math:`\mathbb{P}^{1}(\mathbb{C})`
    when the Belyi map is viewed as a covering map.  Each of :code:`around0` and :code:`around1` should be
    specified using should use the generators (and identity) of :math:`F_{2}` imported from :doc:`monodromy`
    or as a string

    INPUT:

    - ``around0`` -- :code:`list[GapElement]`; a function from edges of a Belyi map to :math:`F_{2}` describing transitions
      between sheets of the associated covering map resulting from lifting a loop around 0; given as a list with
      :code:`f0[i] =`:math:`f_{0}(i)`

    - ``around1`` -- :code:`list[GapElement]`; a function from edges of a Belyi map to :math:`F_{2}` describing transitions
      between sheets of the associated covering map resulting from lifting a loop around 0; given as a list with
      :code:`f1[i] =`:math:`f_{1}(i)`

    - ``edge_labelling`` -- :code:`Optional[dict]`; a labelling of the edges of the Belyi map

    EXAMPLES::

        sage: from dessins import ExtendingPattern
        sage: from dessins.belyi_maps.monodromy import A, B, ID
        sage: ext_pat1 = ExtendingPattern(
        ....:     [ID, ID, ID, A, ID], [ID, B, ID, A, A**-1]
        ....: )
        sage: ext_pat2 = ExtendingPattern(
        ....:     "[ID, ID, ID, A, ID]", "[ID, B, ID, A, A**-1]"
        ....: )
        sage: ext_pat1 == ext_pat2
        True
    """

    def __init__(
        self,
        around0: list,
        around1: list,
        # TODO: how is this used?
        edge_labelling: Optional[dict] = None,
    ):
        if isinstance(around0, str):
            self.around0 = sage_eval(around0, {"A": A, "B": B, "ID": ID})
        else:
            self.around0 = around0

        if isinstance(around1, str):
            self.around1 = sage_eval(around1, {"A": A, "B": B, "ID": ID})
        else:
            self.around1 = around1

        self.edge_labelling = edge_labelling

    def __len__(self):
        return 2

    def __eq__(self, other):
        if len(other) == 2 and self[0] == other[0] and self[1] == other[1]:
            return True
        else:
            return False

    def __getitem__(self, idx):
        if idx == 0:
            return self.around0
        elif idx == 1:
            return self.around1
        else:
            raise ValueError("ExtendingPatterns only contain 2 items.")

    def __iter__(self):
        return iter([self.around0, self.around1])

    def __repr__(self):
        return f"{self.around0, self.around1}"

    def apply_monodromy_representation(self, representation):
        r"""
        Postcomposes the group homomorphism :code:`representation` with each
        function in the extending pattern.  This is affected by applying the
        homomorphism to each element of the lists storing the extending
        pattern.

        INPUT:

        - ``representation`` -- GAP Homomorphism; the monodromy representation to postcompose with :code:`self`

        EXAMPLES::

            sage: from dessins import ExtendingPattern
            sage: from dessins.belyi_maps.monodromy import F2, ID, A, B
            sage: ext_pat = ExtendingPattern(
            ....:     [ID, ID, ID, A, ID], [ID, B, ID, A, A**-1]
            ....: )
            sage: G = libgap.eval("Group((1,2,3), ())")
            sage: g, h = G.GeneratorsOfGroup()
            sage: rep = libgap.GroupHomomorphismByImages(F2, G, [A, B], [g, h])
            sage: ext_pat.apply_monodromy_representation(rep)
            ([(), (), (), (1,2,3), ()], [(), (), (), (1,2,3), (1,3,2)])
        """
        return (
            [representation.Image(elt) for elt in self.around0],
            [representation.Image(elt) for elt in self.around1],
        )

    def relabel_edges(self, other):
        new_around0 = [
            self.around0[(idx + 1) ** (other.Inverse()) - 1]
            for idx in range(len(self.around0))
        ]
        new_around1 = [
            self.around1[(idx + 1) ** (other.Inverse()) - 1]
            for idx in range(len(self.around1))
        ]
        self.around0, self.around1 = new_around0, new_around1
