from typing import Optional

import numpy as np

# isort: split
########## Sage imports ##########
from sage.calculus.var import var
from sage.groups.free_group import FreeGroup
from sage.libs.gap.libgap import libgap
from sage.rings.cc import CC
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.structure.element import parent

##################################

# isort: split
########## this package ##########
F2 = FreeGroup(["a", "b"]).gap()
A, B = F2.GeneratorsOfGroup()
ID = F2.One()

##################################


# need to handle when infinity is on a path
def monodromy(self, pts_per_path=10, prec=53, algorithm="odes", cache=True):
    r"""
    Computes the monodromy of the Belyi map by numerically solving
    differential equations.

    INPUT:

    - ``belyi_pair`` -- a Belyi map and a curve (list)

      - the curve should be given as :code:`expression`, where the curve is defined by :code:`expression = 0`

      - in the case of PP1(CC), the curve should be :code:`QQ['x,y'](var('y'))`
        when determining the ODE's, a lexicographic ordering will be
        used on the dependent and independent variables

      - if the curve is omitted, ( monodromy(beta) ), assumes PP1(CC)

    - ``pts_per_path`` -- the number of points to find on each path of the
      monodromy

    EXAMPLES::

        sage: from dessins import BelyiMap
        sage: y = var("y")
        sage: b = BelyiMap(x**2 / (x**2 - 1), y**2 - x**3 + x)
        sage: b.monodromy().canonical()
        ((1,2,3,4), (1,2,3,4), (1,3)(2,4))

    """
    if cache is False and hasattr(self, "_monodromy"):
        del self._monodromy

    try:
        # cache the Belyi map's monodromy
        return self._monodromy
    except AttributeError:
        pass

    if cache is False and hasattr(self, "_loop_lifts"):
        del self._loop_lifts

    # there should be a check for whether infinity is on
    # the lifting of the path
    try:
        # cache the Belyi map's liftings of loops
        paths0, paths1 = self._loop_lifts
    except AttributeError:
        # TODO: allow using preimages for positive genera
        if "preimage" in algorithm and self._genus0:
            try:
                paths0, paths1 = loops_from_preimages(self, pts_per_path)
            except Exception as e:
                if "Singular error" in str(e):
                    print(
                        "Lifting loops using preimages failed with a Singular "
                        "error.  Trying again using differential equations."
                    )
                    paths0, paths1 = loops_from_odes(self, pts_per_path)
                else:
                    raise e
        else:
            paths0, paths1 = loops_from_odes(self, pts_per_path)

        # TODO: move the following code to lifting.py

        self._loop_lifts = (paths0, paths1)

    path_ends0 = {tuple(path.start_point): path.end_point for path in paths0}
    path_ends1 = {tuple(path.start_point): path.end_point for path in paths1}

    preimage_dict = self.edge_labelling()

    # dicts to pop from
    preimage_dict0, preimage_dict1 = preimage_dict.copy(), preimage_dict.copy()

    # lists to hold the permutations
    sigma0, sigma1 = [0] * len(path_ends0), [0] * len(path_ends0)

    from .lifting import _pop_nearest_key

    for pt, label in preimage_dict.items():
        # points in preimage_dict and path_ends can fail to match at high precisions:
        endpt = min(
            path_ends0.keys(),
            key=lambda a: abs(np.linalg.norm(np.array(pt) - np.array(a))),
        )

        sigma0[label] = _pop_nearest_key(preimage_dict0, path_ends0.pop(endpt)) + 1
        sigma1[label] = _pop_nearest_key(preimage_dict1, path_ends1.pop(endpt)) + 1

    from ..constellations.constellation import Constellation3

    self._monodromy = Constellation3(libgap.PermList(sigma0), libgap.PermList(sigma1))
    return self._monodromy


# TODO: standardize the return format from each loops_from function
def loops_from_preimages(self, pts_per_path):
    from sage.symbolic.constants import I, e, pi

    from .lifting import _lift_to_preimages

    paths0 = _lift_to_preimages(
        self,
        e ** (2 * pi * I * var("t")) / 2,
        [0, 1],
        pts_per_path,
    )
    paths1 = _lift_to_preimages(
        self,
        1 - e ** (2 * pi * I * var("t")) / 2,
        [0, 1],
        pts_per_path,
    )
    return paths0, paths1


# TODO: standardize the return format from each loops_from function
def loops_from_odes(self, pts_per_path):
    from .lifting import zvode

    # TODO: handle approximations and ring coercion better
    approx_fun = self.n()
    R = PolynomialRing(CC, self.variables())

    ode_around_0, ode_around_1 = _monodromy_odes(
        approx_fun, [R(self.curve_poly())], self.genus == 0, R.gens()
    )
    ivs = [[0, pt] for pt in self.edge_labelling().keys()]
    paths0 = [zvode(ode_around_0, iv, pts_per_path, time_range=[0, 1]) for iv in ivs]
    paths1 = [zvode(ode_around_1, iv, pts_per_path, time_range=[0, 1]) for iv in ivs]

    # ensure the endpoint of each path is a midpoint of an edge:
    for path in paths0:
        endpt = path.end_point
        edge_pt = min(
            self.edge_labelling().keys(),
            key=lambda a: np.linalg.norm(np.array(a, dtype=np.complex128) - endpt),
        )
        path[-1] = (1, edge_pt)
    for path in paths1:
        endpt = path.end_point
        edge_pt = min(
            self.edge_labelling().keys(),
            key=lambda a: np.linalg.norm(np.array(a, dtype=np.complex128) - endpt),
        )
        path[-1] = (1, edge_pt)

    return paths0, paths1


def _monodromy_odes(belyi_approx, curves_approx, genus0, varis):
    from sage.symbolic.constants import I, pi

    if genus0 is True:
        vari = belyi_approx.numerator().variables()[0]
        exprs = [
            2 * pi * I * (belyi_approx - a) / belyi_approx.derivative() for a in [0, 1]
        ]
        return (
            lambda t, z: [exprs[0](**{str(vari): z[0]})],
            lambda t, z: [exprs[1](**{str(vari): z[0]})],
        )
    else:
        from sage.matrix.constructor import matrix
        from sage.modules.free_module_element import vector

        # TODO: handle approximations and ring coercions better
        R = parent(curves_approx[0]).fraction_field()

        # TODO: is this the right place to convert to numerical?
        # TODO: is there a better way to handle this?
        # R = parent(belyi).change_ring(base_ring=CC).fraction_field()
        # belyi_approx = R(belyi)
        # curves_approx = [
        #     parent(curve).change_ring(base_ring=CC)(curve) for curve in curves
        # ]
        jac = matrix(
            [[R(belyi_approx).derivative(vari) for vari in R.gens()]]
            + [[curve.derivative(vari) for vari in R.gens()] for curve in curves_approx]
        )
        e1_vector = matrix(
            len(curves_approx) + 1, 1, [1] + [0] * len(curves_approx)
        )  # (1,0,...,0)
        # right hand side of differential equation, aside from 2*pi*I*(belyi-a)
        rhs = vector(jac.inverse() * e1_vector)
        # the rhs function for each of 0, 1
        rhs_funs = [rhs * 2 * R(pi) * R(I) * (belyi_approx - a) for a in [0, 1]]
        # TODO: this needs to be done better:
        return (
            lambda t, z: [
                rhs_fun(**dict(zip([str(vari) for vari in varis], z)))
                for rhs_fun in rhs_funs[0]
            ],
            lambda t, z: [
                rhs_fun(**dict(zip([str(vari) for vari in varis], z)))
                for rhs_fun in rhs_funs[1]
            ],
        )


class Monodromy:
    def __init__(
        self,
        monodromy: "Constellation3",  # noqa: F821
        edge_labelling: Optional[dict] = None,
    ):
        self.monodromy = monodromy
        self.edge_labelling = edge_labelling


class MonodromyRepresentation:
    def __init__(self, around0, around1):
        self.around0 = around0
        self.around1 = around1

    def __call__(self, other):
        # TODO: Implement this as a GAP homomorphism
        if other == A:
            return self.around0
        elif other == B:
            return self.around1
        elif other == ID:
            return self.around0.Identity()
        else:
            # use libgap.PreImagesRepresentative(libgap.EpimorphismFromFreeGroup(F2)),
            # libgap.Factorization seems like a better fit, but it raised an error
            raise NotImplementedError
