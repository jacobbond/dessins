r"""
Functionality for computing algebraic relations for algebraic numbers

This module provides a class for working with approximations to algebraic numbers,
as well as an alternative implementation of PARI's
`algdep <https://doc.sagemath.org/html/en/reference/rings_standard/sage/arith/misc.html#sage.arith.misc.algdep>`_,
and additional convenience functions.

EXAMPLES::

    sage: from dessins import AlgebraicApproximation
    sage: alpha = AlgebraicApproximation( (3**(1/5)).n(250), precision=250 )
    sage: alpha.algebraic_relation(degree=7, height=100)
    x^5 - 3
    sage: alpha.min_poly()
    x^5 - 3
    sage: alpha.algebraic_number()
    1.245730939615518?
    sage: type(alpha.algebraic_number())
    <class 'sage.rings.qqbar.AlgebraicNumber'>

AUTHORS:

- Jacob Bond: initial implementation
"""

import sys
from numbers import Number
from typing import Optional

# isort: split
########## Sage imports ##########
# from sage.arith.misc import algebraic_dependency
from sage.functions.generalized import sgn
from sage.functions.log import log
from sage.functions.other import ceil, floor, imag, sqrt
from sage.misc.functional import n
from sage.rings.complex_interval_field import ComplexIntervalField
from sage.rings.complex_mpfr import ComplexField
from sage.rings.integer_ring import ZZ
from sage.rings.qqbar import QQbar, find_zero_result, isolating_interval
from sage.symbolic.ring import SR

##################################

##################################
#                                #
#  AlgebraicApproximation class  #
#                                #
##################################


class AlgebraicApproximation:
    r"""
    An approximation to an algebraic number.

    Any computations with an instance of :class:`AlgebraicApproximation` must begin with a call to
    the :code:`algebraic_relation` method, which receives a user-specified degree bound, and optionally
    a height bound, on the algebraic relation.

    INPUT:

    - ``approximation`` -- Number; An approximation to an algebraic number

    - ``precision`` -- Number; The precision to which approximation is known, given as ``s`` so that
      ``|approx - alpha| < 2^(-s)``

    EXAMPLES::

        sage: from dessins import AlgebraicApproximation
        sage: a = AlgebraicApproximation(sqrt(5).n(250), precision=250)
        sage: a.algebraic_relation(degree=5, height=150) in [x**2 - 5, -x**2 + 5]
        True
        sage: a.isolating_interval()
        2.236067977499789697?
        sage: a.min_poly() in [x**2 - 5, -x**2 + 5]
        True
        sage: a.algebraic_number()
        2.236067977499790?

    .. NOTE::

        This class has the following attributes:

        * ``_algebraic_number`` -- :code:`AlgebraicNumber`; The approximated algebraic number, as a member of QQbar
        * ``_approximation`` -- :code:`Number`; The approximation to the algebraic number
        * ``_isolating_interval`` -- :code:`ComplexIntervalFieldElement`; An interval isolating the approximated
          algebraic number from any other roots of its minimal polynomial
        * ``_min_poly`` -- :code:`Polynomial_integer_dense_flint`; The minimal polynomial of the approximated algebraic number
        * ``_precision`` -- :code:`Number`; The precision to which the approximated number is known
        * ``_relation`` -- :code:`Polynomial_integer_dense_flint`; An algebraic relation satisfied by the approximated algebraic number
    """

    def __init__(self, approximation: Number, precision: Number):
        self._approximation = approximation
        self._precision = precision
        self._relation = None

    def __str__(self):
        return (
            f"Approximate algebraic number {self._approximation.n()} "
            f"with precision {self._precision}"
        )

    def __repr__(self):
        return f"AlgebraicApproximation({self._approximation}, {self._precision})"

    def algebraic_relation(self, degree: int, height: int = None, proof: bool = False):
        if self._relation is not None:
            return self._relation
        self._relation = algebraic_relation(
            self._approximation, self._precision, degree, height
        )
        # self._relation = algebraic_dependency(
        #     self._approximation,
        #     degree,
        #     known_bits=self._precision,
        #     # height_bound=height,
        #     # proof=proof,
        # )
        return self._relation

    def isolating_interval(self):
        if hasattr(self, "_isolating_interval"):
            return self._isolating_interval
        if self._relation is None:
            raise AttributeError(
                "Cannot compute an isolating interval without first finding an algebraic relation."
            )

        # isolating_interval takes a single argument precision function
        def prec_fn(prec):
            return ComplexIntervalField(prec)(self._approximation)

        self._isolating_interval = isolating_interval(prec_fn, self._relation)
        return self._isolating_interval

    def min_poly(self):
        if hasattr(self, "_min_poly") and self._min_poly is not None:
            return self._min_poly
        if self._relation is None:
            raise AttributeError(
                "Cannot compute a minimal polynomial without first finding an algebraic relation."
            )
        if self._relation.is_irreducible() is True:
            self._min_poly = self._relation
        else:
            R = self._relation.parent()
            # TODO: the SR may need to be removed:
            factors = [R(factor[0]) for factor in SR(self._relation).factor_list()]

            # find_zero_result takes a two argument precision function;
            # substitutes the ComplexIntervalField into the polynomial p
            def prec_fn(poly, prec):
                return poly(ComplexIntervalField(prec)(self._approximation))

            min_poly = find_zero_result(prec_fn, factors)
            self._min_poly = min_poly
        return self._min_poly

    def algebraic_number(self):
        if hasattr(self, "_algebraic_number") and self._algebraic_number is not None:
            return self._algebraic_number
        # TODO: is this needed:
        # x = polygen(QQbar)
        self._algebraic_number = QQbar.polynomial_root(
            self.min_poly(),
            self.isolating_interval(),
        )
        return self._algebraic_number


# TODO: add a quiet argument
def determine_algebraic_numbers(
    roots: list[Number],
    precision: Number,
    degree: int,
    height: Optional[Number] = None,
    height_incr: Number = 10**5,
    **kwargs,
):
    r"""
    For each approximation of an algebraic number in the list roots,
    finds the AlgebraicNumber object for that approximation.

    INPUT:

    - ``roots`` -- :code:`list[Number]`; a list of approximations to algebraic numbers (list)

    - ``precision`` -- :code:`Number`; the precision to which the algebraic numbers are known,
      should be the least precision for a number in the list

    - ``degree`` -- :code:`int`; a bound on the degrees of the algebraic numbers

    - ``height`` -- :code:`Optional[Number]`; a guess for a bound on the heights of the algebraic numbers,
      if no height is given, uses the precision to determine the
      maximum height bound for which precision is sufficient

    - ``height_incr`` -- :code:`Number`; the increment to make to height each time no algebraic
      relation is found

    - ``kwargs`` -- any keyword arguments to be passed to :code:`algebraic_relation`

    OUTPUT: a list of :code:`AlgebraicNumber` objects for the approximations

    EXAMPLES::

        sage: from dessins import determine_algebraic_numbers
        sage: approxs = [sqrt(sqrt(2)+sqrt(3)).n(prec=275),\
        (7^(1/3)+9^(1/3)).n(prec=250)]
        sage: alg_approxs = determine_algebraic_numbers(approxs, 250, 10, 100)
        Finding an algebraic relation for 1.77377123,
        3.99301501
        sage: alg_approxs
        [1.773771228186424?, 3.993015005824293?]
    """
    approx_printer = ApproximationPrinter(len(roots))

    approxs = []

    for idx, root in enumerate(roots):
        num = AlgebraicApproximation(root, precision)

        approx_printer(num, idx)

        height_bound = height
        while True:  # keep increasing the height until a polynomial is found
            relation = num.algebraic_relation(degree, height_bound, **kwargs)
            if relation is not None:
                break
            height_bound = height_bound * height_incr
        approxs.append(num.algebraic_number())
    print("")
    return approxs


class ApproximationPrinter:
    def __init__(self, num_approximations):
        # intro text approximately the same width as 3 approximations;
        # use stdout to control newlines
        self.num_per_line, self.approxs_printed, self.C = 4, 3, ComplexField(32)
        self.num_approximations = num_approximations
        sys.stdout.write("\nFinding an algebraic relation for ")

    def __call__(self, num, idx):
        sys.stdout.write(f"{self.C(num._approximation)}")
        if idx != self.num_approximations - 1:
            sys.stdout.write(",")
        self.approxs_printed += 2 if imag(num._approximation) != 0 else 1
        if (
            self.approxs_printed % self.num_per_line == 0
            or self.approxs_printed % (self.num_per_line + 1) == 0
        ):
            print("")
            self.approxs_printed = 0
        else:
            sys.stdout.write(" ")


###############################################
#                                             #
#  getting linear relations between elements  #
#                                             #
###############################################


# TODO: use smaller functions to simplify the code
def algebraic_relation(
    alpha,
    precision,
    degree,
    height=None,
    algorithm="fpLLL:wrapper",
    force_precision=True,
    msd_digits=None,
    proof=True,
):
    r"""
    Either returns an algebraic relation (polynomial) for the algebraic
    number approximated by alpha or returns None if no relation is found

    INPUT:

    - ``alpha`` -- Number; an approximation to an algebraic number

    - ``precision`` -- Number; the precision to which alpha is known, given as ``s`` so that
      ``|approx - alpha| < 2^(-s)``

    - ``degree`` -- Number; a bound on the degree of the algebraic relation

    - ``height`` -- Number; a bound on the height (max of the coefficients) of the
      algebraic relation

    - ``algorithm`` -- :code:`string` (default: :code:`"fpLLL:wrapper"`); the
      algorithm which is to be passed to LLL (see Sage documentation for LLL for
      discussion of the options)

    - ``force_precision`` -- :code:`bool` (default: :code:`True`); specifies
      whether to raise an error if alpha has insufficient precsion
      for the degree and height

    - ``proof`` -- :code:`bool` (default: :code:`True`); if :code:`False`,
      uses :code:`0.8*precision`, as in Pari

    - ``msd_digits`` -- :code:`Optional[int]` (default: :code:`None`);
      specifies how many significant digits N should be trimmed to;
      very large N cause issues when taking floor/ceil, so this allows
      taking only the most significant digits

    OUTPUT: an algebraic relation satisfied by the algebraic number which alpha approximates (polynomial) or :code:`None` if no relation is found

    EXAMPLES::

        sage: from dessins.belyi_maps.algebraic_relation import algebraic_relation
        sage: a = (sqrt(2) + sqrt(3)).n(prec=500)
        sage: algebraic_relation(a, 500, 4, height=50)
        x^4 - 10*x^2 + 1

    REFERENCES:

        Kannan, R.; Lenstra, A. K.; Lovasz, L. Polynomial factorization and nonrandomness of bits of algebraic and some transcendental numbers. Math. Comp. 50 (1988), no. 181, 235-250.
    """
    from sage.matrix.constructor import identity_matrix

    d, H = degree, height  # to improve readability of formulas
    if abs(alpha) > 1:  # ensures the approximation is bounded by 1
        alpha = 1 / alpha
        # TODO: check this:  if inverted, the epsilon bound becomes a 3*epsilon bound.
        # For simplicity, remove 4*epsilon from the precision.
        precision -= 2
        inverted = True
    else:
        inverted = False

    N, H = _integer_relation_multiplier(d, H, precision, proof)
    s = n(log(N) / log(2))  # TODO: how does using n() affect things?
    C = ComplexField(2 * s)  # 2* for real and complex parts
    if msd_digits is not None:
        # TODO: is recalculating height bound or necessary precision needed after this?
        N, s = msd_ceil(N, msd_digits), msd_ceil(s, msd_digits)
    # because of floors, etc. approx can have e.g. precision 224, but an error is
    # raised requesting precision 222:
    # TODO: should be s - log_2(4*d):
    if force_precision is True and precision <= (s + 1 / 2):
        raise ValueError(
            f"The approximation does not have sufficient precision. The\n"
            f"approximation must have precision at least {s} to guarantee\n"
            f"the result. It's suggested that the degree and/or height bounds\n"
            f"be increased."
        )
    elif precision <= (s + 1 / 2):
        print(
            f"The approximation does not have sufficient precision. The\n"
            f"approximation must have precision at least {s} to guarantee\n"
            f"the result. It's suggested that the degree and/or height bounds\n"
            f"be increased."
        )

    # Set up the matrix to pass to LLL:
    B = identity_matrix(ZZ, 2).matrix_from_columns([0, 1] + [1, 1])
    # TODO: what types use methods and what types use attributes for real/imag?
    try:
        N_alpha_real = N * alpha.real
        N_alpha_imag = N * alpha.imag
    except TypeError:
        N_alpha_real = N * alpha.real()
        N_alpha_imag = N * alpha.imag()
    B.set_column(2, [floor(N), floor(N_alpha_real)])
    B.set_column(3, [0, floor(N_alpha_imag)])

    # Initialize the while loop:
    alpha_power = alpha
    count = 2
    norm_bound = 2 ** (d / 2) * (d + 1) * H  # bound from KLL88
    # make this a for loop?:
    while True:
        B = B.LLL(algorithm=algorithm)
        v = ZZ["x"](B[0][:-2].list())
        norm = _norm_tilde(v, alpha, N)  # TODO: how does .n() affect this?
        if count == d + 1 or norm <= norm_bound:
            break
        # Ensures the powers stay within 2^(-s) of powers of alpha:
        alpha_power = C(alpha * alpha_power)  # .n(prec=s)
        B = knapsack_dimension_increase(
            B, [floor(N * alpha_power.real()), floor(N * alpha_power.imag())]
        )
        count += 1

    # test for number of coefficients in an attempt to deal with GitLab Issue #24
    # since then v has either no zeros or only has zeros at 0:
    if (count != d + 1 or norm <= norm_bound) and (
        len(v.coefficients()) > 1 or alpha < 0.1
    ):
        if inverted is True:
            v = B[0][:-2].list()
            v.reverse()  # reverse the order of the coefficients since a->1/a
            return ZZ["x"](v)
        else:
            return v
    else:
        return None


def _norm_tilde(poly, alpha, N):
    """
    Computes |ftilde| of a polynomial f as in KLL88:
    |ftilde|^2 = (length of f)^2 + 2^(2*s)|f(approx)|^2

    INPUT:

    - ``poly`` -- the polynomial whose norm should be computed

    - ``alpha`` -- the value at which the polynomial should be evaluated

    - ``N`` -- the integer relation multiplier 2^s, where the approximation
      is known within 2^(-s)

    OUTPUT: norm of ftilde
    """
    poly = SR(poly)  # Gives control of the format of poly.coefficients()
    return sqrt(
        sum([a**2 for a, _ in poly.coefficients()]) + N**2 * abs(poly(x=alpha)) ** 2
    )


def _integer_relation_multiplier(d, H, precision, proof):
    if H is None:  # best height bound given the precision
        H = floor(2 ** (precision / (2 * d) - d / 4) * (d + 1) ** (-(3 / 4 + 1 / d)))
        print(f"No height bound was specified. Using an effective height bound of {H}.")
        int_rel_mult = 2**precision
    elif proof is True:  # Integer relation multiplier from [KLL88]:
        int_rel_mult = 2 ** (d**2 / 2) * (d + 1) ** (3 * d / 2 + 2) * H ** (2 * d)
    else:  # Integer relation multiplier from Pari:
        int_rel_mult = 0.8 * precision

    return int_rel_mult, H


def knapsack_dimension_increase(mat, elt):
    r"""
    Construct a knapsack matrix of dimension one greater than the input
    matrix using the specified element.

    INPUT:

    - ``mat`` -- a knapsack matrix of the form  [ I_n | ``*`` ]

    - ``elt`` -- what to place in the bottom right of the new knapsack matrix
      (usually a number of list of numbers, though a row matrix or
      vector should also work)

    OUTPUT: the matrix::

        [   I_n   0 |  *  ]
        [ 0 .. 0  1 | elt ]

    EXAMPLES::

        sage: from dessins import knapsack_dimension_increase
        sage: B = identity_matrix(ZZ, 2)
        sage: B = B.augment( vector((1, 2)) )
        sage: knapsack_dimension_increase(B, 3)
        [1 0 0 1]
        [0 1 0 2]
        [0 0 1 3]
    """
    try:  # allows for flexibility of input (number or list)
        length = len(elt)
    except TypeError:
        elt = [elt]
        length = 1
    n = mat.nrows()
    m = mat.ncols()
    # Creating new columns by copying current columns seems to allow for faster
    # construction of the new matrix:
    A = mat.matrix_from_rows_and_columns(
        list(range(n)) + [n - 1],
        list(range(m - length + 1)) + list(range(m - length, m)),
    )
    A.set_row(n, [0] * (m - length + 1) + elt)
    A.set_column(m - length, [0] * n + [1])
    return A


####################################
#                                  #
#  miscellaneous helper functions  #
#                                  #
####################################


def msd_ceil(num, digits):
    """
    Increases the (digits+1)th most significant digit of num by between
    6 and 15, thereby giving a rough upper bound on num. (by 6 digits if
    (digits+1)th digit is 4; by 15 if (digits+1)th digit is 5, due to
    rounding)
    """
    total_digits = ceil(log(abs(num.n())) / log(10))
    digits = min(digits, total_digits)
    num = num.n(digits=digits) + sgn(num.n()) * 10 ** (total_digits - digits)
    return num


def total_multiplicities(ramification_dict):
    """
    Find the total of the ramification multiplicities for
    ramification_dict.
    """
    return sum([k * v for k, v in ramification_dict])


def h_d_to_precision(h, d):
    """
    Computes the precision necessary to recover a relation bounded
    by height h and degree d.
    """
    # without using floats, would throw an overflow error when calling
    # from the command line:
    N = 2 ** (d**2 / 2) * (d + 1) ** (3 * d / 2 + 2) * h ** (2 * d)
    return log(N) / log(2)
