#!/usr/bin/env sage
"""
Module for computing with dessins d'enfants and Belyi maps.
"""

import sys

_usage = """
usage: {} [-zero ZERO] [-one ONE] [-inf INF] [-file FILE]
               [-shape SHAPE] [-variables VARIABLES] [-d D] [-h H]
               [-output OUT] [--help]
""".format(
    sys.argv[0]
)
_description = """
Given the ramification indices over 0, 1, and infinity, compute an
approximation to the Belyi maps with that passport.  If a filename
stem and extension are provided, save the plots of the Belyi maps to
stem#.extension.  If degree and height bounds for the minimal
polynomials of the coefficients for each Belyi map are provided,
compute exact Belyi maps and their field of definition.

Examples:
compute_belyi.py -0 4,3,1^3 -1 2^5 -inf 9,1 -d 5 -h 10^10 -out plot.pdf
compute_belyi.py -file sols.csv -shape "(x^3 + a2*x^2 + a1*x + a0)*(x + a3)^3
    *(x + a4)^4/((c0 + x)*k)" -variables a0,a1,a2,a3,a4,c0 -d 5 -h 10^10
"""
import argparse
import textwrap
import time
from collections import Counter, defaultdict
from itertools import chain
from random import choice

# isort: split
########## Sage imports ##########

from sage.calculus.predefined import x
from sage.calculus.var import var
from sage.functions.log import log
from sage.functions.other import ceil
from sage.misc.sage_eval import sage_eval
from sage.rings.complex_mpfr import ComplexField
from sage.rings.number_field.number_field import NumberField
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.qqbar import number_field_elements_from_algebraics
from sage.rings.rational_field import QQ
from sage.symbolic.ring import SR

CC = ComplexField()
##################################

# isort: split
########## this package ##########
if __name__ == "__main__":
    from pathlib import Path

    sys.path.append(str(Path(__file__).parent.joinpath("../../dessins").resolve()))

    from dessins.belyi_maps.algebraic_relation import (
        determine_algebraic_numbers,
        h_d_to_precision,
    )
    from dessins.belyi_maps.solver import Solver, import_roots
else:
    from .algebraic_relation import determine_algebraic_numbers, h_d_to_precision
    from .solver import Solver, import_roots

##################################

# Example usage:
# sage: from dessins import type_to_belyis
#########################
# sage: belyi, belyis = type_to_belyis({4: 1, 3: 2, 1: 4},
#                            {13: 1, 1: 1}, {2: 7}, None,
#                            outfile='stickfig.pdf', pts_per_edge=10,
#                            math_opts={'WorkingPrecision': 2800})
# The polynomial identity coming from the ramification behavior has the form
# (a3*x^3 + x^4 + a2*x^2 + a1*x + a0)*(a5*x + x^2 + a4)^3*(a6 + x)^4 -
# (c0 + x)*k == (b6*x^6 + x^7 + b5*x^5 + b4*x^4 + b3*x^3 + b2*x^2 + b1
# *x + b0)^2
# Which variable should be substituted for first? a6
# What value should be substituted for a6? 0
# Which variable should be substituted for second? c0
# What value should be substituted for c0? 1
#
#                           OR
#
# sage: sub = {var('a6'): 0, var('c0'): 1}
# sage: belyi, belyis = type_to_belyis({4: 1, 3: 2, 1: 4},
#                            {13: 1, 1: 1}, {2: 7}, sub,
#                            outfile='stickfig.pdf', pts_per_edge=10,
#                            math_opts={'WorkingPrecision': 2800})
#########################
# sage: beta = belyi_map(belyis[0][1].items(), belyi,
#                      floor(log(10^(2800))/log(2)), 10, height=10^40)

#########################
#                       #
#  top level functions  #
#                       #
#########################


# own variable should be used, rather than the global x, but
# where should it be initialized?
def compute_belyi_maps(
    zero=None,
    one=None,
    inf=None,
    d=None,
    h=None,
    subs_dict=None,
    out=None,
    proof=True,
    **kwargs,
):
    r"""
    EXAMPLES::

        sage: from dessins import compute_belyi_maps
        sage: compute_belyi_maps([3,1], [2,2], [3,1], d=3, h=10**3, subs_dict={var('a0'): 0, var('c0'): 1})
        ...
        Found 1 Galois orbits.
        [Belyi map defined by (-1/64*x^4 + 1/8*x^3)/(x + 1) on Projective Space of dimension 1 over Rational Field]
    """
    zero_dict, one_dict, inf_dict = [_input_to_dict(i) for i in [zero, one, inf]]

    # would throw an overflow error from the command line without eval;
    # str so that numbers can be passed when running from Sage:
    h, d = sage_eval(str(h)), sage_eval(str(d))
    if proof is True:
        # require some extra pecision in case height bound was too small
        # (this should be investigated):
        prec = ceil(4 * h_d_to_precision(h, d))
    else:
        # should extra precision be requested?
        # taken from Cohen's book; assumes 10^(-6) < alpha^d < 10^6,
        # how should this be adjusted for large
        prec = h ** (-1.5 * d)
    # Mathematica does things in terms of digits:
    # how does ceil and log affect this? Sage has a special constant
    # for log_10(2), use one?
    math_prec = ceil(log(2**prec) / log(10))

    print(
        "\n{} bits of precision will be used to compute approximate "
        "Belyi maps.".format(prec)
    )

    beta, betas = type_to_belyis(
        zero_dict,
        one_dict,
        inf_dict,
        subs_dict=subs_dict,
        math_opts={"WorkingPrecision": math_prec},
        **kwargs,
    )
    print(
        "Found {} potential Belyi maps (some may be repeated or the "
        "result of parasitic solutions).".format(len(betas))
    )

    if out is not None:
        print("\nPlotting approximate Belyi maps.")
        _plot_all_belyis(beta, betas, out)

    print("")

    from .belyi_map import BelyiMap

    # attempts to find one Belyi map from each Galois orbit, stopping
    # when the total size of the Galois orbits is equal to the number
    # of roots of the polynomial system; what if a Galois orbit is
    # repeated?
    total_degree = 0
    y = var("y")
    num_betas = len(betas)
    belyis = []
    prev_belyis = []
    while total_degree != num_betas and betas:
        # remove a beta when it is considered:
        current_root = betas.pop(choice(range(len(betas))))[1]
        belyi = exact_belyi_map(current_root.items(), beta, prec, d, height=h)
        if [belyi[0], belyi[1]] not in prev_belyis:
            # coerce to SR for consistant behavior:
            total_degree += SR(belyi[1]).degree(y)

            # TODO: do this better:
            base_field = NumberField(belyi[1], "a")
            a = base_field.gen()
            form = sage_eval(
                str(belyi[0]),
                locals={"a": a, "x": PolynomialRing(base_field, "x").gen()},
            )
            belyis.append(
                BelyiMap(
                    form,
                    base_field=base_field,
                    embedding=belyi[2],
                )
            )
            prev_belyis.append([belyi[0], belyi[1]])

    print("\nFound {} Galois orbits.".format(len(belyis)))
    return belyis


def _input_to_dict(valencies):
    if (
        hasattr(valencies, "keys")
        and hasattr(valencies, "__get_item__")
        and hasattr(valencies, "copy")
    ):
        return valencies
    elif hasattr(valencies, "split"):
        v_list = valencies.split(",")
        v_dict = defaultdict(int)  # if key is missing, value defaults to 0
        for valency in v_list:
            val_mult = valency.split("**")
            v = int(val_mult[0])
            try:
                m = int(val_mult[1])
            except IndexError:
                m = 1
            v_dict[v] += m
        return v_dict
    else:
        return Counter(valencies)


def _plot_all_belyis(beta, betas, out):
    # TODO: expose the plot function outside of the BelyiMap class:
    from .belyi_map import BelyiMap

    count = 0  # so the image will correspond to the index in betas
    # get the stem of the filename in order to append count to it
    try:
        filename = out.split(".")
        # use this instead of stem,extension = outfile.split('.')
        # in order to be able to recognize an IndexError
        stem = filename[0]
        extension = "." + filename[1]
    except IndexError:  # in case outfile has no extension
        raise ValueError(
            "Filename must have an extension in order to tell"
            " Sage which image format to use."
        )
    C = ComplexField(128)  # only use 128 bits for coefficients when plotting
    for b in betas:
        b0 = b[1].copy()
        b0 = {s: C(t) for s, t in b0.items()}  # coerce coeffs to C
        beta0 = beta.substitute(b0)
        BelyiMap(beta0).plot(output=f"{stem}{str(count)}{extension}")
        count += 1
    return None


def belyi_map_from_root(
    infile=None, form=None, variables=None, h=None, d=None, out=None, **kwargs
):
    # would throw an overflow error from the command line without eval;
    # str so that numbers can be passed when running from Sage:
    h, d = sage_eval(str(h)), sage_eval(str(d))
    # require some extra pecision in case height bound was too small
    # (this should be investigated):
    prec = ceil(4 * h_d_to_precision(h, d))
    # Mathematica does things in terms of digits:
    math_prec = ceil(log(2**prec) / log(10))

    # turn form into a Sage expression:
    variable_tuple = var(variables)
    # make a dictionary for sage_eval containing variables and x:
    variable_dict = dict(zip(variables.split(",") + ["x"], variable_tuple + (x,)))
    form = sage_eval(form, locals=variable_dict)

    roots = import_roots(infile, math_prec)
    tuples = zip(variable_tuple, roots)

    beta = exact_belyi_map(tuples, form, prec, d, height=h, **kwargs)

    # TODO: expose the plot function without creating a BelyiMap
    from .belyi_map import BelyiMap

    BelyiMap(form.substitute(dict(tuples))).plot(output=out)
    return beta


#######################
#                     #
#  Dessins d'Enfants  #
#                     #
#######################


def type_to_belyis(zero_dict, one_dict, inf_dict, **kwargs):
    r"""
    Takes dictionaries for the multiplicity of ramification indices
    above 0, 1, and infinity and calculates approximate Belyi maps.
    Returns a list of the approximate Belyi maps.

    INPUT:

    - ``zero_dict`` -- a dictionary with key, value pairs the ramification
      index over 0 and multiplicity of that index

    - ``one_dict`` -- a dictionary for the ramification over one

    - ``inf_dict`` -- a dictionary for the ramification over infinity

    - ``subs_dict`` -- a dictionary specializing the two degrees of freedom for
      the polynomial system that results for the Belyi map; key, value
      pair should be a variable and the value to be sustituted

    - ``math_opts`` -- options to be passed to NSolve through roots_from_math
      (dictionary) ({'WorkingPrecision': 100} or {'WorkingPrecision':
      '100'})

    OUTPUT: returns the generic Belyi map and list whose elements are
      a tuple containing a specialized Belyi map and a dictionary of
      subsitutions to obtain this Belyi map from the generic Belyi map

    EXAMPLES::

        sage: from dessins import type_to_belyis
        sage: type_to_belyis({3: 1, 1: 1}, {2: 2}, {3: 1, 1: 1},\
                         math_opts={'WorkingPrecision': 5},\
                         subs_dict={var('c0'): 0, var('a0'): 1})
        ((a1 + x)*(x + 1)^3/(k*x),
         [[0.01562500000000000*(x + 9.000000000000000)*(x + 1)^3/x,
           {a1: 9.000000000000000,
            b0: -3.000000000000000,
            b1: 6.000000000000000,
            k: 64.00000000000000}]])
    """
    from ..utils import consume, digits_to_bits

    subs_dict = kwargs.pop("subs_dict", None)
    math_opts = kwargs.pop("math_opts", None)
    poly_sys, sys = type_to_system(zero_dict, one_dict, inf_dict)
    # use the lhs of poly_sys to get beta:
    lhs_k_coeffs = poly_sys.lhs().coefficients(var("k"))
    # - to account moving k term to the other side:
    beta = lhs_k_coeffs[0][0] / (var("k") * (-lhs_k_coeffs[1][0]))

    if subs_dict is None:
        # ask the user for the specializations which should be made
        subs_dict = _get_subs_dict(poly_sys)
    # all betas will use subs_dict, so do it now for all
    beta = beta.substitute(subs_dict)
    # make substitutions in sys for variables in subs_dict:
    sys = [z.substitute(subs_dict) for z in sys]
    k_func = var("k") - sys.pop()  # remove k from the expression

    var_list = list(poly_sys.variables())
    # x, k, and subs_dict keys appear in poly_sys,but shouldn't be in
    # var_list;consume ensures that each element of the map is evaluated
    # TODO: do this without map, consume
    consume(map(lambda v: var_list.remove(v), chain(subs_dict.keys(), [x, var("k")])))

    #    roots = roots_from_math(sys, var_list, math_opts)
    if math_opts is None:
        prec = 53  # Sage default precision (previous code had None as default)
    else:  # WorkingPrecision is given in terms of digits
        prec = digits_to_bits(int(math_opts["WorkingPrecision"]))
    roots = Solver(sys, var_list, prec=digits_to_bits(prec)).solve()

    betas = []
    for root in roots:
        # build a substitution dictionary from roots
        subs_dict0 = dict(zip(var_list, root))

        # use .expand() to force any cancellations
        subs_dict0[var("k")] = _find_k(k_func.substitute(subs_dict0).expand())
        beta0 = beta.substitute(subs_dict0)
        betas.append([beta0, subs_dict0])

    return beta, betas


def _find_k(func):
    r"""
    Computes the value of k corresponding to a given solution for the
    coefficients of a Belyi map.

    INPUT:

        - ``func`` -- k as a rational function of x

    OUTPUT: numerical value of k
    """
    # note: if numerator is 0, then p = r, and relatively prime implies
    # p,r are constant, hence there are no vertices or edges
    denom = func.denominator()
    try:
        CC(denom)  # if this returns, denominator is constant
        return func.substitute(x=0)
    except TypeError:
        # so long as denominator isn't constant, eventually find an
        # integer for subst; checking for < 10^(-5) will make sure
        # subst isn't a 0 ( even if this gives a false positive,
        # eventually a large enough integer will cause the magnitude
        # to be > 10^(-5) )
        subst = 0
        while abs(denom.substitute(x=subst).n()) < 10 ** (-5):
            subst += 1
        try:
            out = func.substitute(x=subst)
        # func.denominator() can sometimes find the wrong denominator (such as
        # when?), causing the above test to fail and resulting in division by 0
        # TODO: is there a better way to handle this case?
        except ValueError:
            subst += 1
            out = func.substitute(x=subst)
        return out


def _get_subs_dict(poly_sys):
    print("")
    print(
        "The polynomial identity coming from the ramification behavior "
        "has the form\n{}".format(poly_sys)
    )

    while True:
        try:
            var1 = eval(input("Which variable should be substituted for first? "))
            break
        except NameError:
            print("That variable is not recognized.")

    while True:
        sub1 = input("What value should be substituted for {}? ".format(var1))
        # TODO: can this be handled without a bare except?
        try:
            CC(sub1)
            sub1 = eval(sub1)
            break
        except:  # noqa: E722
            pass

    while True:
        try:
            var2 = eval(input("Which variable should be substituted for second? "))
            break
        except NameError:
            print("That variable is not recognized.")

    while True:
        sub2 = input("What value should be substituted for {}? ".format(var2))
        # TODO: can this be handled without a bare except?
        try:
            CC(sub2)
            sub2 = eval(sub2)
            break
        except:  # noqa: E722
            pass

    print("")
    # Sage seems to evaluate the inputs automatically, so there is no
    # need to call var or eval:
    subs_dict = {var1: sub1, var2: sub2}
    return subs_dict


def type_to_system(zero_dict, one_dict, inf_dict):
    r"""
    Takes dictionaries of the ramification indices over zero, one,
    and infinity along with the multiplicity of each index and
    returns the polynomial system that must be solved to recover the
    Belyi map (resulting from comparing coefficients of
    P(x) - kQ(x) = R(x); uses the ASD differentiation trick).

    INPUT:

    - ``zero_dict`` -- a dictionary with key, value pairs the ramification
      index over 0 and multiplicity of that index

    - ``one_dict`` -- a dictionary for the ramification over one

    - ``inf_dict`` -- a dictionary for the ramification over infinity

    OUTPUT: the polynomial equation and a list of equations, one for
      each coefficient

    EXAMPLES::

        sage: from dessins import type_to_system
        sage: type_to_system({3: 1, 1: 1}, {2: 2}, {3: 1, 1: 1})
        ((a0 + x)^3*(a1 + x) - (c0 + x)*k == (b1*x + x^2 + b0)^2,
         [a0*a1 - a0*c0 - 3*a1*c0 + 3*b0,
          -2*a1 + 3*b1 - 4*c0,
          3*a0^2 - 2*b1*c0 + b0,
          6*a0 - b1 - 4*c0,
          k - ((a0 + x)^3*(a1 + x) - (b1*x + x^2 + b0)^2)/(c0 + x)])
        sage: type_to_system({3: 1, 1: 3}, {2: 3}, {6: 1})
        ((a3*x^2 + x^3 + a2*x + a1)*(a0 + x)^3 - k == (b2*x^2 + x^3 + b1*x + b0)^2,
         [-a0*a2 - 3*a1 + 6*b0,
          -2*a0*a3 - 4*a2 + 6*b1,
          -3*a0 - 5*a3 + 6*b2,
          6*a0^2 - 2*b1,
          12*a0 - 4*b2,
          -(a3*x^2 + x^3 + a2*x + a1)*(a0 + x)^3 + (b2*x^2 + x^3 + b1*x + b0)^2 + k])
        sage: type_to_system({3: 1}, {1: 3}, {6: 1})
        ((a0 + x)^3 - k == b2*x^2 + x^3 + b1*x + b0,
         [0, 9*a0^2 - b1, 18*a0 - 2*b2, 6, -(a0 + x)^3 + b2*x^2 + x^3 + b1*x + b0 + k])

    REFERENCES:

        Birch, B. Noncongruence subgroups, covers and drawings. The Grothendick theory of dessins d'enfants (Luminy, 1993), 25-46, London Math. Soc. Lecture Note Ser., 200, Cambridge Univ. Press, Cambridge, 1994.

        Sijsling, J.; Voight, J. On computing Belyi maps. Numero consacre au trimestre "Methodes arithmetiques et applications", automne 2013, 73-131, Publ. Math. Besancon Algebre Theorie Nr., 2014/1, Presses Univ. Franch-Comte, Besancon, 2014.
    """
    from sage.arith.misc import gcd
    from sage.calculus.functional import diff, expand

    inf_dict = inf_dict.copy()  # del causes side effect outside function
    max_key = max(inf_dict.keys())
    if inf_dict[max_key] == 1:  # mark face with the most edges
        del inf_dict[max_key]
    else:
        inf_dict[max_key] -= 1
    # strings are for for naming variables:
    creation_info = zip([zero_dict, one_dict, inf_dict], ["a", "b", "c"])

    # build all of the different polynomials as in Sijsling & Voight
    # note different ordering in this line:
    p, r, q = [_poly_from_degree_dict(y[0], y[1]) for y in creation_info]
    pprime, qprime, rprime = (diff(p, x), diff(q, x), diff(r, x))
    # changes to the code for gcd cause issues if polys are not expanded
    expanded_polys = zip(map(expand, [p, q, r]), map(expand, [pprime, qprime, rprime]))
    p0, q0, r0 = [gcd(*polys) for polys in expanded_polys]
    pairs = zip([p, pprime, q, qprime, r, rprime], 2 * [p0] + 2 * [q0] + 2 * [r0])
    P, Pt, Q, Qt, R, Rt = [SR(a).maxima_methods().divide(b)[0] for a, b in pairs]

    # get the leading coefficient so the leading terms will match
    lc1 = (Q * Pt - P * Qt).coefficients(x)[-1][0] / r0.coefficients(x)[-1][0]
    lc2 = (Rt * Q - R * Qt).coefficients(x)[-1][0] / p0.coefficients(x)[-1][0]
    sys1 = [c[0] for c in (lc1 * r0 - Q * Pt + P * Qt).coefficients(x)]
    sys2 = [c[0] for c in (lc2 * p0 - Rt * Q + R * Qt).coefficients(x)]
    poly_sys, coeff_sys = (p - var("k") * q == r), sys1 + sys2 + [
        var("k") - (p - r) / q
    ]
    return poly_sys, coeff_sys


def _poly_from_degree_dict(d, var_name):
    r"""
    Uses the dictionary d to build a polynomial with factors
    (monic poly of degree value)^key and coefficients having prefix
    var_name.  The factors are ordered by ascending degree and
    and descending exponent.

    INPUT:

    - ``d`` -- a dictionary with integer key, value pairs

    - ``var_name`` -- a string to use as the prefix for the coefficent names

    OUTPUT: a polynomial with factors (monic poly of degree value)^key

    .. NOTE::

        The coefficients for the polynomial will enter the namespace
        of the calling function.
    """
    x = var("x")
    poly = 1
    var_num = 0
    # build up each factor, including making variables
    # sort items of d by ascending value, then descending key:
    # switch degree and multiplicity
    deg_and_exp = sorted(d.items(), key=lambda a: (a[1], -a[0]))
    for degree, multiplicity in deg_and_exp:
        current_factor = x**multiplicity
        for m in range(multiplicity):
            current_factor += var(var_name + str(var_num)) * x**m
            var_num += 1
        poly *= (current_factor) ** degree
    return poly


def exact_belyi_map(root_tuples, form, precision, degree, height=None, **kwargs):
    r"""
    INPUT:

    - ``root_tuples`` -- a list of pairs (variable, approximation), where
      approximation will be substituted for variable in the Belyi map

    - ``form`` -- the form the Belyi map should take given as a single-variable
      rational function with coefficients given by the variables in
      root_tuples

    - ``degree`` -- a bound on the degree of the algebraic numbers being
      approximated

    - ``height`` -- a bound on the height of the algebraic numbers being
      approximated

    - ``kwargs`` -- any keyword arguments to be passed to alg_rel (see alg_rel for
      relevant arguments)

    OUTPUT: the Belyi map, the minimal polynomial of the algebraic
      number used to express the coefficients of the Belyi map, and an
      approximation to this algebraic number (as a ComplexInterval)
    """
    C = ComplexField(24)
    print(
        "Finding the coefficients of the Belyi map having approximate " "coefficients"
    )
    s = str([(v, C(r)) for v, r in root_tuples])
    print(textwrap.fill(s, 80))
    minimal = kwargs.pop("minimal", False)
    roots = [r[1] for r in root_tuples]
    algnums = determine_algebraic_numbers(roots, precision, degree, height, **kwargs)
    fld, nums, hom = number_field_elements_from_algebraics(algnums, minimal=minimal)
    # reassociate variables to roots and make a dictionary
    subs = {root[0]: algnum for root, algnum in zip(root_tuples, nums)}

    ###
    # time these:
    # is it worth using FractionFields just to use a for the root?

    # get Sage to treat the numberfield element as a polynomial in a:
    subs = {k: QQ["a"](str(v)) for k, v in subs.items()}
    form = form.substitute(subs)
    # is this next line necessary?:
    # form = FractionField(fld['x'])( str(form) )

    # R.<r> = PolynomialRing(QQ)
    ## treat the coefficients as polynomials in r, so they can be
    ## substituted into form
    # for k,v in subs.items():
    #    subs[k] = R(str(v).replace('a', 'r'))
    # form = form.substitute(subs)
    # form = FractionField(fld['x'])(str(form).replace('r', 'a'))
    ###
    try:
        return form, fld.polynomial(), hom(fld.gen())
    except AttributeError:  # QQ.polynomial() throws AttributeError
        return form, var("y") - 1, hom(fld.gen())


############################
#                          #
#  command line interface  #
#                          #
############################

# def _verify():
#    # compatible values:
#    zero_dict = {4: 1, 3: 2, 1: 4}; one_dict = {2: 7}; inf_dict = {13: 1, 1: 1}
#    # compute the field of definition and monodromy
#    # and compare to the actual values (in parentheses):
#    fld = "z^10 - 2*z^9 + 701604*z^8 + 331139820*z^7 + 175405434960*z^6 + 14"\
#          "6558088827136*z^5 + 60876268774035960*z^4 + 12651284961746481828*"\
#          "z^3 + 1380286362267735570576*z^2 + 74473123741238478344320*z + 15"\
#          "05475930344549111438672"
#    fld_computed = ""
#    sigma0 = "(1 2 3 4)(5 6 7)(8 9 10)"
#    sigma1 = "Either (1 5)(2 11)(3 8)(4 12)(6 7)(9 13)(10 14) or \n"\
#             "(1 5)(2 11)(3 8)(4 12)(6 13)(7 14)(9 10), depending on the "\
#             "ordering of the \ndegree three vertices."
#    sigma0_computed, sigma1_computed = "", ""
#    print('Verification:\nField of definition: {0} ({1})\nsigma0: {2} ( {3} )'\
#          '\nsigma1: {4} ({5})'.format(fld_computed, fld, sigma0_computed,
#                                       sigma0, sigma1_computed, sigma1))


def _option_parse():
    parser = argparse.ArgumentParser(add_help=False, description=_description)

    from_valencies = parser.add_argument_group("Belyi maps from ramification")
    from_valencies.add_argument(
        "-zero",
        "-0",
        help="The ramification indices above 0 (4,3^2,1^4 or 4,3,3,1,1,1,1)",
    )
    from_valencies.add_argument(
        "-one", "-1", help="The ramification indices above 1 (2^7 or 2,2,2,2,2,2,2)"
    )
    from_valencies.add_argument(
        "-inf", "-infinity", help="The ramification indices above infinity (13,1)"
    )

    from_root = parser.add_argument_group("Belyi map from root")
    from_root.add_argument(
        "-file", help="The filename from which to read an approximate root."
    )
    from_root.add_argument(
        "-shape",
        help="The shape of the Belyi map, including variables for coefficients.",
    )
    from_root.add_argument(
        "-variables",
        help="The names of the variables corresponding to each coordinate of "
        "the root.  These must be given in the same order as the "
        "variables are specified in the input file.",
    )

    parser.add_argument(
        "-d", "-degree", help="A bound on the degree of the field of definition"
    )
    parser.add_argument(
        "-h",
        "-height",
        help="A bound on the height of the minimal polynomial of solutions to "
        "the polynomial system for the dessin; note that this bound is "
        "only a guide in that heights above this bound will be used in an"
        " attempt to find a minimal polynomial",
    )
    parser.add_argument(
        "-approximate", action="store_true", help="Compute approximate Belyi maps only."
    )

    parser.add_argument(
        "-output",
        help="The stem and extension to use for plotting the dessins (example:"
        " plot.pdf saves the plots as plot#.pdf)",
    )
    parser.add_argument(
        "-time", action="store_true", help="Provide timing information."
    )
    parser.add_argument("--help", action="help", help="Print this help message.")
    return parser.parse_args()


def main(args):
    if (
        args.zero,
        args.one,
        args.inf,
    ) != (None, None, None) and (
        args.file,
        args.shape,
        args.variables,
    ) != (None, None, None):
        string = (
            "{}: error: arguments -zero/-0, -one/-1, -inf/-infinity, not "
            "allowed with arguments -file, -shape, "
            "-variables".format(sys.argv[0])
        )
        print(string)
        sys.exit()
    elif (args.zero, args.one, args.inf) != (None, None, None):
        if args.time is True:
            start_wall = time.time()
            start_process = time.clock()

        # get approximate Belyi maps only:
        if args.approximate is True:
            zero_dict, one_dict, inf_dict = [
                _input_to_dict(i) for i in [args.zero, args.one, args.inf]
            ]
            beta, betas = type_to_belyis(zero_dict, one_dict, inf_dict, subs_dict=None)
            print(
                "Approximate potential Belyi maps (some may be repeated or the "
                "result of parasitic solutions):"
            )
            for belyi in betas:
                print("\n" + str(belyi[0]))
            if args.output is not None:
                print("\nPlotting approximate Belyi maps.")
                _plot_all_belyis(beta, betas, args.output)
            print("")

        else:
            # class objects have a __dict__ attribute with attributes:values
            belyis = compute_belyi_maps(**args.__dict__)
            print("\nThe potential Belyi maps are given by:")
            for belyi in belyis:
                # is checking the field of definition sufficient?
                # how canonical is POLRED(min_poly)?
                if belyi[2] != 1:
                    print(
                        "\n{}\nwhere a is a root of the "
                        "polynomial\n{}\n".format(belyi[0], belyi[1])
                    )
                else:
                    print("\n{}\n".format(belyi[0]))

        if args.time is True:
            print(
                "Time taken:\n{}s CPU time\n{}s wall time"
                "".format(time.clock() - start_process, time.time() - start_wall)
            )
    elif (args.file, args.shape, args.variables) != (None, None, None):
        if args.time is True:
            start_wall = time.time()
            start_process = time.clock()
        belyi = belyi_map_from_root(**args.__dict__)
        if belyi[2] != 1:
            print(
                "\nThe Belyi map is given by\n{}\nwhere a is the root"
                "\n{}\nof the polynomial"
                "\n{}\n".format(belyi[0], belyi[2], belyi[1])
            )
        else:
            print("The Belyi map is given by \n{}".format(belyi))
        if args.time is True:
            print(
                "Time taken:\n{}s processor time\n{}s wall time"
                "".format(time.clock() - start_process, time.time() - start_wall)
            )


if __name__ == "__main__" and "__file__" in globals():
    args = _option_parse()
    main(args)
