from collections import defaultdict
from numbers import Number
from typing import Callable, Optional

import matplotlib.pyplot as plt
import numpy as np

# isort: split
########## Sage imports ##########
from sage.misc.functional import N
from sage.rings.complex_mpfr import ComplexField
from sage.rings.integer import Integer

CC = ComplexField()
##################################

# isort: split
########## this package ##########
from ..utils import enlist
from .plot import add_arrow
from .solver import Solver

##################################


class Path:
    r"""
    A container to store a path defined as a sequence of points across some time range.

    INPUT:

    - ``start_time`` -- Number; the starting time of the path

    - ``start_point`` -- Number; the starting point of the path
    """

    def __init__(
        self,
        start_time: Number,
        start_point: Number,
        # length: Optional[Number] = None,
    ):
        self.start_time = start_time
        self.end_time = start_time
        self.start_point = start_point
        self.end_point = start_point
        # if length is None:
        self.times = np.array([start_time], dtype=np.float64)
        self.points = np.array([start_point], dtype=np.complex128)
        # else:
        #     self.times = np.zeros((length,))
        #     self.times[0] = start_time
        #     self.points = np.zeros((length, np.array(start_point).shape[0]))
        #     self.points[0, :] = start_point

    def __len__(self):
        return self.times.shape[0]

    def __call__(self, time):
        return self.interpolation(time)

    def __getitem__(self, pos):
        if isinstance(pos, slice):
            new_path = Path(self.times[pos.start], self.points[pos.start, :])
            new_path.times = self.times[pos]
            new_path.points = self.points[pos, :]
            new_path.end_time = new_path.times[-1]
            new_path.end_point = new_path.points[-1, :]
            return new_path
        else:
            return (self.times[pos], self.points[pos, :])

    def __setitem__(self, pos, new):
        new_time, new_point = new
        self.times[pos] = new_time
        self.points[pos, :] = new_point
        if pos == 0 or pos == -self.times.shape[0]:
            self.start_time = new_time
            self.start_point = new_point
        elif pos == self.times.shape[0] - 1 or pos == -1:
            self.end_time = new_time
            self.end_point = new_point

    def append(self, new_time, new_point):
        self.end_time = new_time
        self.end_point = new_point

        self.times = np.concatenate(
            [self.times, np.array([new_time], dtype=np.float64)],
            axis=0,
            dtype=np.float64,
        )
        self.points = np.concatenate(
            [self.points, np.array([new_point], dtype=np.complex128)],
            axis=0,
            dtype=np.complex128,
        )

    def reverse(self):
        new_path = Path(self.end_time, self.end_point)
        new_path.end_time = self.start_time
        new_path.end_point = self.start_point
        new_path.times = np.flip(self.times, axis=0)
        new_path.points = np.flip(self.points, axis=0)
        return new_path

    def concatenate(self, other):
        new_path = Path(self.start_time, self.start_point)
        new_path.end_time = other.end_time
        new_path.end_point = other.end_point
        new_path.times = np.concatenate(
            [self.times, other.times], axis=0, dtype=np.float64
        )
        new_path.points = np.concatenate(
            [self.points, other.points], axis=0, dtype=np.complex128
        )
        return new_path

    def plot(self, num_pts=None, arrow=False, **kwargs):
        if num_pts is None:
            num_pts = len(self)
        t_s = np.linspace(self.start_time, self.end_time, num_pts)
        path_pts = self.interpolation()(t_s)
        p = plt.plot(path_pts.real, path_pts.imag, **kwargs)
        if arrow is True:
            add_arrow(
                p[0],
                xposition=self.interpolation()(
                    (self.end_time - self.start_time) / 2
                ).real,
                size=kwargs.get("arrow_size", 15),
                color=kwargs.get("arrow_color", None),
            )
        return p

    def interpolation(self):
        try:
            return self._interpolation
        except AttributeError:
            from scipy.interpolate import make_interp_spline

            self._interpolation = make_interp_spline(self.times, self.points, axis=0)
            return self._interpolation

    # https://docs.scipy.org/doc/scipy/tutorial/interpolate/1D.html#parametric-spline-curves
    def interpolation2(self):
        try:
            return self._interpolation
        except AttributeError:
            from scipy.interpolate import make_interp_spline

            # https://stackoverflow.com/a/24230980
            dr = np.sqrt(
                np.diff(self.points.real, axis=0) ** 2
                + np.diff(self.points.imag, axis=0) ** 2
            )  # segment lengths
            r = np.zeros_like(self.times)
            r[1:] = np.cumsum(dr)  # integrate path

            # renormalize to [self.start_time, self.end_time]:
            r /= r.max()
            r = r * (self.end_time - self.start_time) + self.start_time

            self._interpolation = make_interp_spline(r, self.points, axis=0)
            return self._interpolation

    def nearest_to_path_end(self, points):
        return nearest_point(self.end_point, points)


def nearest_point(source, points):
    return points[np.argmin(np.linalg.norm(source - points, axis=1))]


def _lift_to_preimages(function, path_fn, time_range, pts_per_path=10, prec=53):
    start, end = time_range
    solve_vars = function.variables()
    approx_fn = function.n(prec=prec)
    num, denom = approx_fn.numerator(), approx_fn.denominator()

    def equation_for_current_point(current_time):
        path_point = path_fn({path_fn.variables()[0]: current_time})
        return num - N(path_point) * denom

    start_points = Solver(
        equation_for_current_point(start),
        solve_vars,
        multiplicities=True,
    ).solve()

    paths = [Path(start, pt) for pt in start_points]

    # range from (pts_per_path - 1)/pts_per_path * (end - start) down to 0
    for j in range(pts_per_path - 1, -1, -1):
        # use Integer(j) to get exact 0 and exact 1:
        # TODO: look deeper at this:
        current_time = end - (Integer(j) / pts_per_path) * (end - start)
        upstairs = Solver(
            equation_for_current_point(current_time),
            solve_vars,
            multiplicities=True,
        ).solve()

        # TODO: can this be done better?:
        neighbors = nearest_neighbors([path.end_point for path in paths], upstairs)

        for path in paths:
            try:  # handle poles (some paths won't have a neighbor)
                new = neighbors[path.end_point].pop()
            # some point didn't get a neighbor, so neighbors default dict
            # returns an empty list
            except IndexError:
                pass  # move on to the next path
            else:  # if no KeyError
                path.append(current_time, new)

    # sort paths according to their midpoint:
    paths = sorted(paths, key=lambda path: path[len(path) // 2][1])
    return paths


# def _sample_from_lifting(belyi, path_fn, time_range, pts_per_path=10, prec=53):
#     start, end = time_range
#     try:
#         genus0 = True if belyi.genus == 0 else False
#     except RuntimeError:
#         # Curve().genus raises a RuntimeError for extension fields
#         # curve for genus 0 is just y, so does not have genus 0
#         genus0 = False

#     #    if QQbar.has_coerce_map_from(belyi._embedding.codomain()):
#     #        beta = belyi.embedding(QQbar)(belyi.function())
#     #        num, denom = beta.numerator(), beta.denominator()
#     #        curves = list(map(belyi.embedding(QQbar), belyi._curve.defining_polynomials()))
#     #    else: # embed into ComplexField
#     beta = belyi.n(prec=prec)
#     num, denom = beta.numerator(), beta.denominator()
#     curves = list(
#         map(belyi.embedding(ComplexField(prec)), belyi._curve.defining_polynomials())
#     )

#     def eqns(current_time, genus0):
#         path_pt = path_fn({path_fn.variables()[0]: current_time})
#         if genus0:
#             return num - path_pt * denom
#         else:
#             return [num - path_pt * denom] + curves

#     solve_vars = belyi.variables()

#     starting_pts = Solver(eqns(start, genus0), solve_vars, multiplicities=True).solve()
#     # create a path from each starting point
#     paths = [[[start, pt]] for pt in starting_pts]

#     # going (solve_pts-1)/solve_pts * starting_dist down to 0/solve_pts
#     for j in range(pts_per_path - 1, -1, -1):
#         # to make sure the last point is at end
#         current_time = end - (j / pts_per_path) * (end - start)
#         upstairs = Solver(
#             eqns(current_time, genus0), solve_vars, multiplicities=True
#         ).solve()
#         neighbors = nearest_neighbors([path[-1][1] for path in paths], upstairs)
#         for path in paths:
#             try:  # handle poles (some paths won't have a neighbor)
#                 new = neighbors[path[-1][1]]
#             except KeyError:  # some point didn't get a neighbor
#                 pass  # move on to the next path
#             else:  # if no KeyError
#                 path.append([current_time, new])

#     return paths


def nearest_neighbors(current, new):
    # TODO: use numpy or scipy.spatial.KDTree?
    current, new = current[:], new[:]
    neighbors = defaultdict(list)
    if len(new) < len(current):  # match unbalanced lists
        for pt in new:
            np_pt = np.array(pt)
            prev_pt = min(
                current, key=lambda a: abs(np.linalg.norm(np_pt - np.array(a)))
            )
            neighbors[prev_pt].append(pt)
            current.remove(prev_pt)
    else:
        for pt in current:
            np_pt = np.array(pt)
            next_pt = min(new, key=lambda a: abs(np.linalg.norm(np_pt - np.array(a))))
            neighbors[pt].append(next_pt)
            new.remove(next_pt)
    return neighbors


def _pop_nearest_key(dictionary, point):
    r"""
    Pop value for the nearest key
    """
    point = np.array(list(map(CC, enlist(point))))

    def normfrompoint(a):
        a = np.array(list(map(CC, enlist(a))))
        return np.linalg.norm(a - point)  # use point as a "global" variable

    to_pop = min(dictionary.keys(), key=normfrompoint)
    return dictionary.pop(to_pop)


def zvode(
    rhs: Callable,
    iv: list,
    time_steps: int,
    time_range: list[int] = [0, 1],
    min_final_step: Optional[Number] = None,
) -> list[tuple]:
    """
    A wrapper for the 'zvode' method of scipy.integrate.ode.

    INPUT:

    - ``rhs`` -- Callable; The right hand side of the differential equation to be solved;
      given as a Python function such as
    .. code-block::

        def f(t, y):
            return [1j*y[0] + y[1], y[1]**2]

    - ``iv`` -- List; the initial value; given as :code:`[time, [val1, val2, ...]]`; for the
      :code:`f` defined above, :code:`iv=[0, [1, 2]]` means that :code:`y[0](0) = 1` and
      :code:`y[1](0) = 2`

    - ``time_steps`` -- :code:`int`; the number of steps to take (will return time_steps + 1
      points)

    - ``time_range`` -- :code:`list[int]`; the range over which the time_steps should be taken;
      given as a two element list :code:`[beginning_time, ending_time]`

    - ``min_final_step`` -- Number

    OUTPUT: a list of two-tuples, each two-tuple having the format (time, ndarray of variable values)
    """
    from sage.functions.generalized import sgn
    from scipy.integrate import ode

    # set up the integrator
    r = ode(rhs).set_integrator("zvode")

    # set up parameters:
    r.set_initial_value(iv[1], iv[0])
    initial, final = time_range
    dt = float(final - initial) / time_steps
    # due to floating point arithmetic, it may be that
    #           initial+(time_steps-1)*dt > final-dt;
    # if not withint dt/time_steps of final, take one last step
    if min_final_step is None:
        min_final_step = dt / time_steps

    pts = Path(r.t, iv[1])
    # using sgn(dt) * () > 0 allows for both positive and negative steps
    while r.successful() and sgn(dt) * (final - (r.t + dt)) > 0:
        pts.append(r.t + dt, r.integrate(r.t + dt))
    if sgn(dt) * (final - r.t) > min_final_step:
        pts.append(final, r.integrate(final))
    return pts
