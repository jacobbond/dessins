import re

# isort: split
########## Sage imports ##########
from sage.rings.complex_mpfr import ComplexField
from sage.rings.complex_mpfr import create_ComplexNumber as ComplexNumber
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing

##################################

# isort: split
########## this package ##########
from ..utils import bits_to_digits, digits_to_bits, enlist

##################################


##################
#                #
#  Solver Class  #
#                #
##################


class Solver:
    r"""
    EXAMPLES::

    Use :class:`Solver` to find preimages of 0 ::

        sage: from dessins import BelyiMap, Solver
        sage: nu_3 = BelyiMap((x**3 + 1) ** 2 / (x**3 - 1) ** 2)
        sage: Solver(nu_3.numerator(), x).solve()
        [(-1.00000000000000,),
         (0.500000000000000000 - 0.866025403784438600*I,),
         (0.50000000000000000 + 0.86602540378443860*I,)]
        sage: Solver(nu_3.numerator(), x, multiplicities=True).solve()
        [(-1.00000000000000,),
         (-1.00000000000000,),
         (0.500000000000000000 - 0.866025403784438600*I,),
         (0.500000000000000000 - 0.866025403784438600*I,),
         (0.50000000000000000 + 0.86602540378443860*I,),
         (0.50000000000000000 + 0.86602540378443860*I,)]

    Use :class:`Solver` to find preimages of 1.  Note that :class:`Solver` does not find the preimage at :math:`\infty` ::

        sage: Solver(nu_3.numerator() - nu_3.denominator(), x, multiplicities=True).solve()
        [(0.000000000000000,), (0.000000000000000,), (0.000000000000000,)]

    Use :class:`Solver` to find preimages of :math:`\infty` ::

        sage: Solver(nu_3.denominator(), x, multiplicities=True).solve()
        [(1.00000000000000,),
        (1.00000000000000,),
        (-0.50000000000000000 + 0.86602540378443860*I,),
        (-0.50000000000000000 + 0.86602540378443860*I,),
        (-0.500000000000000000 - 0.866025403784438600*I,),
        (-0.500000000000000000 - 0.866025403784438600*I,)]

    Use :class:`Solver` to find preimages of :math:`\frac{1}{2}` ::

        sage: Solver(nu_3.numerator() - 0.5 * nu_3.denominator(), x, multiplicities=True).solve()
        [(-1.7996323451519970,),
         (-0.555669052456122500,),
         (0.27783452622806120 + 0.48122351552382990*I,),
         (0.277834526228061200 - 0.481223515523829900*I,),
         (0.89981617257599870 - 1.5585273283737950*I,),
         (0.89981617257599870 + 1.5585273283737950*I,)]
    """

    def __init__(
        self,
        eqns,
        varis,
        interface="singular",
        prec=53,
        solution_dict=False,
        multiplicities=False,
    ):
        self._eqns, self._variables = enlist(eqns), enlist(varis)
        self._solver, self._dict = interface, solution_dict
        self._prec, self._multiplicities = prec, multiplicities

    def solve(self):
        if self._solver == "singular":
            self._sols = roots_from_singular(
                self._eqns,
                self._variables,
                prec=self._prec,
                multiplicities=self._multiplicities,
            )
        elif self._solver == "math" or self._solver == "mathematica":
            self._sols = roots_from_mathematica(
                self._eqns,
                self._variables,
                {"WorkingPrecision": bits_to_digits(self._prec)},
            )
            if self._multiplicities is False:
                self._sols = list(set(self._sols))
        elif self._solver == "bertini" or self._solver == "bertini2":
            raise NotImplementedError(
                "Solving systems using Bertini2 " "is not yet implemented."
            )
        if self._dict is True:
            self._sols = [dict(zip(self._variables, sol)) for sol in self._sols]
        return self._sols

    def equations(self):
        return self._eqns

    def variables(self):
        return self._variables


##############
#            #
#  Singular  #
#            #
##############


def roots_from_singular(system, variables, **kwargs):
    from sage.functions.other import ceil
    from sage.interfaces.singular import SingularError, singular

    prec = kwargs.get("prec", 53)
    if prec is None:
        prec = 53
    digits = str(ceil(bits_to_digits(prec)))
    multiplicities = kwargs.get("multiplicities", False)

    try:
        R = PolynomialRing(ComplexField(), variables)
        [R(eqn) for eqn in system]
    except TypeError:
        raise TypeError("All equations in system must be complex polynomials.")

    var_str = str(tuple(variables)) if len(variables) > 1 else str(variables[0])
    # use regular expression to fix differences in scientific notation;
    # Singular does not allow ints to be raised to negative exponents:
    eq_strings = [re.sub(r"(?<=\d)e(?=-?\d+)", "*10.0^", str(eq)) for eq in system]

    singular.lib("solve.lib")
    try:
        r = singular.ring("(complex," + digits + ",I)", var_str, "dp")  # noqa: F841
        a = singular.ideal(*eq_strings)
        R = singular.solve(a, digits, int(multiplicities), digits)
        singular.setring(R)
        if multiplicities is True:
            sols = []
            for sols_of_mult, multi in singular("SOL"):
                if len(variables) == 1:
                    extender = [[sol] for sol in sols_of_mult for _ in range(multi)]
                else:
                    extender = [sol for sol in sols_of_mult for _ in range(multi)]
                sols.extend(extender)
        elif len(variables) == 1:
            #            sols = [[sol] for sol in singular('SOL')]
            # revert to laguerre_solve as there are currently multiple
            # formats for the results of solve depending on the ideal
            raise SingularError
        else:
            sols = singular("SOL")
    except (SingularError, TypeError):
        #        print("Warning: Please update Singular's triang.lib ("\
        #                "https://github.com/Singular/Sources/blob/spielwiese/"\
        #                "Singular/LIB/triang.lib).")
        r = singular.ring("(complex," + digits + ",I)", var_str, "lp")  # noqa: F841
        if len(variables) == 1 and len(system) > 1:
            raise ValueError(
                "Unable to solve a univariate system of "
                "multiple equations using the older version of "
                "Singular's triang.lib. (https://github.com/Singular/"
                "Sources/blob/spielwiese/Singular/LIB/triang.lib)."
            )
        elif len(variables) == 1:
            to_solver = [eq_strings[0]] + [digits] * 2 + [str(int(multiplicities))]
            sols = [[sol] for sol in singular.laguerre_solve(*to_solver)]
        else:
            if multiplicities is True:
                raise ValueError(
                    "Unable to compute multiplicities using the "
                    "older version of Singular's triang.lib ("
                    "https://github.com/Singular/Sources/blob/spielwiese/"
                    "Singular/LIB/triang.lib)."
                )
            a = singular.ideal(*eq_strings)
            R = singular.fglm_solve(a, digits)
            singular.setring(R)
            sols = singular("rlist")

    def singular_to_sage(num, prec):
        return ComplexNumber(num.repart(), num.impart(), min_prec=prec)

    return [tuple(singular_to_sage(coord, prec) for coord in sol) for sol in sols]


#################
#               #
#  Mathematica  #
#               #
#################


def roots_from_mathematica(system, variables, options=None):
    r"""
    Sends a system involving variables to Mathematica to be solved with
    NSolve.

    INPUT:

    - ``system`` -- the equations comprising the system (list)

    - ``variables`` -- the variables the system contains (list)

    - ``options`` -- the options to pass to NSolve (dictionary of strings)

    OUTPUT: a list of solutions as Sage numbers with precision as
      specified in options
    """
    from sage.functions.other import floor

    if options is None:
        opts = ""
        digits = 15  # Mathematica's $MachinePrecision is 15.95
    else:
        # leading comma separates options from other NSolve arguments
        # map(str, i) so that {'WorkingPrecision', 100} will work:
        opts = "," + (",".join(["->".join(map(str, i)) for i in options.items()]))
        digits = floor(options["WorkingPrecision"])
    var_str = ",".join(map(str, variables))

    # use regular expressions to format the system of equations
    # fix differences in scientific notation: ( an alternative
    # substitution: (\d+)e(-?)(\d+)  -->  \1*^\2\3 )
    eq_strings = [re.sub(r"(?<=\d)e(?=-?\d+)", "*^", str(eq)) for eq in system]
    # fix sqrt's:
    eq_strings = [re.sub(r"sqrt\(([^\)]*)\)", r"Sqrt[\1]", eq) for eq in eq_strings]
    sys = ",".join(eq_strings)

    tomath = (
        "{"
        + var_str
        + "}/.(Quiet[NSolve[{"
        + sys
        + "},{"
        + var_str
        + "}"
        + opts
        + "],NSolve::precw])"
    )
    return _mathnums_to_sage(tomath, digits)


def import_roots(roots, digits):
    r"""
    Takes a text file of comma separated numbers exported from
    Mathematica and reads them in as Sage numbers with :code:`digits` digits.
    The file is intended to be the result of Export['roots', sols[[i]]]
    where roots ends in .csv and sols is the output of NSolve[].

    INPUT:

    - ``roots`` -- the filename of a text file of Mathematica numbers

    - ``digits`` -- the number of digits to include in the Sage numbers

    OUTPUT: a list of Sage numbers with digits digits
    """
    from sage.interfaces.mathematica import mathematica

    root_list = []
    with open(roots, "r") as infile:
        for line in infile:
            root = line.split("->")[1].strip()
            prec = mathematica("Precision[" + root + "]")
            if prec < digits:
                raise ValueError(
                    "Root is given to only %g digits of " "preicison" % prec
                )
            # {{}} for compatibility with _mathnums_to_sage:
            tomath = "N[{{" + root + "}}," + str(digits) + "]"
            # [0][0] to undo the {{}}
            root_list.append(_mathnums_to_sage(tomath, digits)[0][0])
    return root_list


def _mathnums_to_sage(mathnums, digits):
    r"""
    Takes a list (or a list of lists) of Mathematica numbers and returns
    Sage numbers with :code:`digits` digits.

    INPUT:

    - ``mathnums`` -- a string representing a Mathematica list of numbers (or
        list of lists of numbers)

    - ``digits`` -- the number of digits to which each number should be given

    OUTPUT: a list of lists of Sage numbers
    """
    from sage.interfaces.mathematica import mathematica
    from sage.misc.sage_eval import sage_eval

    # take the real and imaginary part of each mathnum, and express it
    # without using scientific notation; the {3} argument to map
    # instructs to apply at the third level; the /.nums-> is to avoid
    # calling NSolve twice
    tomath = (
        "Map[ToString[NumberForm[#,ExponentFunc"
        "tion->(Null&)]]&,Map[Transpose,MapThread[List,({Re[nums],"
        "Im[nums]}/.nums->(" + mathnums + "))]],{3}]"
    )
    # pass to Mathematica, get the repr, and remove any newlines
    frommath = repr(mathematica(tomath)).replace("\n", "")
    # deals with how Mathematica handles 0
    frommath = re.sub(r"SetAccuracy\[0,.*\]", "0", frommath)
    # replace the {} for a Mathemaica list with []
    tosage = sage_eval(frommath.replace("{", "[").replace("}", "]"))
    num_list = [
        tuple(ComplexNumber(r, c, min_prec=digits_to_bits(digits)) for r, c in nums)
        for nums in tosage
    ]
    return num_list
