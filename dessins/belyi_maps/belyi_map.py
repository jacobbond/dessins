r"""
|Belyi| maps and their properties

Enables conversion between :class:`.BelyiMap` and other representations of
dessins d'enfants, plotting |Belyi| maps, and working with the basic properties
of |Belyi| maps.

EXAMPLES::

    sage: from dessins import BelyiMap
    sage: y = var("y")
    sage: b = BelyiMap(x**2 / (x**2 - 1), y**2 - x**3 + x)
    sage: b.numerator()
    x^2
    sage: b(1)
    Infinity
    sage: (b.degree, b.genus)
    (4, 1)
    sage: b.edge_labelling()
    {(-1.00000000000000*I, -1.00000000000000 - 1.00000000000000*I): 0,
     (-1.00000000000000*I, 1.00000000000000 + 1.00000000000000*I): 1,
     (1.00000000000000*I, -1.00000000000000 + 1.00000000000000*I): 2,
     (1.00000000000000*I, 1.00000000000000 - 1.00000000000000*I): 3}
    sage: b.conjugates()
    [Belyi map defined by x^2/(x^2 - 1) on Elliptic Curve defined by y^2 = x^3 - x over Rational Field]

AUTHORS:

- Jacob Bond: initial implementation

.. |Belyi| unicode:: Bely U+012D .. Belyi
"""

# check if this is being imported by a Sage or Python script
import sys  # isort:skip

if ".py" in sys.argv[0] or ".sage" in sys.argv[0]:
    import sage.all  # noqa: F401

# isort: split
########## Sage imports ##########
from sage.calculus.calculus import limit
from sage.calculus.var import var
from sage.categories.map import Map
from sage.plot.point import point
from sage.rings.cc import CC
from sage.rings.complex_mpfr import ComplexField
from sage.rings.fraction_field import FractionField
from sage.rings.infinity import Infinity
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.qqbar import QQbar
from sage.rings.rational_field import QQ
from sage.structure.element import parent
from sage.symbolic.ring import SR

##################################

##############
#            #
#  BelyiMap  #
#            #
##############


class BelyiMap:
    r"""
    The |Belyi| map defined by a rational function and a Riemann surface.

    INPUT:

    - ``belyi`` -- rational function; The rational function defining the |Belyi| map

    - ``curve`` -- :code:`None`, ``str``, Polynomial, EllipticCurve, HyperellipticCurve
      (default: :code:`None`); The Riemann surface the |Belyi| map is defined on

    - ``base_field`` -- (default: :code:`None`); The base field that the coefficients of
      ``belyi`` are defined over

    - ``embedding`` -- (default: :code:`None`); A root of the minimal polynomial of the
      generator for ``base_field``

    - ``verify`` -- (default: :code:`False`); Whether the verify if ``belyi`` is a |Belyi|
      map (only supported for genus 0 |Belyi| maps)

    EXAMPLES::

    A :class:`.BelyiMap` (genus 0, defined over \\QQ) can be constructed from a rational
    function which is a member of ``SR``: ::

        sage: from dessins import BelyiMap
        sage: b = BelyiMap((x+1)^3*(x+9)/(64*x))
        sage: c = b.constellation(); c
        ((2,3,4), (1,2)(3,4), (1,2,3))

    Numerical approximations (with floating point coefficients in a ``ComplexField``) can also be
    constructed by providing a rational function: ::

        sage: BelyiMap(
        ....:     (0.008015463*x^5 - 0.05529851*x^4 - 0.01762993*x^3 + 0.3590559*x^2 + 0.5097303*x + 0.1963584)/x
        ....: )
        Belyi map defined by (6416198/800477527*x^5 - 5529851/100000000*x^4 - 1762993/100000000*x^3 + 3590559/10000000*x^2 + 5097303/10000000*x + 30681/156250)/x on Projective Space of dimension 1 over Rational Field

    Because the ``base_field`` argument wasn't supplied, :class:`.BelyiMap` assumes the base
    field is \\QQ.  By specifying the base field, :class:`.BelyiMap` will use the correct parent
    for the coefficients: ::

        sage: BelyiMap(
        ....:     (0.008015463*x^5 - 0.05529851*x^4 - 0.01762993*x^3 + 0.3590559*x^2 + 0.5097303*x + 0.1963584)/x,
        ....:     base_field=ComplexField(26)
        ....: )
        Belyi map defined by (0.008015463*x^5 - 0.05529851*x^4 - 0.01762993*x^3 + 0.3590559*x^2 + 0.5097303*x + 0.1963584)/x on Projective Space of dimension 1 over Complex Field with 26 bits of precision

    When the |Belyi| map is not defined over \\QQ (or a ``ComplexField``) a complex embedding for the generator
    of the field of coefficients must be specified.  This can be done by providing either an
    image of the generator: ::

        sage: K = NumberField(x**2 - 3, "nu"); nu = K.gen()
        sage: R = PolynomialRing(K, "t"); t = R.gen()
        sage: BelyiMap(
        ....:    (t^6-2*t^5+t^4)/(t^6-2*t^5+t^4+1/9*(2*nu+3)*t^2+1/27*(-8*nu-14)*t+1/243*(26*nu+45)),
        ....:     embedding=x.substitute(solve(x**2 - 3, x)[0]),
        ....: )
        Belyi map defined by (t^6 - 2*t^5 + t^4)/(t^6 - 2*t^5 + t^4 + (2/9*nu + 1/3)*t^2 + (-8/27*nu - 14/27)*t + 26/243*nu + 5/27) on Projective Space of dimension 1 over Number Field in nu with defining polynomial x^2 - 3, with nu embedded as -1.732050807568878?

    or an embedding of the base field: ::

        sage: BelyiMap(
        ....:     (t^6-2*t^5+t^4)/(t^6-2*t^5+t^4+1/9*(2*nu+3)*t^2+1/27*(-8*nu-14)*t+1/243*(26*nu+45)),
        ....:     embedding=K.embeddings(QQbar)[0]
        ....: )
        Belyi map defined by (t^6 - 2*t^5 + t^4)/(t^6 - 2*t^5 + t^4 + (2/9*nu + 1/3)*t^2 + (-8/27*nu - 14/27)*t + 26/243*nu + 5/27) on Projective Space of dimension 1 over Number Field in nu with defining polynomial x^2 - 3, with nu embedded as -1.732050807568878?

    For |Belyi| maps with positive genus, the curve can be specified either as a polynomial, an equation string,
    or an ``EllipticCurve`` or ``HyperellipticCurve``: ::

        sage: y = var("y")
        sage: gamma = BelyiMap(x**2 / (x**2 - 1), y**2 - x**3 + x); gamma
        Belyi map defined by x^2/(x^2 - 1) on Elliptic Curve defined by y^2 = x^3 - x over Rational Field
        sage: BelyiMap(x**2 / (x**2 - 1), "y**2 = x**3 - x")
        Belyi map defined by x^2/(x^2 - 1) on Elliptic Curve defined by y^2 = x^3 - x over Rational Field
        sage: BelyiMap(x**2 / (x**2 - 1), EllipticCurve([0, 0, 0, -1, 0]))
        Belyi map defined by x^2/(x^2 - 1) on Elliptic Curve defined by y^2 = x^3 - x over Rational Field
        sage: BelyiMap((x**3 + y)/(2*x**3), y**2 - x**6 + 2)
        Belyi map defined by (x^3 + y)/(2*x^3) on Hyperelliptic Curve over Rational Field defined by y^2 = -x^6 + 2

    Belyi maps can be composed: ::

        sage: from dessins import Passport
        sage: p = Passport([4,1], [4,1], [2,2,1])
        sage: beta = p.belyi_maps(d=5, h=500, subs_dict={var("a0"): 0, var("a1"): -1})[0]
        ...
        Found 1 Galois orbits.
        sage: beta_gamma = beta(gamma)
        sage: beta_gamma.embedding() == beta.embedding()
        True
        sage: beta_gamma.function()
        ((3/8*a^3 + 35/4*a^2 - 117/4*a + 41/2)*x^8)/(x^10 + (1/4*a^3 + 5*a^2 - 17*a + 12)*x^8 + (-24*a^3 + 81*a^2 - 81*a + 19)*x^6 + (118*a^3 - 316*a^2 + 293/2*a + 108)*x^4 + (-165*a^3 + 369*a^2 + 18*a - 326)*x^2 + 283/4*a^3 - 139*a^2 - 133/2*a + 186)
        sage: beta_gamma.curve()
        Elliptic Curve defined by y^2 = x^3 - x over Rational Field


    .. NOTE::

        This class has the following attributes:

        - ``_curve`` -- Curve; The curve defining the Belyi map's domain.
        - ``_curve_poly`` -- Polynomial; The polynomial that defines the curve.
        - ``_curve_vars`` -- Tuple; The variables used to define the curve.
        - ``_embedding`` -- ComplexNumber; A specific root of the minimal polynomial for the Belyi map's field of definition.
        - ``_extending_pattern`` -- ExtendingPattern; The extending pattern of the Belyi map.
        - ``_map`` -- FractionFieldElement; The function defining the Belyi map.
        - ``_map_vars`` -- Tuple; The variables used to define the function.
        - ``_genus0`` -- bool; Whether the Belyi map has genus 0.
        - ``_monodromy`` -- Monodromy; The monodromy of the Belyi map.
        - ``_loop_lifts`` -- Sequence of points defining loops around 0 and 1 lifted by BelyiMap.

    """

    def __init__(
        self, belyi, curve=None, base_field=None, embedding=None, verify=False
    ):
        self._map_vars = _handle_map_variables(belyi)

        (
            self._map,
            self._base_field,
            self._base_field_gen,
        ) = _handle_map(belyi, base_field, self._map_vars)

        self._embedding = _handle_embedding(
            self._map, embedding, self._base_field, self._map_vars
        )

        if curve is None:
            from sage.schemes.projective.projective_space import ProjectiveSpace

            self._curve = ProjectiveSpace(1, self._base_field)
            self._curve_poly = None
            self._curve_vars = tuple([])
            self._genus0 = True
        else:
            if isinstance(curve, str):
                curve = _parse_curve_string(curve, self._base_field_gen)

            (
                self._curve,
                self._curve_poly,
                self._curve_vars,
                self._genus0,
            ) = _handle_curve(curve)

            if parent(self._curve) == SR:
                self._curve = PolynomialRing(self._base_field, self._curve_vars)(
                    self._curve
                )

        # standardize all parent rings:
        self._map_parent = FractionField(
            PolynomialRing(self._base_field, self._map_vars)
        )
        self._map_vars = self._map_parent.gens()
        self._map = self._map_parent(self._map)
        self._curve_parent = PolynomialRing(self._base_field, self._curve_vars)
        self._curve_vars = self._curve_parent.gens()
        self._curve_poly = self._curve_parent(self._curve_poly)
        self._belyi_parent = FractionField(
            PolynomialRing(
                self._base_field,
                self._curve_vars if not self._genus0 else self._map_vars,
            )
        )

        if verify is True and self._genus0:
            if not verify_belyi_map(belyi):
                raise ValueError("The input function is not a Belyi map.")
        elif verify is True:
            raise NotImplementedError(
                "Belyi map verification is currently only support for genus 0 maps."
            )

    # TODO: refactor this
    def __call__(self, sub_dict):
        if isinstance(sub_dict, BelyiMap):
            if not self.is_dynamical():
                raise ValueError(
                    "Composing self with a Belyi map won't be another Belyi map."
                )
            return BelyiMap(
                self.function()(self._map_parent(sub_dict)),
                embedding=self.embedding(),
                curve=sub_dict.curve(),
            )
        elif len(self._map_vars) == 1 and not isinstance(sub_dict, dict):
            if sub_dict is Infinity:
                return limit(SR(self._map), **{str(self._map_vars[0]): Infinity})
            try:
                return SR(self._map).substitute({self._map_vars[0]: sub_dict})
            except ValueError:
                return limit(SR(self._map), **{str(self._map_vars[0]): sub_dict})
        else:
            mv_ring = self._map.parent()
            sub_dict = {mv_ring(k): v for k, v in sub_dict.items()}
            return self._map.substitute(sub_dict)

    def __str__(self):
        if self._embedding is not None:
            field_gen = self.defining_field().gen()
            embed_text = (
                ", " + str(field_gen) + "|-->" + str(self._embedding(field_gen))
            )
        else:
            embed_text = ""
        return "[" + str(self._map) + ", " + str(self._curve) + embed_text + "]"

    def __repr__(self):
        if not (self._base_field is QQ or isinstance(self._base_field, type(CC))):
            field_gen = self.defining_field().gen()
            embed_text = ", with {} embedded as {}".format(
                repr(field_gen), repr(self._embedding(field_gen))
            )
        else:
            embed_text = ""
        return f"Belyi map defined by {self._map} on {repr(self._curve)}" + embed_text

    def curve_poly(self):
        return self._curve_poly

    def variables(self):
        if self.genus == 0:
            return self._map_vars
        else:
            # maintain variable ordering:
            addl_vars = [
                vari for vari in self._curve_vars if vari not in self._map_vars
            ]
            return self._map_vars + tuple(addl_vars)

    def function(self):
        return self._map

    def numerator(self):
        return self._map.numerator()

    def denominator(self):
        return self._map.denominator()

    @property
    def degree(self):
        return len(self.edge_labelling())
        # return max(
        #     self.numerator().degree,
        #     self.denominator().degree,
        # )

    # a synonym for curve()
    def domain(self):
        return self._curve

    def curve(self):
        return self._curve

    def embedding(self):
        return self._embedding(self._base_field.gen())

    def embedding_into(self, codomain=CC):
        from sage.structure.coerce_maps import CallableConvertMap

        dom = parent(self._map)

        if self._embedding is None:

            def embed(a):
                return a

        else:
            def_field = self._base_field
            gen = def_field.gen()
            embeds = def_field.embeddings(codomain)
            embed = min(embeds, key=lambda m: abs(m(gen) - self._embedding(gen)))
        cod = PolynomialRing(codomain, dom.gens()).fraction_field()

        def convert(f):
            num = cod(f.numerator().map_coefficients(embed, new_base_ring=codomain))
            # cod(num/denom) uses num/denom and 1 as numerator and
            # denominator and so fails to coerce
            return num / cod(
                f.denominator().map_coefficients(embed, new_base_ring=codomain)
            )

        return CallableConvertMap(dom, cod, convert, parent_as_first_arg=False)

    def conjugates(self):
        r"""
        EXAMPLES::

            sage: from dessins import BelyiMap
            sage: K = NumberField(x**2 - 3, "nu"); nu = K.gen()
            sage: R = PolynomialRing(K, "t"); t = R.gen()
            sage: b = BelyiMap(
            ....:    (t^6-2*t^5+t^4)/(t^6-2*t^5+t^4+1/9*(2*nu+3)*t^2+1/27*(-8*nu-14)*t+1/243*(26*nu+45)),
            ....:     embedding=x.substitute(solve(x**2 - 3, x)[0]),
            ....: )
            sage: [f.embedding() for f in b.conjugates()]
            [-1.732050807568878?, 1.732050807568878?]
        """
        from sage.rings.qqbar import QQbar

        return [
            BelyiMap(self._map, self.curve(), self.base_field(), embedding)
            for embedding in self.base_field().embeddings(QQbar)
        ]

    def defining_field(self):
        return self._base_field

    def base_ring(self):
        return self._base_field

    def base_field(self):
        return self._base_field

    @property
    def genus(self):
        if self._genus0:
            return 0
        else:
            return self._curve.genus()

    def is_dynamical(self):
        if self.genus != 0:
            return False

        R = PolynomialRing(self._base_field, self._map_vars[0])
        num, den = R(self._map.numerator()), R(self._map.denominator())

        zero_q = num(0) == 0 or den(0) == 0 or num(0) == den(0)
        one_q = num(1) == 0 or den(1) == 0 or num(1) == den(1)
        inf_q = num.degree() != den.degree() or num.lc() == den.lc()
        return all([zero_q, one_q, inf_q])

    def constellation(self, algorithm="preimages", cache=True):
        if cache is False and hasattr(self, "_monodromy"):
            del self._monodromy

        try:
            return self._monodromy
        except AttributeError:
            from .monodromy import monodromy

            self._monodromy = monodromy(self, algorithm=algorithm, cache=cache)
            return self._monodromy

    # a synonym for constellation()
    def monodromy(self, algorithm="preimages", cache=True):
        return self.constellation(algorithm=algorithm, cache=cache)

    def monodromy_group(self):
        return self.constellation().monodromy_group()

    def monodromy_representation(self):
        return self.constellation().monodromy_representation()

    def passport(self):
        try:
            monodromy = self._monodromy
        except AttributeError:
            monodromy = self.constellation()
        return monodromy.passport()

    # this returns an element in the codomain of convert_map, but the
    # result should be truncated to the result of coeff.n
    def numerical_approx(self, prec=53, digits=None):
        if prec == 53 and self.domain().base() is CC:
            return self.function()

        if digits is not None:
            from ..utils import digits_to_bits

            prec = digits_to_bits(digits)
        return self.embedding_into(ComplexField(prec))(self._map)

    # a synonym for numerical_approx()
    def n(self, prec=53, digits=None):
        return self.numerical_approx(prec, digits)

    def substitute(self, sub_dict):
        mv_ring = self._map.parent()
        sub_dict = {mv_ring(k): v for k, v in sub_dict.items()}
        return self._map.substitute(sub_dict)

    def plot(self, output=None, pts_per_edge=30, prec=53, **kwargs):
        r"""
        Plots ``self`` using ``pts_per_edge`` sample points for each
        edge and saves the result to ``output``.
        """
        from copy import deepcopy

        if hasattr(self, "_plot") and output is None:
            p = deepcopy(self._plot)
            return p.show(**kwargs)
        elif hasattr(self, "_plot"):
            p = deepcopy(self._plot)
            p.save_image(output, **kwargs)
            return None

        if self.genus != 0:
            raise NotImplementedError(
                "Plotting of Belyi maps is only implemented for genus zero."
            )
        from sage.plot.plot import parametric_plot
        from scipy.interpolate import interp1d

        # TODO: cache the edges here
        # TODO: check whether an edge goes through infinity
        paths = _compute_edges(self, pts_per_edge, prec)

        plots = []
        points = []
        t = var("t")
        for path_pts in paths:
            # format for numpy
            path_pts = [(time, complex(value[0])) for time, value in path_pts]
            path_func = interp1d(*zip(*path_pts), fill_value="extrapolate", axis=0)

            # functions to separate real and imaginary parts (necessary
            # for parametric_plot):
            re, im = lambda t: path_func(t).real, lambda t: path_func(t).imag
            plots.append(parametric_plot((re, im), (t, 0, 1), **kwargs))
            points.append(
                point(
                    (path_func(0).real.item(), path_func(0).imag.item()),
                    rgbcolor="black",
                    markeredgecolor="black",
                    size=50,
                )
            )
            points.append(
                point(
                    (path_func(1).real.item(), path_func(1).imag.item()),
                    rgbcolor="white",
                    markeredgecolor="black",
                    size=50,
                )
            )

        # TODO: add single points over 0 and 1
        # TODO: consider using matplotlib
        self._plot = sum(plots + points)
        if output is None:
            return self._plot
        else:
            self._plot.save(output)

    def edge_labelling(self):
        try:
            return self._preimage_dict
        except AttributeError:
            from .solver import Solver

            # TODO: this is wrong
            # try:
            #     R = parent(self.numerator()).change_ring(base_ring=CC)
            # except TypeError:
            #     R = parent(self.numerator()).change_ring(CC)
            # except TypeError:
            #     R = SR

            approx = self.n()
            eqns = approx.numerator() - CC(0.5) * approx.denominator()
            # eqns = R(self.numerator()) - R(0.5) * R(self.denominator())
            if not self._genus0:
                eqns = [eqns, self._curve_poly]

            preimages = Solver(
                eqns,
                self.variables(),
            ).solve()
            self._preimage_dict = {
                pt: label for label, pt in enumerate(sorted(preimages))
            }
            return self._preimage_dict

    def relabel_edges(self, relabels: dict):
        for key, val in self.edge_labelling().items():
            if val in relabels:
                self._preimage_dict[key] = relabels[val]
        if hasattr(self, "_monodromy"):
            del self._monodromy
        if hasattr(self, "_extending_pattern"):
            del self._extending_pattern
        if hasattr(self, "_extended_monodromy"):
            del self._extended_monodromy

    def extending_pattern(self, algorithm="odes", cache=True):
        if cache is False and hasattr(self, "_extending_pattern"):
            del self._extending_pattern

        try:
            return self._extending_pattern
        except AttributeError:
            from .extending_pattern import extending_pattern

            self._extending_pattern = extending_pattern(
                self, algorithm=algorithm, cache=cache
            )
            return self._extending_pattern

    def extended_monodromy(self):
        try:
            return self._extended_monodromy
        except AttributeError:
            from ..constellations.composition import ExtendedMonodromy

            self._extended_monodromy = ExtendedMonodromy(
                self.extending_pattern(),
                self.constellation(),
            )
            return self._extended_monodromy


def _compute_edges(belyi, pts_per_edge, prec):
    try:
        return belyi._edge_pts
    except AttributeError:
        pass

    from .lifting import _lift_to_preimages

    # TODO: add option to use ODEs to compute edges, still gluing at
    # the midpoint
    paths_toward_0 = _lift_to_preimages(
        belyi, u(var("t")), [0.5, 0], pts_per_edge, prec
    )
    paths_toward_1 = _lift_to_preimages(
        belyi, u(var("t")), [0.5, 1], pts_per_edge, prec
    )
    gluing_dict = {path.start_point: path for path in paths_toward_1}
    # use path_dict to find path from paths_toward_1 with same 1st pt
    # TODO: sort lexicographically on the path midpoint
    edges = [
        p0.reverse().concatenate(gluing_dict[p0.start_point][1:])
        for p0 in paths_toward_0
    ]

    belyi._edge_pts = edges
    return edges


def u(t):
    """
    This function is used to more evenly disperse the preimages of
    [0, 1] along each edge of a Belyi map.
    """
    return -(t**6) * (
        -462 + 1980 * t - 3465 * t**2 + 3080 * t**3 - 1386 * t**4 + 252 * t**5
    )


###############################
#                             #
#  __init__ helper functions  #
#                             #
###############################


def _handle_map_variables(belyi):
    # find the variables used in belyi
    try:
        map_vars = belyi.numerator().variables()
    except AttributeError:
        map_vars = belyi.variables()
    try:
        denom_vars = belyi.denominator().variables()
    except AttributeError:  # denominator is a scalar
        denom_vars = tuple([])
    # maintain variable ordering:
    addl_vars = [vari for vari in denom_vars if vari not in map_vars]
    return map_vars + tuple(addl_vars)


def _handle_map(belyi, base_field, map_vars):
    # TODO: handle when parent is SR better
    # TODO: ensure the map is a member of a fraction field
    if base_field is QQbar:
        raise ValueError("Base field of Belyi map cannot be QQbar.")
    elif base_field is None and parent(belyi) is SR:
        try:
            PolynomialRing(QQ, map_vars).fraction_field()(belyi)
            the_base_field = QQ
            base_field_gen = QQ(1)
        except TypeError:  # use CC if in SR, but not defined over QQ
            # TODO: is there a better way to handle when belyi has e.g. radical
            # coefficients?
            # the_map = R(belyi.numerator()) / R(belyi.denominator())
            the_base_field = CC
            base_field_gen = CC(1)
    elif base_field is None:
        try:
            the_base_field = parent(belyi).base_ring().constant_field()
        except AttributeError:
            the_base_field = parent(belyi).base_ring()
        base_field_gen = the_base_field.gen()
    elif hasattr(base_field, "degree") and base_field.degree() == 1:
        the_base_field = QQ
        base_field_gen = QQ(1)
    else:
        the_base_field = base_field
        base_field_gen = the_base_field.gen()
    R = PolynomialRing(the_base_field, map_vars).fraction_field()
    return R(belyi), the_base_field, base_field_gen


def _handle_embedding(belyi, embedding, base_field, vars):
    if embedding in QQbar:
        embedding_map = base_field.hom([embedding], QQbar)
    elif embedding in CC:
        embedding_map = min(
            base_field.embeddings(QQbar),
            key=lambda embed: abs(embed(base_field.gen()) - embedding),
        )
    elif isinstance(embedding, Map):
        embedding_map = embedding
    elif embedding is None:
        try:
            # Sage is better at coercing num and denom separately
            R = PolynomialRing(QQ, vars)
            R(belyi.numerator()) / R(belyi.denominator())
            embedding_map = QQ.hom(QQ)
        except TypeError:
            R = PolynomialRing(CC, vars)
            R(belyi.numerator()) / R(belyi.denominator())
            embedding_map = CC.hom(CC)
        except TypeError:
            raise ValueError(
                "If no embedding is supplied, Belyi map must be defined over QQ or be a numerical approximation."
            )
    else:
        raise TypeError(
            "Embedding should be either an approximate root of the minimal "
            "polynomial of the defining field or a complex embedding of the "
            "defining field."
        )
    return embedding_map


def _parse_curve_string(curve, base_field_gen):
    from string import ascii_letters

    from sage.misc.sage_eval import sage_eval

    curve_vars = set(
        ch for ch in curve if ch in ascii_letters and ch not in str(base_field_gen)
    )
    curve_vars = tuple(curve_vars)
    R = PolynomialRing(parent(base_field_gen), ",".join(curve_vars))

    locals_dict = {v: R(v) for v in curve_vars} | {str(base_field_gen): base_field_gen}
    the_curve = sage_eval(
        "-(".join(curve.split("=")) + ")",
        locals=locals_dict,
    )

    indep_var = max(the_curve.variables(), key=lambda v: the_curve.degree(v))
    if indep_var != R(curve_vars[0]):
        S = PolynomialRing(parent(base_field_gen), ",".join(curve_vars[::-1]))
        # map the variables to themselves, but change the parent ring:
        hom = R.hom(R.gens(), S)
        the_curve = hom(the_curve)

    return the_curve


def _handle_curve(curve):
    from sage.schemes.elliptic_curves.ell_generic import is_EllipticCurve
    from sage.schemes.hyperelliptic_curves.hyperelliptic_generic import (
        is_HyperellipticCurve,
    )
    from sage.schemes.projective.projective_space import is_ProjectiveSpace

    if is_ProjectiveSpace(curve) and curve.dimension() != 1:
        raise ValueError(
            "ProjectiveSpace which is the domain of a Belyi map must have dimension 1."
        )
    elif is_ProjectiveSpace(curve):
        return curve, None, tuple([]), True
    elif is_EllipticCurve(curve):
        polys = curve.hyperelliptic_polynomials()
        # EllipticCurve forces the use of x and y as variables;
        # however, use this to get the right ambient rings for variables
        PP = curve.ambient_space()
        projective_embedding = PP.affine_patch(2).projective_embedding(2, PP)
        plane_coords = projective_embedding.defining_polynomials()
        curve_poly = curve.defining_polynomials()[0](plane_coords)
        return curve, curve_poly, curve_poly.variables(), False
    elif is_HyperellipticCurve(curve):
        polys = curve.hyperelliptic_polynomials()
        # HyperellipticCurve uses x0, x1 by default for projective embedding,
        # but stores the provided names (or x, y if no names are given) in
        # curve._printing_ring
        curve_vars = (
            curve._printing_ring.base_ring().gen(),
            curve._printing_ring.gen(),
        )
        curve_poly = curve_vars[1] ** 2 + polys[1] * curve_vars[1] - polys[0]
        return curve, curve_poly, curve_vars, False

    curve_degree = max([curve.degree(v) for v in curve.variables()])
    indep_var = max(curve.variables(), key=lambda v: curve.degree(v))
    dep_var = min(curve.variables(), key=lambda v: curve.degree(v))
    curve_degree = curve.degree(indep_var)
    try:
        polys = [curve.coefficient({dep_var: 0}), curve.coefficient({dep_var: 1})]
    except TypeError:  # if parent(curve) == SR
        polys = curve.coefficients(dep_var, sparse=False)[:2]

    from sage.schemes.elliptic_curves.constructor import EllipticCurve
    from sage.schemes.hyperelliptic_curves.constructor import HyperellipticCurve

    if curve_degree > 3:
        try:
            univariate_polys = [poly.univariate_polynomial() for poly in polys]
        except AttributeError:
            if parent(curve) == SR:
                R = PolynomialRing(QQ, indep_var)
            else:
                R = PolynomialRing(parent(curve).base_ring(), indep_var)
            univariate_polys = [R(poly) for poly in polys]
        curve_poly = (
            curve.variables()[1] ** 2
            + univariate_polys[1] * curve.variables()[1]
            - univariate_polys[0]
        )
        return (
            HyperellipticCurve(*univariate_polys),
            curve_poly,
            curve.variables(),
            False,
        )
    else:
        curve_poly = dep_var**2 + dep_var * polys[1] + polys[0]
        return EllipticCurve(curve_poly), curve_poly, curve.variables(), False


def verify_belyi_map(belyi, algorithm="sympy"):
    func_var = belyi.variables()[0]
    critical_points = belyi.derivative(func_var).solve(
        func_var, solution_dict=True, algorithm=algorithm
    )
    critical_points.extend(
        belyi.derivative(func_var)
        .denominator()
        .solve(func_var, solution_dict=True, algorithm=algorithm)
    )
    critical_values = set([])

    from sage.rings.infinity import UnsignedInfinityRing

    P = UnsignedInfinityRing
    # TODO: can this be handled better?
    for pt in critical_points:
        val = limit(belyi, **{str(func_var): pt[func_var]})
        if val in [0, 1]:
            critical_values.add(val)
        elif P(val) != P(Infinity):
            # critical_values.add(P(val))
            return False
    return critical_values.issubset(set([0, 1, P(Infinity)]))
