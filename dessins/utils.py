r"""
Utility functions for dessins d'enfants, including functions which make interfacing with GAP easier

EXAMPLES::

    sage: from dessins import coerce_perm, generic_permutation, GapIterator
    sage: generic_permutation([4,2,1,1,1])
    (4,5,6,7)(8,9)
    sage: type(coerce_perm("(1,2)(3,4)", "gap"))
    <class 'sage.libs.gap.element.GapElement_Permutation'>
    sage: from dessins import GapIterator
    sage: gap_iter = libgap.eval('Iterator( GF(5) )')
    sage: list( GapIterator(gap_iter) )
    [0*Z(5), Z(5)^0, Z(5), Z(5)^2, Z(5)^3]

AUTHORS:

- Jacob Bond: initial implementation
"""

import itertools as it
import operator as op
import re
from collections import deque
from typing import Iterable, Optional

# isort: split
########## Sage imports ##########
from sage.calculus.var import var
from sage.functions.log import function_log as log
from sage.groups.perm_gps.constructor import PermutationGroupElement
from sage.libs.gap.libgap import libgap
from sage.rings.infinity import Infinity
from sage.rings.real_double import RDF

##################################


####################################
#                                  #
#  Constellation helper functions  #
#                                  #
####################################


def coerce_perm(perm, parent):
    r"""
    Coerces a permutation to either a GAP or Sage instance.

    EXAMPLES::

        sage: from dessins import coerce_perm
        sage: s_gap = libgap.eval("(1,2)(3,4)")
        sage: s_sage = PermutationGroupElement([2,1,4,3])
        sage: type(coerce_perm("(1,2)(3,4)", "gap"))
        <class 'sage.libs.gap.element.GapElement_Permutation'>
        sage: type(coerce_perm("(1,2)(3,4)", "sage"))
        <class 'sage.groups.perm_gps.permgroup_element.SymmetricGroupElement'>
        sage: type(coerce_perm(s_sage, "gap"))
        <class 'sage.libs.gap.element.GapElement_Permutation'>
        sage: type(coerce_perm(s_gap, "sage"))
        <class 'sage.groups.perm_gps.permgroup_element.SymmetricGroupElement'>
        sage: type(coerce_perm([2,1,4,3], "gap"))
        <class 'sage.libs.gap.element.GapElement_Permutation'>
        sage: type(coerce_perm([2,1,4,3], "sage"))
        <class 'sage.groups.perm_gps.permgroup_element.SymmetricGroupElement'>
    """
    if parent == "gap" and not (isinstance(perm, str) or isinstance(perm, list)):
        return libgap(perm)
    elif parent == "gap" and isinstance(perm, str):
        if ",)" in perm:  # in case str((1,))
            perm = re.sub(r"\(\d*,\)", "", perm)
        if re.search(r"\(\d*\)", perm):  # handles (1)(2)(3)
            perm = re.sub(r"\(\d*\)", "", perm)
        if perm == "":  # combined with above, handles (1)
            return libgap.eval("()")
        return libgap.eval(perm)
    elif parent == "gap" and isinstance(perm, list):
        return libgap.PermList(perm)
    elif parent == "sage" and hasattr(perm, "sage"):
        # if perm is a GAP permutation, perm.sage() returns an instance
        # of the sage.combinat.permutation class
        return PermutationGroupElement(perm.sage())
    elif parent == "sage":
        if isinstance(perm, str) and ",)" in perm:  # in case str((1,))
            perm = re.sub(r"\(\d*,\)", "", perm)
        return PermutationGroupElement(perm)
    else:
        return perm


def gap_range(start, stop):
    r"""
    Rather than converting ``int``\ s to GAP objects one at a time,
    creates a range natively in GAP (for example, to use with :code:`IsTransitive`).

    EXAMPLES::

        sage: from dessins import gap_range
        sage: s, t = libgap.eval("(1,2)(3,4)"), libgap.eval("(2,3)")
        sage: G = libgap.Group(s, t)
        sage: G.IsTransitive(gap_range(1, 4))
        true
        sage: G.IsTransitive(gap_range(1, 5))
        false

    .. NOTE::

        To maintain consistency with GAP, the range includes its endpoint,
        which diverges from Sage and Python behavior.
    """
    return libgap.eval("[" + str(start) + ".." + str(stop) + "]")


def generic_permutation(cycle_t):
    r"""
    Given a cycle type :code:`cycle_t`, creates a permutation with that
    cycle type.  All singleton cycles are assigned the lowest integers to
    ensure the permutation belongs to the proper parent group.

    EXAMPLES::

        sage: from dessins import generic_permutation
        sage: generic_permutation([1,2,3,4])
        (2,3)(4,5,6)(7,8,9,10)
        sage: generic_permutation([4,2,1,1,1])
        (4,5,6,7)(8,9)
        sage: generic_permutation([1,1,1,1])
        ()

    .. NOTE::

        Due to GAP's representation of the identity permutation, for example,
        :code:`generic_permutation([1]*n)` returns :code:`()` and will not
        strictly belong to :code:`SymmetricGroup(n)`.
    """
    if all([t == 1 for t in cycle_t]):
        return libgap.eval("()")
    cycle_t_moved = [t for t in cycle_t if t != 1]
    num_ones = sum(cycle_t) - sum(cycle_t_moved)
    C = it.count(1 + num_ones)
    cycle = [tuple(it.islice(C, length)) for length in cycle_t_moved]
    to_gap = "".join(map(str, cycle))
    return libgap.eval(to_gap)


###############
#             #
#  Iterators  #
#             #
###############


# from the Recipes section of itertools documentation
# helps for timing generation of very large iterators
def consume(iterator: Iterable, n: Optional[int] = None):
    r"""
    Advance the iterator n-steps ahead. If n is none, consume entirely.

    From the `itertools <https://docs.python.org/3/library/itertools.html#itertools-recipes>`_ recipes,
    ``consume`` provides a useful way to time iterators without incurring the overhead
    of creating a list.

    INPUT:

    - ``iterator`` -- ``Iterable``; The iterator to be consumed

    - ``n`` -- ``Optional[int]``; Number of steps ``iterator`` should be advanced.  If ``None``
      the ``iterator`` is consumed entirely

    EXAMPLES::

        sage: timeit("consume(passport_to_constellations([8,2,1,1], [8,2,1,1], [10,2]))")    # not tested
        5 loops, best of 3: 17.5 s per loop
        sage: timeit("list(passport_to_constellations([8,3,1], [9,2,1], [11,1])))")    # not tested
        5 loops, best of 3: 19.2 s per loop
    """
    # Use functions that consume iterators at C speed.
    if n is None:
        # feed the entire iterator into a zero-length deque
        deque(iterator, maxlen=0)
    else:
        # advance to the empty slice starting at position n
        next(it.islice(iterator, n, n), None)


class GapIterator:
    r"""
    A class for iterating over ``GapIterator``\ s

    EXAMPLES::

        sage: from dessins import GapIterator
        sage: gap_iter = libgap.eval('Iterator( GF(5) )')
        sage: list( GapIterator(gap_iter) )
        [0*Z(5), Z(5)^0, Z(5), Z(5)^2, Z(5)^3]
    """

    def __init__(self, iterator):
        self.iterator = iterator

    def __iter__(self):
        return self

    def __next__(self):
        try:
            return self.iterator.NextIterator()
        except ValueError:
            raise StopIteration


class UniqueConstellationIterator:
    r"""
    A class to create an iterator of unique constellations from a sequence of
    nonunique constellations.

    INPUT:

    - ``iterator`` -- Iterable of ``Constellation3``\ s; The iterable from which to generate the unique constellations

    EXAMPLES::

        sage: from dessins import UniqueConstellationIterator, passport_to_constellations
        sage: list( UniqueConstellationIterator([
        ....:     ('()', '()', '()'), ('()', '()', '()')
        ....: ]) )
        [((), (), ())]
        sage: c_s = list(
        ....:     UniqueConstellationIterator(
        ....:         passport_to_constellations([6, 2], [5, 3], [7, 1], algorithm="conjugates", unique=False)
        ....:     )
        ....: )
        sage: len(c_s) == len(list(passport_to_constellations([6, 2], [5, 3], [7, 1])))
        True
    """

    def __init__(self, iterator: Iterable):
        self.iterator = iter(iterator)
        self.seen = set()

    def __iter__(self):
        return self

    def __next__(self):
        from .constellations.constellation import Constellation3

        while True:
            canonical = Constellation3(*next(self.iterator), canonical=True)
            if canonical not in self.seen:
                self.seen.add(canonical)
                return canonical


#################################
#                               #
#  Belyi maps helper functions  #
#                               #
#################################


def find_moebius(map_from, map_to):
    r"""
    Finds the |Moebius| transformation mapping ``map_from`` to ``map_to``.

    INPUT:

    - ``map_from`` -- Iterable[Complex]; The three complex numbers to use as source values
      for the |Moebius| transformation

    - ``map_to`` -- Iterable[Complex]; The three complex numbers to use as destination values
      for the |Moebius| transformation

    OUTPUT: a |Moebius| transformation mapping ``map_from`` to ``map_to``

    EXAMPLES::

        sage: from dessins import find_moebius
        sage: find_moebius([0, 1, Infinity], [0, 1, Infinity])
        z
        sage: find_moebius([0, 1, Infinity], [0, Infinity, 1])
        z/(z - 1)
        sage: find_moebius([0, 1, Infinity], [1, 0, Infinity])
        -z + 1

    .. |Moebius| unicode:: M U+00F6 bius .. Moebius
    """
    map_from, map_to = list(map_from), list(map_to)
    if len(set(map_from)) != 3 or len(set(map_to)) != 3:
        raise ValueError("3 complex numbers and their images must be supplied.")

    from sage.symbolic.relation import solve

    w, z = var("w,z")

    if set(map_to) == set([0, 1, Infinity]):
        rearranger = op.itemgetter(
            map_to.index(Infinity), map_to.index(0), map_to.index(1)
        )
        z1, z2, z3 = rearranger(map_from)  # map_to is now [Infinity,0,1]
        if Infinity not in map_from:
            return (z - z2) * (z3 - z1) / ((z - z1) * (z3 - z2))
        elif z1 == Infinity:
            return (z - z2) / (z3 - z2)
        else:
            if z2 == Infinity:
                temp_to = [0, Infinity, 1]
            else:
                temp_to = [0, 1, Infinity]
            moeb1 = find_moebius([z1, z2, z3], temp_to)
            moeb2 = find_moebius([0, 1, 2], temp_to)
            moeb2_inv = solve(moeb2 == w, z)[0].rhs()
            moeb3 = find_moebius([0, 1, 2], [Infinity, 0, 1])

            moeb = moeb3(z=moeb2_inv(w=moeb1))
            return moeb.numerator() / moeb.denominator()
    elif Infinity in map_to or Infinity in map_from:
        moeb1 = find_moebius(map_from, [Infinity, 0, 1])
        moeb2 = find_moebius(map_to, [Infinity, 0, 1])
        moeb2_inv = solve(moeb2 == w, z)[0].rhs()

        moeb = moeb2_inv(w=moeb1)
        return moeb.numerator() / moeb.denominator()

    z1, z2, z3 = map_from
    w1, w2, w3 = map_to

    lhs = (w - w2) * (w3 - w1) / ((w - w1) * (w3 - w2))
    rhs = (z - z2) * (z3 - z1) / ((z - z1) * (z3 - z2))
    expr = solve(lhs == rhs, w, solution_dict=True)[0]
    return expr[w].numerator() / expr[w].denominator()


def find_moebius2(map_from, map_to):
    r"""
    Finds the |Moebius| transformation mapping ``map_from`` to ``map_to``.

    INPUT:

    - ``map_from`` -- Iterable[Complex]; The three complex numbers to use as source values
      for the |Moebius| transformation

    - ``map_to`` -- Iterable[Complex]; The three complex numbers to use as destination values
      for the |Moebius| transformation

    OUTPUT: a |Moebius| transformation mapping ``map_from`` to ``map_to``

    EXAMPLES::

        sage: from dessins import find_moebius2
        sage: find_moebius2([0, 1, Infinity], [0, 1, Infinity])
        z
        sage: find_moebius2([0, 1, Infinity], [0, Infinity, 1])
        z/(z - 1)
        sage: find_moebius2([0, 1, Infinity], [1, 0, Infinity])
        -z + 1

    ALGORITHM::

    This function leverages cross-ratios following Section 3.3.2 of [Ahl1979] to compute
    the required transformation.


    REFERENCES::

    [Ahl1979] L. Ahlfors, Complex Analysis. McGraw-Hill, New York, 1979.

    .. |Moebius| unicode:: M U+00F6 bius .. Moebius
    """
    map_from, map_to = list(map_from), list(map_to)
    if len(set(map_from)) != 3 or len(set(map_to)) != 3:
        raise ValueError("3 complex numbers and their images must be supplied.")

    from sage.symbolic.relation import solve

    w, z = var("w,z")

    # TODO: is solving this explicitly useful?
    expr = solve(
        cross_ratio(*map_from, z) == cross_ratio(*map_to, w),
        w,
        solution_dict=True,
    )[0]
    return expr[w].numerator() / expr[w].denominator()


def cross_ratio(z2, z3, z4, v=var("z")):
    r"""
    Returns the unique linear transformation which carries ``z2``, ``z3``,
    and ``z4`` into 0, 1, and Infinity, in that order.  The linear
    transformation is returned as an expression in the variable ``v``.

    INPUT:

    - ``z2`` -- Complex; a complex number which is mapped to 0

    - ``z3`` -- Complex; a complex number which is mapped to 1

    - ``z4`` -- Complex; a complex number which is mapped to Infinity

    - ``v`` -- ``var``; the variable used as the argument in the returned
      expression

    OUTPUT: a linear transformation mapping ``z2``, ``z3``, and ``z4``
    to 0, 1, and Infinity

    EXAMPLES::

        sage: from dessins import cross_ratio
        sage: cross_ratio(0, 1, Infinity)
        z
        sage: cross_ratio(0, I, -I)
        2*I*z/(I*z - 1)
        sage: cross_ratio(1, -1, 0)
        1/2*(z - 1)/z
        sage: cross_ratio(Infinity, 1, 0)
        1/z
        sage: cross_ratio(1, Infinity, 0, v=var("x"))
        (x - 1)/x

    REFERENCES::

    [Ahl1979] L. Ahlfors, Complex Analysis. McGraw-Hill, New York, 1979.
    """
    if z2 == Infinity:
        return (z3 - z4) / (v - z4)
    elif z3 == Infinity:
        return (v - z2) / (v - z4)
    elif z4 == Infinity:
        return (v - z2) / (z3 - z2)
    else:
        return (v - z2) * (z3 - z4) / ((v - z4) * (z3 - z2))


##############################
#                            #
#  Solvers helper functions  #
#                            #
##############################


def digits_to_bits(digits):
    r"""
    Determines the number of bits required to represent ``10**digits``.  The
    result is returned as a decimal so that ``floor`` or ``ceil`` can be
    applied depending on context.

    EXAMPLES::

        sage: from dessins import digits_to_bits
        sage: digits_to_bits(7)
        23.25349666421154
        sage: ceil(digits_to_bits(1))
        4
        sage: ceil(digits_to_bits(2))
        7
        sage: ceil(digits_to_bits(3))
        10
    """
    if digits <= 0:
        raise ValueError("Digits must be positive.")
    return digits * RDF(log(10) / log(2))


def bits_to_digits(bits):
    r"""
    Determines the number of digits required to represent  ``2**bits``.  The
    result is returned as a decimal so that ``floor`` or ``ceil`` can be
    applied depending on context.

    EXAMPLES::

        sage: from dessins import bits_to_digits
        sage: bits_to_digits(12)
        3.6123599479677737
        sage: ceil(bits_to_digits(3))
        1
        sage: ceil(bits_to_digits(6))
        2
        sage: ceil(bits_to_digits(9))
        3
        sage: ceil(bits_to_digits(10))
        4
    """
    if bits <= 0:
        raise ValueError("Bits must be positive.")
    return bits * RDF(log(2) / log(10))


#############################
#                           #
#  Miscellaneous functions  #
#                           #
#############################


def enlist(arg):
    r"""
    Ensures ``arg`` is a list, returning ``[arg]`` in case ``arg``
    is a single element, a ``dict``, or a ``str``.

    EXAMPLES::

        sage: from dessins import enlist
        sage: enlist(1)
        [1]
        sage: enlist([1])
        [1]
        sage: enlist(range(5))
        [0, 1, 2, 3, 4]
        sage: enlist({1: 2, 3: 4})
        [{1: 2, 3: 4}]
    """
    if not (hasattr(arg, "__next__") or hasattr(arg, "__contains__")):
        return [arg]
    # elif issubclass(arg.__class__, (dict, str)):
    elif isinstance(arg, (dict, str)):
        return [arg]
    try:
        return list(arg)
    except:  # noqa: E722
        return [arg]


# def entuple(arg):
#     try:
#         return tuple(arg)
#     except TypeError:
#         return (arg,)
