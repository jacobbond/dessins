r"""
Description

EXAMPLES::

    sage: from dessins import passport_to_constellations

AUTHORS:

- Jacob Bond: initial implementation
"""

import collections

# isort: split
########## Sage imports ##########
from sage.functions.other import factorial
from sage.libs.gap.libgap import libgap

##################################

# isort: split
from ..utils import (
    GapIterator,
    UniqueConstellationIterator,
    gap_range,
    generic_permutation,
)

######### this package ###########
from .constellation import Constellation3

##################################

##################################
#                                #
#  generation of constellations  #
#                                #
##################################


def passport_to_constellations(
    tau0,
    tau1,
    tau_inf=None,
    sym_size=None,
    algorithm="double cosets",
    unique=True,
    as_tuple=False,
):
    r"""
    INPUT:

    - ``tau0`` -- list of positive integers; the first cycle type for the
      passport

    - ``tau1`` -- list of positive integers; the second cycle type for the
      passport

    - ``tau_inf`` -- list of positive integers (default: :code:`None`); the third
      cycle type for the passport; if :code:`None`, all 3-constellations matching the
      first two cycle types will be returned

    - ``sym_size`` -- positive integer (default: :code:`None`); the number of letters
      permuted by the symmetric group containing the desired conjugacy class

    - ``algorithm`` -- :code:`string` (default: :code:`double cosets`); the algorithm to use for
      generating permutation pairs, either computing :code:`double cosets` or
      generating `conjugates` of one of the permutations

    - ``unique`` -- :code:`bool` (deafult: :code:`True`); whether to ensure uniqueness of
      3-constellations when `conjugates` of one of the permutations are
      used for generation

    - ``as_tuple`` -- :code:`bool` (default: :code:`False`); whether to return the
      3-constellations as tuples of permutations or create :class:`Constellation3`
      objects for each 3-constellation

    OUTPUT: an iterator of the 3-constellations matching the given passport

    EXAMPLES::

        sage: from dessins import passport_to_constellations
        sage: list( passport_to_constellations([1], [1], [1]) )
        [((), (), ())]
        sage: list(
        ....:     passport_to_constellations([6], [2,2,1,1], [3,2,1])
        ....: )
        [((1,2,3,4,5,6), (3,6)(4,5), (1,3,2)(4,6)),
         ((1,2,3,4,5,6), (2,4)(5,6), (1,5,2)(3,4)),
         ((1,2,3,4,5,6), (2,6)(4,5), (1,2)(3,6,4))]

    If :code:`tau_inf` is :code:`None`, :code:`passport_to_constellations` will return all
    3-constellations matching the pair of cycle types provided. ::

        sage: list( passport_to_constellations([1], [1]) )
        [((), (), ())]
        sage: list( passport_to_constellations([3], [3]) )
        [((1,2,3), (1,2,3), (1,2,3)), ((1,2,3), (1,3,2), ())]

    Aside from the default :code:`double cosets` algorithm, a algorithm based on iterating
    over a conjugacy class is available.  This algorithm may generate multiple
    isomorphic 3-constellations, but provides an option to ensure each
    3-constellation generated is from a unique isomorphism class. ::

        sage: list(
        ....:     passport_to_constellations(
        ....:         [6], [2,2,1,1], [3,2,1],
        ....:         algorithm="conjugates", unique=True
        ....:     )
        ....: )
        [((1,2,3,4,6,5), (3,5)(4,6), (1,3,2)(4,5)),
         ((1,2,3,5,6,4), (2,4)(3,5), (1,2)(3,4,6)),
         ((1,2,3,5,6,4), (2,4)(5,6), (1,2)(3,4,5))]

    The primary use for the :code:`conjugates` algorithm of generation is to generate
    a small number of examples in large degrees when computation of double
    becomes expensive. ::

        sage: next(passport_to_constellations(
        ....:     [2,4,5,2], [9,4], [4,2,7],
        ....:     algorithm="conjugates", unique=False)
        ....: )
        ((1,13)(2,7,8,9,11)(3,5,4,6)(10,12), (1,5,4,6,3,11,12,9,7)(2,13,10,8), (1,2,3,4)(5,6)(7,8,9,10,11,12,13))

    .. NOTE::

        For degree 12 or less, the :code:`double cosets` algorithm is usually able to
        rapidly (< 1s) compute all 3-constellations of the given type
        (3-constellations with passport :code:`[[11], [11], [11]]` provides a particular
        counterexample.)  For degree 13, the :code:`double cosets` computation usually
        completes within a reasonable (< 30s) time.  However, for larger degrees,
        computation of double cosets becomes prohibitive.

        In order to be able to generate examples of 3-constellations for larger
        degrees without requiring the computation of double cosets, the
        :code:`conjugates` algorithm was included.

    ALGORITHM:

    To find all 3-constellations with a given passport this function enumerates
    the double cosets of :math:`C_{S_{n}}(\sigma_{0})` and :math:`C_{S_{n}}(\sigma_{1})` in
    :math:`S_{n}` following Lemma 1.10 in [KMSV2014], where :math:`\sigma_{0}` has cycle
    type :math:`\tau_{0}` and :math:`\sigma_{1}` has cycle type :math:`\tau_{1}`.  In order to
    reduce the number of double cosets that are iterated over, the function uses
    the cycle types with the fewest conjugates in :math:`S_{n}` among :math:`\{\tau_{0}, \tau_{1}, \tau_{\infty}\}`
    for :math:`\tau_{0}` and :math:`\tau_{1}`, then rearranges the resulting triples to match
    the original passport.

    To generate examples of 3-constellations for larger degrees, this function
    also provides an algorithm which generates pairs of permutations by keeping one
    element constant and iterating over a conjugacy class for the other element.
    As in the case of double cosets, the function uses the conjugacy class for
    the cycle type with the fewest conjugates to iterate over.

    REFERENCES:

        [KMSV2014] M. Klug, M. Musty, S. Schiavone, and J. Voight, “Numerical calculation of three-point branched covers of the projective line,” LMS Journal of Computation and Mathematics, vol. 17, no. 1, pp. 379–430, 2014.

    TESTS: pytest
    """
    if algorithm == "conjugates" and unique is True and as_tuple is True:
        raise ValueError(
            "When using conjugates for generation, return type must be `Constellation3` to determine uniqueness."
        )
    # allow for omitting sym_size if singletons are specified:
    if sym_size is None:
        sym_size = sum(tau0)

    if "double" in algorithm.lower() and "cosets" in algorithm.lower():
        (tau0, tau1, tau_inf, un_rearranger) = _rearrange_by_num_conjs(
            tau0, tau1, tau_inf, sym_size, extremum=max
        )
        candidates = _double_coset_iterator(tau0, tau1, sym_size=sym_size)
    elif "conjug" in algorithm.lower():
        (tau0, tau1, tau_inf, un_rearranger) = _rearrange_by_num_conjs(
            tau0, tau1, tau_inf, sym_size, extremum=max
        )
        candidates = _conjugacy_iterator(tau0, tau1, sym_size=sym_size)
    else:
        raise ValueError("Algorithm not supported.")

    if tau_inf is not None:
        tau_inf = generic_permutation(tau_inf).CycleStructurePerm()
        candidates = (
            un_rearranger(pair)
            for pair in candidates
            if (
                (pair[1] * pair[0]).CycleStructurePerm() == tau_inf
                and bool(libgap.Group(pair).IsTransitive(gap_range(1, sym_size)))
            )
        )
    else:
        candidates = (
            un_rearranger(pair)
            for pair in candidates
            if bool(libgap.Group(pair).IsTransitive(gap_range(1, sym_size)))
        )

    if as_tuple is True:
        return candidates
    elif algorithm == "conjugates" and unique is True:
        return UniqueConstellationIterator(candidates)
    else:
        return (Constellation3(*pair) for pair in candidates)


def _double_coset_iterator(tau0, tau1, sym_size=None):
    if sym_size is None:
        sym_size = sum(tau0)

    # get a representative for each conjugacy class:
    s0, s1 = generic_permutation(tau0), generic_permutation(tau1)

    G = libgap.SymmetricGroup(sym_size)
    C_G_s0, C_G_s1 = libgap.Centralizer(G, s0), libgap.Centralizer(G, s1)
    # use an iterator here?
    coset_space = libgap.DoubleCosetRepsAndSizes(G, C_G_s0, C_G_s1)

    candidates = ((s0, rep * s1 * rep.Inverse()) for rep, _ in coset_space)
    return candidates


def _conjugacy_iterator(tau0, tau1, sym_size=None):
    if sym_size is None:
        sym_size = sum(tau0)

    s0, s1 = generic_permutation(tau0), generic_permutation(tau1)
    G = libgap.SymmetricGroup(sym_size)
    if num_conjugates(tau0) < num_conjugates(tau1):
        conjugacy_class = GapIterator(G.ConjugacyClass(s0).Iterator())
        candidates = ((s, s1) for s in conjugacy_class)
    else:
        conjugacy_class = GapIterator(G.ConjugacyClass(s1).Iterator())
        candidates = ((s0, s) for s in conjugacy_class)
    return candidates


# set up a method to rearrange the passport in order to reduce
# the number of elements that are iterated over, then rearrange the
# resulting 3-constellations to match the original passport
def _rearrange_by_num_conjs(tau0, tau1, tau_inf, sym_size, extremum=max):
    if tau_inf is None:

        def replace(y):
            return y

        return tau0, tau1, tau_inf, replace

    num_tau0_conj, num_tau1_conj, num_taui_conj = [
        num_conjugates(t, sym_size) for t in [tau0, tau1, tau_inf]
    ]
    extremum_conjs = extremum(num_tau0_conj, num_tau1_conj, num_taui_conj)
    if num_taui_conj == extremum_conjs:

        def replace(y):
            return y

    elif num_tau1_conj == extremum_conjs:
        tau1, tau_inf = tau_inf, tau1

        # algorithm will calculate sigma0, sigmai, so replace sigmai by
        # (sigma0*sigmai)^(-1) = sigma1 since sigmai*sigma1*sigma0 = 1
        def replace(y):
            return (y[0], (y[0] * y[1]).Inverse())

    else:
        tau0, tau_inf = tau_inf, tau0

        # algorithm will calculate sigmai, sigma1, so replace sigmai by
        # (sigmai*sigma1)^(-1) = sigma0
        def replace(y):
            return ((y[0] * y[1]).Inverse(), y[1])

    return tau0, tau1, tau_inf, replace


def num_conjugates(cycle_t, sym_size=None):
    r"""
    Returns the size of the conjugacy class in the symmetric group on
    :code:`sym_size` letters consisting of permutations with cycle type
    :code:`cycle_t`.

    INPUT:

    - ``cycle_t`` -- list of positive integers; the cycle type of the
      permutations contained in the conjugacy class

    - ``sym_size`` -- positive integer (default: :code:`None`); the number of letters
      permuted by the symmetric group containing the desired conjugacy class

    OUTPUT: The size of the conjugacy class in the symmetric group on
    :code:`sym_size` letters with cycle type :code:`cycle_t`.

    EXAMPLES::

        sage: from dessins import num_conjugates
        sage: num_conjugates([1])
        1
        sage: num_conjugates([3])
        2
        sage: num_conjugates([5, 2])
        504
    """
    from sage.misc.all import prod

    if sym_size is None:
        sym_size = sum(cycle_t)
    count = collections.Counter(cycle_t)
    denom = prod(cycle_t) * prod([factorial(v) for v in count.values()])
    return prod(range(sym_size, sym_size - sum(cycle_t), -1)) / denom
