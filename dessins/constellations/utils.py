r"""
Utilities for computing with 3-constellations

EXAMPLES::

    sage: from dessins import canonical_constellation, Constellation3
    sage: c = Constellation3("(1,3,5,4)(2,7,9)(6,8)", "(1,2,6)(3,5,9,7,8)", "(1,4,2,9)(3,8,5)", length=4)
    sage: canonical_constellation(c, certificate=True)
    (((1,2)(3,4,5,6)(7,8,9),
      (1,3,4,8,7)(2,6,9),
      (1,4,3)(5,9,8,6),
      (1,5,4,3,2,7,8,9)),
     (1,6,2,9,8)(4,5))

AUTHORS:

- Jacob Bond: initial implementation
"""

import collections
from typing import Optional

# isort: split
########## Sage imports ##########
from sage.libs.gap.libgap import libgap

##################################

# isort: split
########## this package ##########
from ..utils import coerce_perm

##################################


def canonical_constellation(
    perms, sym_size: Optional[int] = None, certificate: bool = False
):
    r"""
    Return a canonical representative of the constellation :code:`perms`.

    INPUT:

    - ``perms`` -- Constellation; a constellation, passed as a sequence of permutations

    - ``sym_size`` -- ``Optional[int]``; the degree of the symmetric group that :code:`perms` are
      elements of

    - ``certificate`` -- ``bool``; whether to return the permutation which conjugates
      :code:`perms` to its canonical constellation

    EXAMPLES::

        sage: from dessins import canonical_constellation, Constellation3
        sage: c = Constellation3("(1,3,5,4)(2,7,9)(6,8)", "(1,2,6)(3,5,9,7,8)", "(1,4,2,9)(3,8,5)", length=4)
        sage: canonical, certificate = canonical_constellation(c, certificate=True)
        sage: c ** certificate == canonical
        True
        sage: canonical2, certificate2 = canonical_constellation(c ** libgap.eval("(1,3,5,9)"), certificate=True)
        sage: canonical == canonical2
        True
        sage: certificate == libgap.eval("(1,3,5,9)") * certificate2
        True

    ALGORITHM:

    The permutations :code:`perms` are relabelled using :code:`relabel_by_base`, starting from
    each base :math:`1, \ldots,`:code:`sym_size`, then the lexicographically smallest
    relabelling is used as the canonical representative of the simultaneous conjugacy class of :code:`perms`.

    REFERENCES:

        [McK2014] B. McKay, The Simultaneous Conjugacy Problem in the symmetric group SN, URL (version: 2014-04-06): https://mathoverflow.net/a/162453

        [HK2019] M. van Hoeij and V. J. Kunwar, "Classifying (almost)-Belyi maps with five exceptional points," Indagationes Mathematicae, Volume 30, Issue 1, Pages 136-156, 2019.
    """
    perms = [coerce_perm(p, "gap") for p in perms]
    if sym_size is None:
        sym_size = max(int(p.LargestMovedPoint()) for p in perms)
    if sym_size == 0:
        sym_size = 1  # trivial dessin

    tuples = sorted(
        relabel_by_base(perms, base, sym_size, certificate)
        for base in range(1, sym_size + 1)
    )

    if certificate is False:
        return tuple(tuples[0])  # to allow for taking a set of these objects
    else:
        return tuple(tuples[0][0]), tuples[0][1]


def relabel_by_base(perms, base, sym_size=None, certificate=False):
    r"""
    ALGORITHM:

    Let :code:`perms`:math:`= (g_{1}, \ldots, g_{d})` and :math:`N =`:code:`sym_size`.  Beginning at vertex
    :code:`base`, label the graph on vertices :math:`\{1, \ldots, N\}` using
    breadth-first search by removing the vertex :math:`x` at the head of the queue, and pushing,
    in order, any of :math:`x^{g_{1}}, \ldots, x^{g_{d}}` which haven't been previously seen.  The order
    that the vertices :math:`1, \ldots, N` were visited gives a relabelling acts on the permutaitons in :code:`perms`.

    REFERENCES:

        [McK2014] B. McKay, The Simultaneous Conjugacy Problem in the symmetric group SN, URL (version: 2014-04-06): https://mathoverflow.net/a/162453
    """
    perms = [coerce_perm(p, "gap") for p in perms]
    if sym_size is None:
        sym_size = max(p.LargestMovedPoint() for p in perms)

    base = libgap.eval(str(base))
    seen = set([base])  # faster checking for new elements
    q = collections.deque([base])

    perm = libgap.eval(str([base]))
    while perm.Size() < sym_size:
        current = q.popleft()
        for p in perms:
            im = current**p
            if im not in seen:
                seen.add(im)
                q.append(im)
                perm.Add(im)
    # conjugate this to get relabelled perms; TODO: is this supposed to be inverse?
    relabel = libgap.PermList(perm).Inverse()

    if certificate is False:
        return [p**relabel for p in perms]
    else:
        return [p**relabel for p in perms], relabel


def is_simultaneous_conjugates(perms1, perms2, G=None):
    r"""
    INPUT:

    - ``perms1`` -- :code:`list`; a list of two permutations

    - ``perms2`` -- :code:`list`; a second list of permutations so that the :code:`i`th element of
      :code:`perms2` corresponds to the ith element of :code:`perms1`

    - ``G`` -- Group (default: None); the group to use for determining conjugacy

    OUTPUT: boolean

    EXAMPLES::

        sage: from dessins import passport_to_constellations, is_simultaneous_conjugates
        sage: c_s = passport_to_constellations([8,2], [7,3], [9,1])
        sage: c1 = next(c_s)
        sage: c2 = c1 ** libgap.eval("(1,2,3)(4,5,6)(7,8,9)")
        sage: is_simultaneous_conjugates(c1[:2], c2[:2])
        True
        sage: c3 = next(c_s)
        sage: is_simultaneous_conjugates(c1[:2], c3[:2])
        False

    .. TODO::

        Add a certificate option to return a conjugating element

    ALGORITHM:

    Let :code:`perms1`:math:`= (s_{1}, s_{2})` and :code:`perms2`:math:`= (t_{1}, t_{2})`.  Determine
    elements :math:`\sigma_{1}, \sigma_{2}` so that :math:`s_{1}^{\sigma_{1}} = t_{1}` and :math:`s_{2}^{\sigma_{2}} = t_{2}`.
    Then :math:`(s_{1}, s_{2})` is simultaneously conjugate to
        :math:`(s_{1}, s_{2})^{\sigma_{1}} = (t_{1}, s_{2}^{\sigma_{1}}) = (t_{1}, t_{2}^{\sigma_{2}^{-1}\sigma_{1}})`.
    By [KMSV2014; Lemma 1.12], :math:`(t_{1}, t_{2}^{\sigma_{2}^{-1}\sigma_{1}})` is simultaneously conjugate in :math:`G` to :math:`(t_{1}, t_{2})`
    iff :math:`C_{G}(t_{1})\;1\;C_{G}(t_{2}) = C_{G}(t_{1})\;(\sigma_{2}^{-1}\sigma_{1})^{-1}\;C_{G}(t_{2})`.

    REFERENCES:

        [KMSV2014] M. Klug, M. Musty, S. Schiavone, and J. Voight, “Numerical calculation of three-point branched covers of the projective line”, LMS Journal of Computation and Mathematics, Volume 17, Issue 1, Pages 379–430, 2014.
    """
    perms = perms1 + perms2
    try:
        max_moved = max([t.LargestMovedPoint() for t in perms])
    except AttributeError:
        perms = [coerce_perm(p, "gap") for p in perms]
        max_moved = max([t.LargestMovedPoint() for t in perms])
    if G is None:
        G = libgap.SymmetricGroup(max_moved)
    s1, s2, t1, t2 = perms

    if bool(G.IsConjugate(s1, t1)) is False or bool(G.IsConjugate(s2, t2)) is False:
        return False

    conj1, conj2 = G.RepresentativeAction(s1, t1), G.RepresentativeAction(s2, t2)
    C1, C2 = G.Centralizer(t1), G.Centralizer(t2)

    return bool(
        libgap.DoubleCoset(C1, conj1.Inverse() * conj2, C2)
        == libgap.DoubleCoset(C1, G.Identity(), C2)
    )


def conjugating_element(p, q):
    r"""
    Takes two permutations :code:`p`, :code:`q` and determines a permutation :math:`\sigma \in S_{n}` so
    that :math:`p^{\sigma} = q`.

    EXAMPLES::

        sage: from dessins import conjugating_element
        sage: s1 = libgap.eval("(1,5,9,3,4,2)")
        sage: s2 = libgap.eval("(8,4,2,9,5,6)")
        sage: s1 ** conjugating_element(s1, s2) == s2
        True
    """
    from ..utils import gap_range

    try:
        max_moved = max(p.LargestMovedPoint(), q.LargestMovedPoint())
    except AttributeError:
        p, q = coerce_perm(p, "gap"), coerce_perm(q, "gap")
        max_moved = max(p.LargestMovedPoint(), q.LargestMovedPoint())
    max_moved = int(max_moved)  # coerce from GAP
    if max_moved == 0:
        return libgap.eval("()")
    p_cycles = list(p.Cycles(gap_range(1, max_moved)))
    q_cycles = list(q.Cycles(gap_range(1, max_moved)))

    # sort by cycles lengths so that cycles of the correct length
    # will match up:
    p_cycles.sort(key=lambda tup: len(tup), reverse=True)
    q_cycles.sort(key=lambda tup: len(tup), reverse=True)

    # had to sort the cycles anyway, so use this instead of .cycle_type
    p_type = [len(c) for c in p_cycles]
    q_type = [len(c) for c in q_cycles]

    if p_type != q_type:
        raise ValueError("Permutations do not have the same cycle type.")
    sigma = max_moved * [0]

    for p_cycle, q_cycle in zip(p_cycles, q_cycles):
        for p_elt, q_elt in zip(p_cycle, q_cycle):
            sigma[int(p_elt - 1)] = q_elt
    return libgap.PermList(sigma)
