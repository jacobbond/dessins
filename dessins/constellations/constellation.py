r"""
The :class:`.Constellation3` class for 3-constellations and other functions for computing with 3-constellations

EXAMPLES::

    sage: from dessins import Constellation3
    sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)")
    sage: c.monodromy_group().StructureDescription()
    "A4"
    sage: c.canonical()
    ((2,3,4), (1,2)(3,4), (1,2,3))
    sage: c.flexible_equivalence(c.canonical())
    Braid action by (0, 1, Infinity) |-> (0, 1, Infinity), Conjugation by (1,3,2,4)
    sage: list(c.passport().constellations())
    [((2,3,4), (1,2)(3,4), (1,2,3))]
    sage: c.belyi_map(d=3, h=100, subs_dict={var('c0'): 0, var('a0'): 1})
    ...
    Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field

AUTHORS:

- Jacob Bond: initial implementation
"""

from collections.abc import Iterator, Sequence

# check if this is being imported by a Sage or Python script
import sys  # isort: skip

if ".py" in sys.argv[0] or ".sage" in sys.argv[0]:
    import sage.all  # noqa: F401

# isort: split
########## Sage imports ##########
from sage.arith.functions import lcm
from sage.groups.perm_gps.permgroup_named import SymmetricGroup
from sage.libs.gap.libgap import libgap
from sage.misc.misc_c import prod
from sage.misc.sage_eval import sage_eval
from sage.rings.infinity import Infinity

####################################

# isort: split
########## this package ##########
from ..utils import coerce_perm, enlist, gap_range
from .utils import canonical_constellation

##################################

# Note:  if GAP's SmallGroups package is not installed, a RuntimeError
#    is raised

###########################
#                         #
#  3-Constellation Class  #
#                         #
###########################


class Constellation3:
    r"""
    A constellation of length 3; however, the class supports arbitrary length constellations

    The class name follows a naming convention similar to that for small vectors and matrices in the Rust `nalgebra <https://www.nalgebra.org/docs/user_guide/vectors_and_matrices/#the-generic-matrix-type>`_ library.

    INPUT:

    - ``*sigmas`` -- :code:`GapElement_Permutation`; the permutations comprising the constellation

    - ``length`` -- Nonnegative Integer or :code:`None` (default: :code:`3`); the length of the
      constellation being represented; if :code:`length` is :code:`len(sigmas) + 1`, :code:`sigmas`
      is appended with the permutation satisfying :code:`prod(reversed(sigmas)) == ()`; if
      :code:`length` is :code:`None`, :code:`len(sigmas)` is used as the length

    - ``canonical`` -- :code:`bool` (default: :code:`False`); whether to convert permutations
      to canonical representatives

    EXAMPLES::

        sage: from dessins import Passport, Constellation3
        sage: p = Passport([ [3,1], [2,2], [3,1] ])
        sage: c = Constellation3('(1,2,3)', '(1,2)(3,4)')
        sage: c.is_isomorphic( next(p.constellations()) )
        True
        sage: c.belyi_map(d=2, h=100, subs_dict={var('c0'): 0, var('a0'): 1})
        ...
        Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field
        sage: c ** libgap.eval('(1,2)')
        ((1,3,2), (1,2)(3,4), (2,4,3))

    .. TODO::

        Consider inheriting from Sage's :code:`Constellation` class

    .. NOTE::

        This class has the following attributes:

        * ``_sigmas`` -- :code:`Tuple[GapElement_Permutation]`; The permutations comprising the 3-constellation.
        * ``_degree`` -- nonnegative integer; The degree of the 3-constellation.
        * ``_length`` -- nonnegative integer; The length of the 3-constellation.
        * ``_belyi`` -- :class:`BelyiMap`; A Belyi map for the 3-constellation.
    """

    def __init__(self, *sigmas, length=3, canonical=False):
        if len(sigmas) == 1 and isinstance(sigmas[0], (Sequence, Iterator)):
            sigmas = enlist(sigmas[0])
        # need to do check that the inputs are some type of permutations
        sigmas = [coerce_perm(s, "gap") for s in sigmas]
        if length is not None and length == len(sigmas) + 1:
            sigmas.append(prod(reversed(sigmas)).Inverse())
        elif length is not None and length != len(sigmas):
            raise ValueError(
                "Specified degree is inconsistent with the number of permutations supplied."
            )
        else:
            length = len(sigmas)
        self._length = length

        if prod(reversed(sigmas)) != libgap.eval("()"):
            raise ValueError("Product of permutations does not equal the identity.")

        degree = max(int(s.LargestMovedPoint()) for s in sigmas)
        self._degree = degree if degree != 0 else 1

        if not bool(libgap.Group(sigmas).IsTransitive(gap_range(1, self._degree))):
            raise ValueError("Permutations do not form a transitive group.")

        if canonical is True:
            sigmas = canonical_constellation(sigmas)
        self._sigmas = tuple(sigmas)

    def __eq__(self, other):
        r"""
        Evaluates whether the given constellations are equal.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c1 = Constellation3("(1,2,3)", "(1,2)(3,4)")
            sage: c2 = Constellation3("(1,2,3)", "()")
            sage: c3 = Constellation3("(2,3,4)", "(1,2)(3,4)")
            sage: c1 == c1
            True
            sage: c1 == c2
            False
            sage: c2 == c3
            False
        """
        if isinstance(other, Constellation3):
            return self.permutations() == other.permutations()
        if issubclass(other.__class__, (Sequence, Iterator)):
            return self._sigmas == tuple(other)
        else:
            raise TypeError("Other must be a constellation, a sequence or an iterator.")

    def __ne__(self, other):
        if isinstance(other, Constellation3):
            return self.permutations() != other.permutations()
        elif issubclass(other.__class__, (Sequence, Iterator)):
            return self._sigmas != tuple(other)
        else:
            raise TypeError("Other must be a constellation, a sequence or an iterator.")

    def __gt__(self, other):
        if isinstance(other, Constellation3):
            return self.permutations() > other.permutations()
        elif issubclass(other.__class__, (Sequence, Iterator)):
            return self._sigmas > tuple(other)
        else:
            raise TypeError("Other must be a constellation, a sequence or an iterator.")

    def __ge__(self, other):
        if isinstance(other, Constellation3):
            return self.permutations() >= other.permutations()
        elif issubclass(other.__class__, (Sequence, Iterator)):
            return self._sigmas >= tuple(other)
        else:
            raise TypeError("Other must be a constellation, a sequence or an iterator.")

    def __lt__(self, other):
        if isinstance(other, Constellation3):
            return self.permutations() < other.permutations()
        elif issubclass(other.__class__, (Sequence, Iterator)):
            return self._sigmas < tuple(other)
        else:
            raise TypeError("Other must be a constellation, a sequence or an iterator.")

    def __le__(self, other):
        if isinstance(other, Constellation3):
            return self.permutations() <= other.permutations()
        elif issubclass(other.__class__, (Sequence, Iterator)):
            return self._sigmas <= tuple(other)
        else:
            raise TypeError("Other must be a constellation, a sequence or an iterator.")

    def __str__(self):
        return str(self._sigmas)

    def __repr__(self):
        return repr(self._sigmas)

    def __hash__(self):
        return hash(self._sigmas)

    def __getitem__(self, pos):
        if pos is Infinity:
            pos = 2
        return self._sigmas[pos]

    def __getslice__(self, pos1, pos2):
        if pos1 is Infinity:
            pos1 = 2
        if pos2 is Infinity:
            pos2 = 2
        return self._sigmas[pos1:pos2]

    def __iter__(self):
        return iter(self._sigmas)

    def __len__(self):
        return len(self._sigmas)

    def __pow__(self, other):
        r"""
        Returns the action of :code:`other` on :code:`self` by
        simultaneous conjugation.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)")
            sage: c ** libgap.eval("(1,3)")
            ((1,3,2), (1,4)(2,3), (1,3,4))
        """
        return Constellation3(*[s**other for s in self._sigmas], length=self._length)

    def permutations(self):
        r"""
        Returns a tuple of the permutations of :code:`self`.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)")
            sage: c.permutations()
            ((1,2,3), (1,2)(3,4), (1,4,3))
        """
        return self._sigmas

    def constellation(self):
        r"""
        Returns :code:`self`. This method is implemented to provide a
        consistent interface between different representations of a
        dessin d'enfant.
        """
        return self

    def canonical(self, certificate=False):
        r"""
        Returns a canonical representative of :code:`self`.
        """
        out = canonical_constellation(self._sigmas, certificate=certificate)
        if certificate is True:
            return Constellation3(*out[0]), out[1]
        else:
            return Constellation3(*out)

    def canonicalize(self):
        r"""
        Replaces the permutations of :code:`self` with its canonical representative.
        """
        self._sigmas = canonical_constellation(self._sigmas)

    def passport(self):
        r"""
        Returns the passport of :code:`self`.
        """
        from ..passport import Passport

        Sn = SymmetricGroup(self._degree)
        sigmas = [Sn(coerce_perm(s, "sage")) for s in self._sigmas]
        return Passport(s.cycle_type(as_list=True) for s in sigmas)

    @property
    def degree(self):
        r"""
        Returns the degree of :code:`self`.
        """
        return self.passport().degree

    def belyi_map(self, d=None, h=None, subs_dict=None, **kwargs):
        r"""
        Returns a Belyi map for :code:`self`.  Only supports
        computation of genus 0 Belyi maps.

        INPUT:

        - ``d`` -- positive integer; the degree bound to use when finding
          the minimal polynomial of the field of definition of the Belyi map

        - ``h`` -- positive integer; the height bound to use when finding
          the minimal polynomial of the field of definition of the Belyi map

        - ``subs_dict`` -- ``dict`` (default: ``None``); a dictionary providing variable substitutions
          for two of the indeterminates in the generic form for the Belyi map
          with passport matching that of ``self`` (The generic form uses variables ``ai`` for the zeros
          of the map in order of decreasing degree.  The variables ``bi`` and ``ci`` are used similarly
          for the preimages of 1 and Infinity, with the preimage of Infinity of largest degree being
          located at Infinity.)

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)")
            sage: c.belyi_map(2, 10**3, subs_dict={var("a0"): 1, var("c0"): 0})
            ...
            Found 1 Galois orbits.
            Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field
            sage: c = Constellation3("(2,4,3,5,6)", "(1,2,3)(5,6)")
            sage: b = c.belyi_map(d=6, h=10**4, subs_dict={var("a0"): 0, var("c0"): 1})
            ...
            Found 2 Galois orbits.
            sage: c.canonical() == b.constellation().canonical()
            True
        """
        # TODO: assess whether to cache the computed Belyi map
        try:
            return self._belyi
        except AttributeError:
            pass
        if self.genus > 0:
            raise NotImplementedError(
                "Computation of Belyi maps for genera greater than 0 is not implemented."
            )
        else:
            if d is None or h is None:
                raise ValueError(
                    "If dessin has no stored Belyi map, degree and height bounds are required."
                )

            # TODO: revisit this after refactoring belyi_map.py
            belyis = self.passport().belyi_maps(d, h, subs_dict=subs_dict)
            for belyi in belyis:
                for elt in belyi.conjugates():
                    if (
                        self.constellation().canonical()
                        == elt.constellation().canonical()
                        # TODO: how to handle Belyi map isomorphims?
                    ):  # .is_isomorphic(elt.constellation()):
                        self._belyi = elt
                        return self._belyi

    #        belyis = self.passport().belyis()
    #        constell_dict = {belyi.constellation().canonical(): belyi
    #                                                        for belyi in belyis}
    #        self._belyi = constell_dict[self._constellation()]
    #        return self._belyi

    def drawing(self):
        r"""
        Returns the drawing of :code:`self`.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c = Constellation3("(1,2,3,4)", "(1,2)(3,5)(4,6)")
            sage: c.drawing()
            {0: [(3, 1), (3, 2), (4, 3), (5, 4)], 1: [(4, 5)], 2: [(5, 6)], 3: [(0, 1), (0, 2)], 4: [(0, 3), (1, 5)], 5: [(0, 4), (2, 6)]}
        """
        from ..drawing import Drawing, constellation_to_drawing

        if self._length != 3:
            raise NotImplementedError(
                "Representation as a drawing is only implemented for 3-constellations."
            )
        return Drawing(constellation_to_drawing(*self._sigmas[:2]))

    @property
    def genus(self):
        r"""
        Returns the genus of :code:`self`.
        """
        return self.passport().genus

    def monodromy_group(self):
        r"""
        Returns the monodromy group of :code:`self`.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c = Constellation3("(1,2,3,4,5)", "()")
            sage: c.monodromy_group().StructureDescription()
            "C5"
        """
        return libgap.Group(self._sigmas[:-1])

    def monodromy_representation(self):
        r"""
        Returns the monodromy representation of :code:`self`.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c = Constellation3("(1,2,3,4,5)", "()")
            sage: c.monodromy_representation()
            [ a, b ] -> [ (1,2,3,4,5), () ]
        """
        from ..belyi_maps.monodromy import F2, A, B

        return libgap.GroupHomomorphismByImagesNC(
            F2,
            self.monodromy_group(),
            [A, B],
            self.permutations()[:-1],
        )

    def is_isomorphic(self, other):
        r"""
        Returns whether :code:`self` and :code:`other` have the same canonical representation.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c1 = Constellation3("(1,2)(3,4,5)", "(2,3,5,4)")
            sage: c2 = Constellation3("(1,2)(3,4,5)", "(1,2,5,4)")
            sage: ( c1 ** libgap.eval("(1,3)(2,4)") ).is_isomorphic(c1)
            True
            sage: c2.is_isomorphic(c1)
            False
        """
        if isinstance(other, Constellation3):
            return self.canonical() == other.canonical()
        else:
            return self.canonical() == Constellation3(*other).canonical()

    def is_flexibly_equivalent(self, other):
        r"""
        Returns whether there is a flexible equivalence between :code:`self` and :code:`other`.
        """
        if self.flexible_equivalence(other) is not False:
            return True
        else:
            return False

    def braid_action(self, idx):
        r"""
        Returns the result of applying the braid action transposing the permutations at positions
        :code:`idx` and :code:`idx+1`.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c = Constellation3("(1,2)(3,4,5)", "(1,4,2,5)", "(1,4,3,2,5)")
            sage: c.braid_action(0).passport()
            [[1, 4], [2, 3], [5]]
        """
        sigmas = list(self._sigmas)
        si, siplus1 = sigmas[idx : idx + 2]
        sigmas[idx : idx + 2] = [siplus1, siplus1 * si * siplus1.Inverse()]
        return Constellation3(*sigmas)
        # if idx == 0:
        #     return Constellation3(
        #         self._sigmas[1],
        #         self._sigmas[1] * self._sigmas[0] * self._sigmas[1].Inverse(),
        #         self._sigmas[2],
        #     )
        # elif idx == 1:
        #     return Constellation3(
        #         self._sigmas[0],
        #         self._sigmas[i],
        #         self._sigmas[i] * self._sigmas[1] * self._sigmas[i].Inverse(),
        #     )
        # # elif idx == 2:
        # #     return Constellation3(
        # #         self._sigmas[0] * self._sigmas[i] * self._sigmas[0].Inverse(),
        # #         self._sigmas[1],
        # #         self._sigmas[0],
        # #     )
        # else:
        #     raise ValueError("Invalid index for braid action.")

    def flexible_equivalence(self, other):
        r"""
        Returns a flexible equivalence mapping :code:`self` to :code:`other` or :code:`False`
        if there is none.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c1 = Constellation3('(2,3,4,5)', '(1,3,5,2)', '(1,2,3,5,4)')
            sage: c2 = Constellation3('(1,4,3,2)', '(1,4,5,2,3)', '(1,5,4,3)')
            sage: equiv = c1.flexible_equivalence(c2); equiv
            Braid action by (0, 1, Infinity) |-> (Infinity, 0, 1), Conjugation by (1,2,3)(4,5)
            sage: equiv(c1) == c2
            True
            sage: c3 = Constellation3(
            ....:     '(1,2,6,3,4)', '(1,2,4,6,5,3)', '(1,2,3,4,5,6)'
            ....: )
            sage: c4 = c3.braid_action(1) ** libgap.eval('(1,2)(3,4)')
            sage: equiv = c3.flexible_equivalence(c4); equiv
            Braid action by (0, 1, Infinity) |-> (0, Infinity, 1), Conjugation by (1,2)(3,4)
            sage: equiv(c3) == c4
            True
            sage: c1.flexible_equivalence(c3)
            False
            sage: c5 = Constellation3('(2,5,3,4)', '(1,2)(3,4,5)', '(1,2,3,4,5)')
            sage: c6 = Constellation3('(1,3,2,5,4)', '(2,5,3,4)', '(1,3)(2,5,4)')
            sage: c5.flexible_equivalence(c6)
            Braid action by (0, 1, Infinity) |-> (1, Infinity, 0), Conjugation by (1,3,5)
            sage: c7 = Constellation3("(2,4,6,3,5)", "(1,2,5,4,3)")
            sage: c8 = Constellation3("(2,4,5,3,6)", "(1,2,5,6,3)")
            sage: c9 = Constellation3("(2,4,3,6,5)", "(1,2,4,6,3)")
            sage: c7.flexible_equivalence(c8)
            False
            sage: equiv = c7.flexible_equivalence(c9); equiv
            Braid action by (0, 1, Infinity) |-> (Infinity, 0, 1), Conjugation by (1,6)(2,5)
            sage: equiv(c7) == c9
            True
            sage: c7.flexible_equivalence(c7)
            Braid action by (0, 1, Infinity) |-> (0, 1, Infinity), Conjugation by ()


        .. NOTE::

            Is there an error in ``flexible_equivalence`` and ``braid_action``?:

            .. code-block::

                sage: from dessins import passport_to_constellations, Constellation3
                sage: c1 = Constellation3("(2,3,4,5,6)", "(1,2,3,5,4)")
                sage: [
                ....:     c1.is_flexibly_equivalent(c2)
                ....:     for c2 in passport_to_constellations([5,1], [5,1], [5,1])
                ....: ]
                [True, False, False, False, False, True, False, True, False]
        """
        sorted1 = list(map(sorted, self.passport()))
        sorted2 = list(map(sorted, other.passport()))
        if sorted(sorted1) != sorted(sorted2):
            return False

        for braid_action, func in ConstellationFlexibleEquivalence.lookup_table.items():
            isomorph = func(self).isomorphism(other)
            if isomorph is not False:
                return ConstellationFlexibleEquivalence(braid_action, isomorph)

        return False

    def isomorphism(self, other):
        r"""
        Returns a permutation :code:`tau` so that :code:`self ** tau = other`, or :code:`False`
        if no such :code:`tau` exists.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c1 = Constellation3("(1,2,3)", "(1,2,4)")
            sage: c2 = c1 ** libgap.eval("(1,3,4)")
            sage: c1.isomorphism(c2)
            (1,3,4)
            sage: c1.isomorphism( Constellation3("(1,2,3)", "(1,3,4)") )
            False
        """
        # if self.passport != other.passport:
        #     return False

        canonical1, conj1 = self.canonical(certificate=True)
        canonical2, conj2 = other.canonical(certificate=True)

        if canonical1 != canonical2:
            return False
        else:
            return conj1 * (conj2 ** (-1))

    def abc(self):
        r"""
        Returns the list of orders of the elements comprising the constellation.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: Constellation3("(1,2)(3,4,5)", "(1,2,3,4)", "(2,4)(3,5)").abc()
            [6, 4, 2]
        """
        return [lcm(p) for p in self.passport()]

    def geometry(self):
        r"""
        Returns the geometry type of the triangle group associated with :code:`self`.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: Constellation3("(1,2,3)", "(1,2)(3,4)").geometry()
            'Spherical'
            sage: Constellation3("(1,2,3,4)", "(1,3,4,2)").geometry()
            'Hyperbolic'
            sage: Constellation3("(1,2,3)", "(1,2,3)").geometry()
            'Euclidean'
        """
        defect = sum(1 / elt for elt in self.abc())
        if defect == 1:
            return "Euclidean"
        elif defect > 1:
            return "Spherical"
        elif defect < 1:
            return "Hyperbolic"

    def primitivization(self, full=False):
        r"""
        Returns the constellation that results from :code:`self` acting on the blocks
        of :code:`self`.  If :code:`full` is :code:`True`, then this procedure will be
        repeated until the resulting constellation is primitive.

        EXAMPLES::

            sage: from dessins import Constellation3
            sage: c1 = Constellation3("(1,6,3,8,5,2,7,4)", "(1,8)(2,6)(4,5)")
            sage: c2 = Constellation3("(1,2,3,4)", "(2,4,3)")
            sage: c1.primitivization()
            ((1,2,3,4), (1,4), (2,4,3))
            sage: c1.primitivization().is_flexibly_equivalent(c2)
            True
            sage: c3 = Constellation3("(1,2,3,4,5,6,7,8)", "(1,8,7,6,5,4,3,2)")
            sage: c3.primitivization()
            ((1,2,3,4), (1,4,3,2), ())
            sage: c3.primitivization(full=True)
            ((1,2), (1,2), ())
        """
        G = self.monodromy_group()
        if G.IsPrimitive():
            return self

        blocks = G.Blocks(gap_range(1, self.degree))
        action = G.ActionHomomorphism(blocks, libgap.OnSets)
        primitive = Constellation3(*action.Image().GeneratorsOfGroup())
        if full and not primitive.monodromy_group().IsPrimitive():
            primitive = primitive.primitivization(full=full)
        return primitive


class ConstellationFlexibleEquivalence:
    r"""
    Two constellations are considered flexibly equivalent if they are isomorphic, up
    to action by a braid group.  That is, constellations :code:`s1` and :code:`s2` are
    flexibly equivalent if there exists a braid element :code:`b` so that
    :code:`s1 ** b` and :code:`s2` are isomorphic.  See [LZ2004],
    pg. 24-26 for more details.

    EXAMPLES::

        sage: from dessins import Constellation3, ConstellationFlexibleEquivalence
        sage: equivalence = ConstellationFlexibleEquivalence(
        ....:     "(0, 1, Infinity) |-> (0, Infinity, 1)",
        ....:     libgap.eval("(1,2,4)"),
        ....: )
        sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)", "(1,4,3)")
        sage: equivalence(c)
        ((2,4,3), (1,3,2), (1,2)(3,4))

    .. TODO::

        Replace use of strings in the lookup table with permutations or
        braid group elements.  Extend to work with constellations of
        arbitrary length.

    REFERENCES:

        [LZ2004] S. K. Lando and A. K. Zvonkin, Graphs on Surfaces and their Applications, Encyclopedia of Mathematical Sciences, Vol. 141, Springer-Verlag, 2004.
    """

    # Table which maps permutations of ``0``, ``1``, and ``Infinity`` to
    # braid actions.
    lookup_table = {
        "(0, 1, Infinity) |-> (0, 1, Infinity)": lambda y: y,
        "(0, 1, Infinity) |-> (1, 0, Infinity)": lambda y: Constellation3.braid_action(
            y, 0
        ),
        "(0, 1, Infinity) |-> (0, Infinity, 1)": lambda y: Constellation3.braid_action(
            y, 1
        ),
        "(0, 1, Infinity) |-> (1, Infinity, 0)": lambda y: Constellation3.braid_action(
            Constellation3.braid_action(y, 1), 0
        ),
        "(0, 1, Infinity) |-> (Infinity, 0, 1)": lambda y: Constellation3.braid_action(
            Constellation3.braid_action(y, 0), 1
        ),
        "(0, 1, Infinity) |-> (Infinity, 1, 0)": lambda y: Constellation3.braid_action(
            Constellation3.braid_action(Constellation3.braid_action(y, 0), 1), 0
        ),
    }

    def __init__(self, braid_action, conj):
        self._braid_action = braid_action
        self._conjugating_element = conj

    def __str__(self):
        return (
            f"Braid action by {self._braid_action}, Conjugation by "
            f"{self._conjugating_element}"
        )

    def __repr__(self):
        return (
            f"Braid action by {self._braid_action}, Conjugation by "
            f"{self._conjugating_element}"
        )

    def __call__(self, constellation):
        braided = self.lookup_table[self._braid_action](constellation)
        return braided**self._conjugating_element

    def moebius(self):
        r"""
        Returns the Mobius function permuting 0, 1, and Infinity in the way
        corresponding to the braid action in the flexible equivalence.

        EXAMPLES::

            sage: from dessins import ConstellationFlexibleEquivalence
            sage: equiv = ConstellationFlexibleEquivalence(
            ....:     "(0, 1, Infinity) |-> (1, Infinity, 0)", libgap.eval("()")
            ....: )
            sage: equiv.moebius()
            -1/(z - 1)
        """
        from ..utils import find_moebius

        pre, post = self._braid_action.split(" |-> ")
        return find_moebius(
            sage_eval(pre),
            sage_eval(post),
        )
