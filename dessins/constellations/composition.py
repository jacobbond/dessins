r"""
Computing the 3-constellation of a composition of Belyi maps

EXAMPLES::

    sage: from dessins import compose_dessins, BelyiMap, Constellation3
    sage: beta = BelyiMap(-27 * (x**3 - x**2) / 4)
    sage: gamma = BelyiMap(x**2)
    sage: composed = compose_dessins(
    ....:     beta.constellation(),
    ....:     beta.extending_pattern(),
    ....:     gamma.constellation(),
    ....: )
    sage: composed.canonical()
    ((2,3,5,4), (1,2)(5,6), (1,2,4,6,5,3))
    sage: libgap.Group(*composed).StructureDescription()
    "C2 x S4"

AUTHORS:

- Jacob Bond: initial implementation
"""

# isort: split
########## Sage imports ##########
from sage.libs.gap.libgap import libgap
from sage.rings.integer_ring import ZZ

##################################

# isort: split
########## this package ##########
from ..belyi_maps.monodromy import F2, A, B
from .constellation import Constellation3

##################################

#######################
#                     #
#  composing dessins  #
#                     #
#######################


def compose_dessins(
    constellation1: Constellation3,
    extending_pattern: "ExtendingPattern",  # noqa: F821
    constellation2: Constellation3,
):
    r"""
    Determine the monodromy of a composition of Belyi maps :math:`\beta \circ \gamma`, where
    :math:`\beta` has monodromy :code:`(t0, t1)` and extending pattern :code:`ext_pat` and
    where :math:`\gamma` has monodromy :code:`(s0, s1)`.

    INPUT:

    - ``constellation1`` -- 3-Constellation; the monodromy of :math:`\beta`

    - ``extending_pattern`` -- :class:`ExtendingPattern<..belyi_maps.extended_monodromy.ExtendingPattern>`;
      the extending pattern of :math:`\beta`

    - ``constellation2`` -- 3-Constellation; the monodromy of :math:`\gamma`

    EXAMPLES::

        sage: from dessins import compose_dessins, BelyiMap, Constellation3
        sage: beta = BelyiMap(-27 * (x**3 - x**2) / 4)
        sage: gamma = BelyiMap(x**2)
        sage: composed = compose_dessins(
        ....:     beta.constellation(),
        ....:     beta.extending_pattern(),
        ....:     gamma.constellation(),
        ....: )
        sage: composed.canonical()
        ((2,3,5,4), (1,2)(5,6), (1,2,4,6,5,3))
        sage: libgap.Group(*composed).StructureDescription()
        "C2 x S4"

    Directly composing the Belyi maps results in the same 3-constellation: ::

        sage: BelyiMap( (-27 * (x**3 - x**2) / 4)(x=x**2) ).constellation()
        ((2,3,5,4), (1,2)(5,6), (1,2,4,6,5,3))


    Note that the |Belyi| map for ``gamma`` is not required in order
    to compose dessins: ::

        sage: compose_dessins(
        ....:     beta.constellation(),
        ....:     beta.extending_pattern(),
        ....:     (
        ....:         libgap.eval('(1,2)(3,5)(4,6,7)(8,10,11,9)'),
        ....:         libgap.eval('(1,3,5,4)(6,7,8,10,11,9)')
        ....:     )
        ....: )
        ((1,5,4,2)(3,9,15,12)(7,14,13,8)(10,17,16,20,19,11)(18,21,24,30,33,27)(22,29,28,32,31,26,25,23), (2,3)(5,6)(8,9)(11,12)(14,15)(17,18)(20,21)(23,24)(26,27)(29,30)(32,33), (1,3,11,19,21,17,10,12,14,7,9,2,4,6,5)(8,13,15)(16,18,26,31,33,29,22,24,20)(23,25,27,32,28,30))


    Additional examples: ::

        sage: from dessins import Passport
        sage: p = Passport([4,1], [4,1], [2,2,1])
        sage: beta = p.belyi_maps(d=5, h=500, subs_dict={var("a0"): 0, var("a1"): -1})[0]
        ...
        Found 1 Galois orbits.
        sage: # choose a specific embedding to ensure doctest consistency:
        sage: beta = [
        ....:     b for b in beta.conjugates()
        ....:     if b.embedding().real() < 0 and b.embedding().imag() > 0
        ....: ][0]
        sage: gamma = BelyiMap(x**2 / (x**2 - 1), "y**2 = x**3 - x")
        sage: c1 = beta(gamma).constellation().canonical(); c1
        ((1,2,4,8,14,19,20,15,9,11,5,10,16,18,13,3)(6,7,12,17), (1,3,6,5)(4,9,15,12)(7,13,14,8)(10,17,20,16), (2,5,11,4)(3,7)(6,10)(8,12)(13,18,20,19)(15,17))
        sage: c2 = compose_dessins(
        ....:     beta.constellation(),
        ....:     beta.extending_pattern(),
        ....:     gamma.constellation(),
        ....: ).canonical(); c2
        ((1,2,4,8,14,19,20,15,9,11,5,10,16,18,13,3)(6,7,12,17), (1,3,6,5)(4,9,15,12)(7,13,14,8)(10,17,20,16), (2,5,11,4)(3,7)(6,10)(8,12)(13,18,20,19)(15,17))
        sage: c1 == c2
        True

    .. |Belyi| unicode:: Bely U+012D .. Belyi
    """
    from ..utils import coerce_perm

    t0, t1, s0, s1 = [
        coerce_perm(perm, "gap") for perm in constellation1[:2] + constellation2[:2]
    ]
    deg_n = max([t.LargestMovedPoint() for t in [t0, t1]])
    deg_m = max([s.LargestMovedPoint() for s in [s0, s1]])

    f0, f1 = extending_pattern.apply_monodromy_representation(
        libgap.GroupHomomorphismByImagesNC(
            F2,
            libgap.Group(t0, t1),
            [A, B],
            [s0, s1],
        )
    )
    # deg = deg_n * deg_m
    eta0, eta1 = [], []  # deg * [0], deg * [0]
    for i in map(ZZ, range(1, deg_m + 1)):
        for j in map(ZZ, range(1, deg_n + 1)):
            eta0.append((i ** f0[j - 1] - 1) * deg_n + j**t0)
            eta1.append((i ** f1[j - 1] - 1) * deg_n + j**t1)
    return Constellation3(libgap.PermList(eta0), libgap.PermList(eta1))


class ExtendedMonodromy:
    r"""
    The extended monodromy of a Belyi map, consisting of the extending pattern
    and the monodromy of the Belyi map.

    INPUT:

    - ``extending_pattern`` -- :class:`ExtendingPattern`; the extending pattern of the Belyi map

    - ``monodromy`` -- :class:`Constellation3<..constellations.constellation.Constellation3>`; the monodromy of the Belyi map

    EXAMPLES::

        sage: from dessins import ExtendingPattern, ExtendedMonodromy, Constellation3
        sage: ext_pat = ExtendingPattern(
        ....:     "[ID, ID, ID, A, ID]", "[ID, B, ID, A, A**-1]"
        ....: )
        sage: mon = Constellation3("(3,4,5)", "(1,3,2)(4,5)")
        sage: ext_mon = ExtendedMonodromy(ext_pat, mon)
        sage: ext_mon.extended_monodromy_group.GeneratorsOfGroup()
        [ WreathProductElement(<identity ...>,<identity ...>,<identity ...>,a,<identity ...>,(3,4,5)), WreathProductElement(<identity ...>,b,<identity ...>,a,a^-1,(1,3,2)(4,5)) ]

    Given two Belyi maps :math:`\beta` and :math:`\gamma`, the monodromy group of :math:`\beta \circ \gamma` can be
    computed by applying the monodromy representation of :math:`\gamma` to the first component of the extended monodromy group. ::

        sage: from dessins import BelyiMap, Constellation3
        sage: beta = BelyiMap(x**3)
        sage: gamma = BelyiMap(3*x**2 - 2*x**3)
        sage: emon_beta = beta.extended_monodromy()
        sage: emon_beta.apply_to_extended_monodromy(
        ....:     gamma.monodromy_representation()
        ....: ).StructureDescription()
        "(S3 x S3 x S3) : C3"

    The following example shows that although :math:`\operatorname{\mathrm{Mon}} \beta \circ \delta` is a group extension of :math:`\operatorname{Mon} \beta`,
    :math:`\operatorname{\mathrm{Mon}} \beta \circ \delta` is not, in general, a split extension.  Let :math:`\delta` be a Belyi map with 3-constellation
    :math:`((2,3,4), (1,2)(3,4), (1,2,3))`, such as :math:`\frac{x^{3}(x+8)}{64(x-1)}`.  Then :code:`ComplementClassesRepresentatives`
    determines representatives of each conjugacy class of complements of :math:`\operatorname{Mon} \beta` in :math:`\operatorname{Mon} \beta \circ \delta`,
    for which there are none. ::

        sage: delta = Constellation3("(2,3,4)", "(1,2)(3,4)")
        sage: mon_beta_delta = emon_beta.apply_to_extended_monodromy(
        ....:     delta.monodromy_representation()
        ....: )
        sage: mon_ext_composed = emon_beta.apply_to_monodromy_extending(
        ....:     delta.monodromy_representation()
        ....: )
        sage: libgap.ComplementClassesRepresentatives(mon_beta_delta, mon_ext_composed)
        [  ]
    """

    def __init__(self, extending_pattern, monodromy):
        self.extending_pattern = extending_pattern
        self.monodromy = monodromy

        MonBeta = monodromy.monodromy_group()
        self._wr = F2.WreathProduct(MonBeta)

        # tau0, tau1 = (
        #     wr.Embedding(len(extending_pattern[0]) + 1).Image().GeneratorsOfGroup()
        # )

        self._f0tau0 = libgap.WreathProductElementList(
            self._wr, libgap(self.extending_pattern.around0 + [monodromy[0]])
        )
        self._f1tau1 = libgap.WreathProductElementList(
            self._wr, libgap(self.extending_pattern.around1 + [monodromy[1]])
        )
        self._extended_monodromy_representation = libgap.GroupHomomorphismByImagesNC(
            F2,
            self._wr,
            [A, B],
            [self._f0tau0, self._f1tau1],
        )

        self._extended_monodromy_group = self._extended_monodromy_representation.Image()

        mon_rep = libgap.GroupHomomorphismByImagesNC(F2, MonBeta)
        self._monodromy_extending_group = self._extended_monodromy_representation.Image(
            mon_rep.Kernel()
        )

    def __repr__(self):
        return (
            f"ExtendedMonodromy({repr(self.extending_pattern)}, {repr(self.monodromy)})"
        )

    def __str__(self):
        return f"ExtendedMonodromy with ExtendingPattern {self.extending_pattern} and Monodromy {self.monodromy})"

    @property
    def extended_monodromy_group(self):
        return self._extended_monodromy_group

    @property
    def extended_monodromy_representation(self):
        return self._extended_monodromy_representation

    @property
    def monodromy_extending_group(self):
        return self._monodromy_extending_group

    def apply_to_extended_monodromy(self, other):
        r"""
        Apply the group homomorphism :code:`other` to the extended monodromy group of :code:`self`.
        """
        return _postcompose_with_base_group(other, self._extended_monodromy_group)

    def apply_to_monodromy_extending(self, other):
        r"""
        Apply the group homomorphism :code:`other` to the monodromy extending group of :code:`self`.
        """
        return _postcompose_with_base_group(other, self._monodromy_extending_group)

    # def compose_with(self, other: Union[BelyiMap, Constellation3]):
    #     from ..constellation import compose_dessins
    #
    #     if isinstance(other, BelyiMap):
    #         monodromy = other.constellation()
    #     else:
    #         monodromy = other
    #     return compose_dessins(
    #         *self.monodromy, *self.extending_pattern, *(monodromy[:2])
    #     )


def _postcompose_with_base_group(homomorphism, wreath_subgroup):
    # use parent in case wreath_group is a subgroup of a wreath product:
    wreath_projection = wreath_subgroup.Parent().Projection()
    top_group = wreath_projection.Image()
    wreath_subgroup_projected = wreath_projection.Image(wreath_subgroup)
    image_wreath = libgap.WreathProduct(homomorphism.Image(), top_group)
    gens_and_projections = zip(
        wreath_subgroup.GeneratorsOfGroup(),
        wreath_subgroup_projected.GeneratorsOfGroup(),
    )
    gen_images = [
        image_wreath.WreathProductElementList(
            _image_of_base_component(homomorphism, gen, wreath_subgroup) + [proj]
        )
        for gen, proj in gens_and_projections
    ]
    return libgap.Group(gen_images)


def _image_of_base_component(homomorphism, wreath_elt, wreath_subgp):
    return [
        homomorphism.Image(elt)
        for elt in list(wreath_subgp.Parent().ListWreathProductElement(wreath_elt))[:-1]
    ]


def find_kernel_elements(ext_mon, max_length):
    r"""
    Given the extended monodromy of a dessin with monodromy :code:`(t0, t1)` and extending pattern :code:`(f0, f1)`,
    finds words in the kernel of :math:`\langle f_{0} \rtimes t_{0}, f_{1} \rtimes t_{1} \rangle \rightarrow \langle t_{0}, t_{1} \rangle`
    which have length at most :code:`max_length`.

    INPUT:

    - ``ext_mon`` -- :class:`ExtendedMonodromy`; the extended monodromy of a dessin

    - ``max_length`` -- the maximum length of words being considered

    OUTPUT: words in the kernel of :math:`\langle f_{0} \rtimes t_{0}, f_{1} \rangle \rtimes t_{1} \rightarrow \langle t_{0}, t_{1} \rangle`
    which have length at most :code:`max_length`

    EXAMPLES::

        sage: from dessins import find_kernel_elements, ExtendingPattern, ExtendedMonodromy, Constellation3
        sage: ext_pat = ExtendingPattern(
        ....:     "[ID, ID, ID, A, ID]", "[ID, B, ID, A, A**-1]"
        ....: )
        sage: mon = Constellation3("(3,4,5)", "(1,3,2)(4,5)")
        sage: ext_mon = ExtendedMonodromy(ext_pat, mon)
        sage: find_kernel_elements(ext_mon, 8)
        [WreathProductElement(<identity ...>,<identity ...>,<identity ...>,<identity ...>,<identity ...>,()),
         WreathProductElement(<identity ...>,<identity ...>,a,a,a,()),
         WreathProductElement(<identity ...>,<identity ...>,a^2,a^2,a^2,()),
         WreathProductElement(b^2,b^2,b^2,<identity ...>,<identity ...>,()),
         WreathProductElement(a*b,b*a,a*b,<identity ...>,b*a,()),
         WreathProductElement(b^2,b^2,a*b,<identity ...>,b*a,()),
         WreathProductElement(a*b,b*a,b*a,a*b,<identity ...>,()),
         WreathProductElement(a*b,b^2,b^2,a*b,<identity ...>,()),
         WreathProductElement(b^2,b*a,b^2,<identity ...>,b*a,()),
         WreathProductElement(b^2,b^2,b*a,a*b,<identity ...>,())]
    """

    eta0, eta1 = ext_mon._f0tau0, ext_mon._f1tau1
    wr = ext_mon._wr
    mon = ext_mon.monodromy.monodromy_group()

    # sets to hold words which have not been seen, words which
    # have been seen, and words in the kernel
    new, seen, ker = [wr.Identity()], set([wr.Identity()]), [wr.Identity()]
    proj = wr.Projection()  # projection onto <t0, t1>
    ident = mon.One()  # identity of <t0, t1>

    for _ in range(max_length):
        queue = new  # new words to consider
        new = []  # for g1,g2 in queue, possible that eta0*g1 = eta1*g2
        for g in queue:
            for eta in eta0, eta1:
                elt = g * eta
                # if elt has already been seen, it has already been
                # multiplied by each eta and so can be discarded
                if elt not in seen:
                    seen.add(elt)
                    new.append(elt)
                    if proj.Image(elt) == ident:
                        ker.append(elt)
    return ker
