# check if this is being imported by a Sage or Python script
import sys  # isort: skip

if ".py" in sys.argv[0] or ".sage" in sys.argv[0]:
    import sage.all  # noqa: F401

from .belyi_maps import *  # noqa: F401, F403
from .constellations.composition import *  # noqa: F401, F403
from .constellations.constellation import *  # noqa: F401, F403
from .constellations.generation import *  # noqa: F401, F403
from .constellations.utils import *  # noqa: F401, F403
from .counting import *  # noqa: F401, F403
from .dessin import *  # noqa: F401, F403
from .drawing import *  # noqa: F401, F403
from .passport import *  # noqa: F401, F403
from .utils import *  # noqa: F401, F403

__version__ = "0.0.3"
