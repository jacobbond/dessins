r"""
Passports for dessins d'enfants

EXAMPLES::

    sage: from dessins import Passport
    sage: p = Passport([ [3,1], [2,2], [3,1] ])
    sage: list(p.constellations())
    [((2,3,4), (1,2)(3,4), (1,2,3))]
    sage: p.belyi_maps(d=1, h=10**3, subs_dict={var("a0"): 0, var("c0"): 1})
    ...
    [Belyi map defined by (-1/64*x^4 + 1/8*x^3)/(x + 1) on Projective Space of dimension 1 over Rational Field]

AUTHORS:

- Jacob Bond: initial implementation

.. |Belyi| unicode:: Bely U+012D .. Belyi
"""

from collections.abc import Iterator, Sequence

# check if this is being imported by a Sage or Python script
import sys  # isort: skip

if ".py" in sys.argv[0] or ".sage" in sys.argv[0]:
    import sage.all  # noqa: F401

# isort: split
########## Sage imports ##########
from sage.rings.infinity import Infinity

##################################

# isort: split
########## this package ##########
from .utils import enlist

##################################


####################
#                  #
#  Passport Class  #
#                  #
####################


# TODO: should Passport inherit from Sequence?
class Passport:
    r"""
    A class for passports of dessins d'enfants

    A :class:`.Passport` class instance is an object which behaves similarly to a list of
    lists, the inner lists being representations of cycle types of permutations.
    Instances may consist of either two or three cycle types only.  Instances
    can be compared, indexed into, and sliced.  The :class:`.Passport` class supports
    using the index Infinity as a synonym for the index 2.

    INPUT:

    - ``*types`` -- variable length list of list, tuple, or iterator instances;
      cycle types can be passed either individually or inside of a list,
      tuple, or iterator.

    .. NOTE::
        A `ValueError` is raised if
            - ``types`` has too many or too few elements,
            - an element of ``types`` is not iterable,
            - a cycle length (a 2-deep element of ``types``, or 3-deep if ``types`` has length 1) is iterable,
            - any cycle length is nonpositive,
            - all cycle types don't have the same degree.

    EXAMPLES::

        sage: from dessins import Passport

    Passport elements can be passed within a container. ::

        sage: Passport([ [3,2], [5], [4,1] ])
        [[2, 3], [5], [1, 4]]

    Passport elements can also be passed as seperate arguments. ::

        sage: Passport([3, 2], [5], [4, 1])
        [[2, 3], [5], [1, 4]]

    In addition to the conventional three element passports for dessins
    d'enfants, :class:`.Passport` also permits the creation of passports consisting
    of two elements, since a pair of permutations generating a transitive
    group specifies a dessin d'enfant.  ::

        sage: Passport([ [3,2], [5] ])
        [[2, 3], [5]]
        sage: Passport([3, 2], [5])
        [[2, 3], [5]]

    :class:`.Passport` can generate the 3-constellations with the given type. ::

        sage: p = Passport([ [3,1], [2,2], [3,1] ])
        sage: list(p.constellations())
        [((2,3,4), (1,2)(3,4), (1,2,3))]

    :class:`.Passport` also supports many list-like operations, with the added
    capability of accessing the third cycle type using the index :code:`Infinity`. ::

        sage: p = Passport([ [3,1], [2,2], [3,1] ])
        sage: p[1:]
        [[2, 2], [1, 3]]
        sage: p[2]
        [1, 3]
        sage: p[Infinity]
        [1, 3]
        sage: p[:Infinity] = [[2, 2], [1, 3]]
        sage: p
        [[2, 2], [1, 3], [1, 3]]

    .. NOTE:
        This class has the following (private) attributes:
        - ``_pass`` -- list; The internal list representation of the passport.
    """

    def __init__(self, *types):
        r"""
        Constructs an instance of the Passport class.
        """
        # if only one item is passed and it is iterable, assume
        # that the passport was passed as a sequence
        if len(types) == 1 and isinstance(types[0], (Sequence, Iterator, Passport)):
            types = enlist(types[0])
        if len(types) < 2:
            raise ValueError("A passport must contain at least two cycle types.")
        for elt in types:
            _check_cycle_type_format(elt)
        degree = sum(types[0])
        # if the degree is 0 or all elements don't sum to the
        # same degree
        if degree == 0 or any(sum(t) != degree for t in types[1:]):
            raise ValueError("Each cycle type must have the same positive degree.")
        # if any cycle length is not positive
        if any(u <= 0 for t in types for u in t):
            raise ValueError("Each cycle length must be positive.")
        self._pass = list(map(sorted, types))

    def __eq__(self, other):
        r"""
        Evaluates whether the given passports are equal.  Two passports
        are considered equal if they differ only in the ordering of
        cycle lengths in each of their cycle types.

        EXAMPLES::

            sage: from dessins import Passport
            sage: Passport([1,2,1], [3,1], [2,1,1]) == (
            ....:     Passport([1,1,2], [1,3], [1,2,1])
            ....: )
            True
            sage: Passport([1,2,1], [3,1], [2,1,1]) == (
            ....:     Passport([1,3], [1,1,2], [1,2,1])
            ....: )
            False
            sage: Passport([1,2,1], [3,1], [2,1,1]) == (
            ....:     [[1,1,2], [1,3], [1,2,1]]
            ....: )
            True
        """
        if isinstance(other, Passport):
            other = other._pass
        return list(map(sorted, self._pass)) == list(map(sorted, other))

    def __ne__(self, other):
        if isinstance(other, Passport):
            other = other._pass
        return list(map(sorted, self._pass)) != list(map(sorted, other))

    def __gt__(self, other):
        if isinstance(other, Passport):
            other = other._pass
        return list(map(sorted, self._pass)) > list(map(sorted, other))

    def __ge__(self, other):
        if isinstance(other, Passport):
            other = other._pass
        return list(map(sorted, self._pass)) >= list(map(sorted, other))

    def __lt__(self, other):
        if isinstance(other, Passport):
            other = other._pass
        return list(map(sorted, self._pass)) < list(map(sorted, other))

    def __le__(self, other):
        if isinstance(other, Passport):
            other = other._pass
        return list(map(sorted, self._pass)) <= list(map(sorted, other))

    def __repr__(self):
        return repr(self._pass)

    def __str__(self):
        return str(self._pass)

    def __len__(self):
        return len(self._pass)

    def __iter__(self):
        return iter(self._pass)

    def __getitem__(self, pos):
        if isinstance(pos, slice):
            if pos.start is Infinity:
                pos = slice(2, pos.stop, pos.step)
            if pos.stop is Infinity:
                pos = slice(pos.start, 2, pos.step)
        else:
            if pos is Infinity:
                pos = 2
        return self._pass[pos]

    def __setitem__(self, pos, new):
        if isinstance(pos, slice):
            if pos.start is Infinity:
                pos = slice(2, pos.stop, pos.step)
            if pos.stop is Infinity:
                pos = slice(pos.start, 2, pos.step)
            # slice.indices(l) returns start, stop, step assuming as sequence
            # of length l:
            pos = slice(*pos.indices(len(self._pass)))
            for elt in new:
                _check_cycle_type_format(elt, sum(self._pass[0]))
        else:
            if pos is Infinity:
                pos = 2
            _check_cycle_type_format(new, sum(self._pass[0]))
        self._pass[pos] = new

    def __delitem__(self, pos):
        if len(self._pass) < 3:
            raise ValueError("Resulting passport would have too few elements.")
        if isinstance(pos, slice):
            if pos.start is Infinity:
                pos = slice(2, pos.stop, pos.step)
            if pos.stop is Infinity:
                pos = slice(pos.start, 2, pos.step)
        else:
            if pos is Infinity:
                pos = 2
        del self._pass[pos]

    # __delslice__ is not implemented because deleting more than one
    # element would result in a passport with too few elements

    def __contains__(self, other):
        return other in self._pass

    @property
    def passport(self):
        return self._pass

    @property
    def degree(self):
        r"""
        The degree of the passport.

        EXAMPLES::

            sage: from dessins import Passport
            sage: Passport([1, 3, 2], [5, 1], [2, 2, 1, 1]).degree
            6
        """
        return sum(self._pass[0])

    @property
    def genus(self):
        r"""
        The genus of the passport.  Requires the passport to be fully specified.

        EXAMPLES::

            sage: from dessins import Passport
            sage: Passport([2, 2, 4, 4], [10, 2], [4, 4, 2, 2]).genus
            2
        """
        # if len(self._pass) < 3:
        #     raise ValueError("Partial passports have no genus.")
        eul_char = sum(map(len, self._pass)) - sum(self._pass[0])
        return int((2 - eul_char) / 2)

    def equiv(self, other):
        r"""
        Evaluates whether the given passports are equivalent.  Two passports
        are considered equivalent if they are equal under some reordering of
        its cycle types.

        EXAMPLES::

            sage: from dessins import Passport
            sage: Passport([1,2,1], [3,1], [2,1,1]).equiv(
            ....:     Passport([1,1,2], [1,3], [1,2,1])
            ....: )
            True
            sage: Passport([1,2,1], [3,1], [2,1,1]).equiv(
            ....:     Passport([1,3], [1,1,2], [1,2,1])
            ....: )
            True
            sage: Passport([1,2,1], [3,1], [2,1,1]).equiv(
            ....:     [[1,1,2], [1,3], [1,2,1]]
            ....: )
            True
        """
        if isinstance(other, Passport):
            other = other._pass
        return sorted(map(sorted, self._pass)) == sorted(map(sorted, other))

    def belyi_maps(self, d, h, subs_dict=None, **kwargs):
        r"""
        Returns the |Belyi| maps with the same passport as :code:`self`.  Only supports
        computation of genus 0 |Belyi| maps.

        INPUT:

        - ``d`` -- positive integer; the degree bound to use when finding
          the minimal polynomial of the field of definition of the |Belyi| map

        - ``h`` -- positive integer; the height bound to use when finding
          the minimal polynomial of the field of definition of the |Belyi| map

        - ``subs_dict`` -- ``dict`` (default: ``None``); a dictionary providing variable substitutions
          for two of the indeterminates in the generic form for the |Belyi| map
          with passport matching that of ``self`` (The generic form uses variables ``ai`` for the zeros
          of the map in order of decreasing degree.  The variables ``bi`` and ``ci`` are used similarly
          for the preimages of 1 and Infinity, with the preimage of Infinity of largest degree being
          located at Infinity.)

        EXAMPLES::

            sage: from dessins import Passport
            sage: Passport([3,1],[2,2],[3,1]).belyi_maps(4, 10**3, subs_dict={var("a0"): 1, var("c0"): 0})
            ...
            Found 1 Galois orbits.
            [Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field]
        """
        # TODO: assess whether to cache Belyi maps
        try:
            return self._belyis
        except AttributeError:
            pass
        if self.genus > 0:
            raise NotImplementedError(
                "Computation of Belyi maps for genera greater than 0 is not implemented."
            )
        else:
            from dessins.belyi_maps.compute_belyi import compute_belyi_maps

            belyis = compute_belyi_maps(*self._pass, d, h, subs_dict=subs_dict)
            self._belyis = belyis
            return belyis

    def constellations(self, as_tuple=False, algorithm="double cosets", unique=True):
        r"""
        Returns the 3-constellations with the given passport.

        EXAMPLES::

            sage: from dessins import Passport
            sage: list(
            ....:     Passport([5,1], [4,2], [4,2]).constellations()
            ....: )
            [((1,6,3,2,4), (1,3)(2,4,6,5), (1,2)(3,4,5,6)),
             ((1,6,3,2,5), (1,3)(2,5,4,6), (1,2)(3,4,5,6)),
             ((1,5,6,3,4), (1,2,4,6)(3,5), (1,2)(3,4,5,6)),
             ((1,5,2,4,6), (1,5,2,6)(3,4), (1,2)(3,4,5,6))]
        """
        from .constellations.generation import passport_to_constellations

        tau0, tau1 = self._pass[:2]
        if len(self._pass) == 3:
            tau_inf = self._pass[2]
        else:
            tau_inf = None
        sym_size = sum(tau0)
        return passport_to_constellations(
            tau0,
            tau1,
            tau_inf,
            sym_size,
            as_tuple=as_tuple,
            algorithm=algorithm,
            unique=unique,
        )

    def num_constellations(self):
        r"""
        Returns the number of 3-constellations with the given passport.

        EXAMPLES::

            sage: from dessins import Passport
            sage: Passport([6,1], [4,2,1], [3,2,1,1]).num_constellations()
            12
        """
        return sum(1 for _ in self.constellations())


def _check_cycle_type_format(cycle_type, deg=None):
    if (
        not hasattr(cycle_type, "__iter__")
        # some element of cycle_type is iterable
        or any(hasattr(t, "__iter__") for t in cycle_type)
    ):
        raise ValueError("New value is not well formed.")
    # cycle_type doesn't have the same degree as self
    if deg is not None and sum(cycle_type) != deg:
        raise ValueError("New value does not have the correct degree.")
