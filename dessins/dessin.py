r"""
Abstract dessins d'enfants and their properties

Because dessins d'enfants have several equivalent represenations,
:class:`.Dessin` facilitates conversion between various representations.

EXAMPLES::

    sage: from dessins import Dessin, BelyiMap
    sage: d = Dessin(
    ....:     BelyiMap( (x + 9) * (x + 1)**3 / (64*x) )
    ....: )
    sage: d.constellation()
    ((2,3,4), (1,2)(3,4), (1,2,3))
    sage: d.drawing()
    {0: [(2, 1)], 1: [(2, 2), (3, 3), (3, 4)], 2: [(0, 1), (1, 2)], 3: [(1, 3), (1, 4)]}


AUTHORS:

- Jacob Bond: initial implementation

.. |Belyi| unicode:: Bely U+012D .. Belyi
"""

# isort: split
########## Sage imports ##########

##################################

##################
#                #
#  Dessin Class  #
#                #
##################


class Dessin:
    r"""
    The abstract dessin associated with various dessin representations.

    INPUT:

    - ``dessin`` -- :class:`.BelyiMap`, :class:`.Constellation3`, or :class:`.Drawing`; The dessin
      to create a :class:`.Dessin` object for

    EXAMPLES::

        sage: from dessins import Dessin, BelyiMap, Constellation3, Drawing
        sage: Dessin(
        ....:     Constellation3('(1,2,3)', '(1,2)(3,4)', '(1,4,3)')
        ....: )
        Dessin with constellation ((1,2,3), (1,2)(3,4), (1,4,3))
        sage: Dessin(
        ....:     BelyiMap( (x + 9) * (x + 1)**3 / (64*x) )
        ....: )
        Dessin with Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field
        sage: Dessin( Drawing(
        ....:     {0: [(1, 4)], 1: [(0, 4), (2, 3)], 2: [(1, 3), (3, 1), (3, 2)], 3: [(2, 1), (2, 2)]}
        ....: ) )
        Dessin with drawing {0: [(1, 4)], 1: [(0, 4), (2, 3)], 2: [(1, 3), (3, 1), (3, 2)], 3: [(2, 1), (2, 2)]}

    .. NOTE::

        This class has the following attributes:

        * ``_constellation`` -- Constellation; The 3-constellation for the dessin.
        * ``_belyis`` -- BelyiMap; The |Belyi| maps with the same passport as the dessin.
        * ``_drawing`` -- Drawing; The drawing for the dessin.
        * ``_genus`` -- nonnegative integer; The genus of the dessin.
        * ``_degree`` -- nonnegative integer; The degree of the dessin.

    TESTS: pytest
    """

    def __init__(self, dessin):
        from .belyi_maps.belyi_map import BelyiMap
        from .constellations.constellation import Constellation3
        from .drawing import Drawing

        if isinstance(dessin, Constellation3):
            self._constellation = dessin
        elif isinstance(dessin, BelyiMap):
            self._belyi = dessin
        elif isinstance(dessin, Drawing):
            self._drawing = dessin
        else:
            raise TypeError(
                "Argument must be of type Constellation3, BelyiMap, or Drawing."
            )

        self._genus = dessin.genus
        self._degree = dessin.degree

    def __eq__(self, other):
        return self.constellation().is_isomorphic(other.constellation())

    def __repr__(self):
        if hasattr(self, "_constellation"):
            return f"Dessin with constellation {repr(self._constellation)}"
        elif hasattr(self, "_belyi"):
            return f"Dessin with {repr(self._belyi)}"
        elif hasattr(self, "_drawing"):
            return f"Dessin with drawing {repr(self._drawing)}"

    def passport(self):
        r"""
        Returns the passport of :code:`self`.

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: Dessin(
            ....:     Constellation3('(1,2,3)', '(1,2)(3,4)', '(1,4,3)')
            ....: ).passport()
            [[1, 3], [2, 2], [1, 3]]

        TESTS: pytest
        """
        return self.constellation().passport()

    def constellation(self):
        r"""
        Returns a constellation for :code:`self`.

        If :code:`self` doesn't currently have a :class:`.Constellation3` defined,
        computes a :class:`.Constellation3` from the associated
        :class:`.BelyiMap` or :class:`.Drawing`.

        EXAMPLES::

            sage: from dessins import Dessin, BelyiMap
            sage: Dessin(
            ....:     BelyiMap( (x + 9) * (x + 1)**3 / (64*x) )
            ....: ).constellation()
            ((2,3,4), (1,2)(3,4), (1,2,3))

        TESTS: pytest
        """
        if hasattr(self, "_constellation"):
            return self._constellation
        elif hasattr(self, "_drawing"):
            the_constellation = self._drawing.constellation()
        elif hasattr(self, "_belyi"):  # and self._genus < 1:
            the_constellation = self._belyi.constellation()
            # elif hasattr(self, "_belyi") and self._genus >= 1:
            # raise ValueError(
            #     "Computation of 3-constellations from a Belyi "
            #     "map is only supported for genera <= 1."
            # )
        else:
            raise AttributeError("Dessin has no determining attributes defined.")
        return the_constellation

    def drawing(self):
        r"""
        Returns a drawing for :code:`self`.

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: Dessin( Constellation3('(1,2,3)', '(1,2)(3,4)') ).drawing()
            {0: [(2, 1), (2, 2), (3, 3)], 1: [(3, 4)], 2: [(0, 1), (0, 2)], 3: [(0, 3), (1, 4)]}

        TESTS: pytest
        """
        return self.constellation().drawing()

    def belyi_map(self, d=None, h=None, subs_dict=None, **kwargs):
        r"""
        Returns a |Belyi| map for :code:`self`.  Only supports
        computation of genus 0 |Belyi| maps.

        INPUT:

        - ``d`` -- positive integer; the degree bound to use when finding
          the minimal polynomial of the field of definition of the |Belyi| map

        - ``h`` -- positive integer; the height bound to use when finding
          the minimal polynomial of the field of definition of the |Belyi| map

        - ``subs_dict`` -- ``dict`` (default: ``None``); a dictionary providing variable substitutions
          for two of the indeterminates in the generic form for the |Belyi| map
          with passport matching that of ``self`` (The generic form uses variables ``ai`` for the zeros
          of the map in order of decreasing degree.  The variables ``bi`` and ``ci`` are used similarly
          for the preimages of 1 and Infinity, with the preimage of Infinity of largest degree being
          located at Infinity.)

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: d = Dessin( Constellation3("(1,2,3)", "(1,2)(3,4)") )
            sage: d.belyi_map(4, 10**3, subs_dict={var("a0"): 1, var("c0"): 0})
            ...
            Found 1 Galois orbits.
            Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field

        TESTS: pytest
        """
        from .constellations.constellation import Constellation3

        return Constellation3.belyi_map(self, d, h, subs_dict, **kwargs)
        # self.CombOrbit = self.CombinatorialOrbit().belyi_map(d, h, **kwargs)
        # for dessin in self.CombOrbit:
        #     if ( self.constellation() == dessin.constellation() ):
        #         self._belyi = dessin._belyi
        #         return self._belyi

    @property
    def genus(self):
        r"""
        Returns the genus of :code:`self`.

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: Dessin(
            ....:     Constellation3('(1,2,3,9)(4,5,6,7,8)', '(1,2,3,4)(5,6,7,8)')
            ....: ).genus
            2

        TESTS: pytest
        """
        return self._genus

    @property
    def degree(self):
        r"""
        Returns the degree of :code:`self`.

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: Dessin(
            ....:     Constellation3('(1,2,3,9)(4,5,6,7,8)', '(1,2,3,4)(5,6,7,8)')
            ....: ).degree
            9

        TESTS: pytest
        """
        return self._degree

    def monodromy_group(self):
        r"""
        Returns the monodromy group of :code:`self`.

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: Dessin(
            ....:     Constellation3('(1,2,3)', '(1,2)(3,4)', '(1,4,3)')
            ....: ).monodromy_group()
            Group([ (1,2,3), (1,2)(3,4) ])

        TESTS: pytest
        """
        return self.constellation().monodromy_group()

    def combinatorial_orbit(self):
        r"""
        Returns the combinatorial orbit of :code:`self`.

        EXAMPLES::

            sage: from dessins import Dessin, Constellation3
            sage: Dessin(
            ....:     Constellation3('(1,2,3)(4,5,6)', '(1,2,3,4)(5,6)')
            ....: ).combinatorial_orbit()
            [((1,2,3)(4,5,6), (1,2)(3,4,5,6), (1,6,4,5,3)),
             ((1,2,3)(4,5,6), (1,4)(2,3,6,5), (1,2,4,3,5))]

        TESTS: pytest
        """
        # TODO: Once a CombinatorialOrbit class is created, use it here:
        # return CombinatorialOrbit(*self.passport())
        return list(self.passport().constellations())

    # def galois_orbit(self):
    #     r"""
    #     Returns the Galois orbit of :code:`self`.

    #     .. TODO::

    #         After :code:`belyi_map.py` interface has been improved, add ability for
    #         this method to return a Galois orbit.
    #     """
    #     raise NotImplementedError
