r"""
The :class:`.Drawing` class for computing with dessins d'enfants as graphs

EXAMPLES::

    sage: from dessins import Drawing

AUTHORS:

- Jacob Bond: initial implementation
"""

import collections

# check if this is being imported by a Sage or Python script
import sys  # isort: skip

if ".py" in sys.argv[0] or ".sage" in sys.argv[0]:
    import sage.all  # noqa: F401

# isort: split
########## Sage imports ##########
from sage.graphs.bipartite_graph import BipartiteGraph
from sage.graphs.graph import Graph
from sage.groups.perm_gps.constructor import PermutationGroupElement
from sage.groups.perm_gps.permgroup_named import SymmetricGroup
from sage.libs.gap.libgap import libgap

################################

# isort: split
########## this package ##########
from .constellations.constellation import Constellation3

################################

#########################
#                       #
#  drawings and graphs  #
#                       #
#########################


class Drawing:
    r"""
    The underlying data structure for the :class:`Drawing` class is a :code:`dict`
    whose keys are the vertex indices.  The value at key :code:`v` is a list of 2-tuples,
    where each 2-tuple contains the vertex index :code:`w` of a neighbor of :code:`v` and
    the label of an edge joining :code:`v` and :code:`w`.  The order of the list determines
    the cyclic ordering of the edges around each vertex.

    EXAMPLES::

        sage: from dessins import Constellation3
        sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)")
        sage: dr = c.drawing(); dr
        {0: [(2, 1), (2, 2), (3, 3)], 1: [(3, 4)], 2: [(0, 1), (0, 2)], 3: [(0, 3), (1, 4)]}
        sage: dr.constellation()
        ((1,2,3), (1,2)(3,4), (1,4,3))
        sage: c = Constellation3("(1,2,3)", "(1,2,3)")
        sage: dr = c.drawing(); dr
        {0: [(1, 1), (1, 2), (1, 3)], 1: [(0, 1), (0, 2), (0, 3)]}
        sage: dr.constellation()
        ((1,2,3), (1,2,3), (1,2,3))
    """

    def __init__(self, drawing):
        if isinstance(drawing, dict):
            self._dict = drawing
            self._graph = BipartiteGraph(
                {v1: [v2 for v2, e in drawing[v1]] for v1 in drawing}
            )
        elif isinstance(drawing, Graph):
            # TODO: improve this:
            constellation = Constellation3(*graph_to_constellation(drawing))
            dr = constellation.drawing()
            self._dict = dr._dict
            self._graph = dr._graph
            # self._dict = drawing.to_dictionary(multiple_edges=True)
            # self._graph = BipartiteGraph(drawing)
        elif isinstance(drawing, Drawing):
            self._dict = drawing._dict
            self._graph = drawing._graph
        else:
            raise ValueError

    def __repr__(self):
        return str(self._dict)

    def __str__(self):
        return str(self._dict)

    def __eq__(self, other):
        return self.constellation() == Drawing(other).constellation()

    def constellation(self):
        if not hasattr(self, "_constellation"):
            # self._constellation = Constellation3(*drawing_to_constellation(self._dict))
            left, right = self._graph.bipartition()
            sigma0 = [str(tuple(e for _, e in self._dict[v])) for v in left]
            sigma1 = [str(tuple(e for _, e in self._dict[v])) for v in right]
            return Constellation3("".join(sigma0), "".join(sigma1))

        return self._constellation

    @property
    def genus(self):
        return self.constellation().genus

    @property
    def degree(self):
        return self.constellation().degree

    def plot(self, **kwargs):
        return self._graph.plot(partition=self._graph.bipartition(), **kwargs)


def drawing_to_constellation(drawing):
    drawing_dict = drawing._dict
    left, right = drawing._graph.bipartition()
    sigma0 = [tuple(e for _, e in drawing_dict[v]) for v in left]
    sigma1 = [tuple(e for _, e in drawing_dict[v]) for v in right]
    return (
        libgap.eval("".join(map(str, sigma0))),
        libgap.eval("".join(map(str, sigma1))),
    )


def graph_to_constellation(drawing):
    """
    This function only supports simple graphs due to the way that Sage handles
    combinatorial embeddings (see `set_embedding <https://doc.sagemath.org/html/en/reference/graphs/sage/graphs/generic_graph.html#sage.graphs.generic_graph.GenericGraph.set_embedding>`_).
    """
    if isinstance(drawing, Graph):
        G = BipartiteGraph(drawing)
        embedding = drawing.to_dictionary(multiple_edges=True)
    elif isinstance(drawing, dict):
        G = BipartiteGraph(drawing)
        embedding = drawing
    else:
        raise ValueError
    # embedding = G.get_embedding()
    # G = Graph(embedding)
    # get a bipartition of G:
    black, white = G.bipartition()
    # label the edges
    labels_for_edges = list(enumerate(G.edge_iterator(labels=False), start=1))

    # needed to allow working with multigraphs:
    # https://stackoverflow.com/a/2823367
    labelled_edges = collections.defaultdict(list)
    for key, value in labels_for_edges:
        labelled_edges[value].append(key)
    # add edges a second time to allow popping for both black and
    # white vertices:
    for key, value in labels_for_edges:
        labelled_edges[value].append(key)

    sigma0 = [
        tuple(labelled_edges[tuple(sorted((v, w)))].pop(0) for w in embedding[v])
        for v in black
    ]
    sigma1 = [
        tuple(labelled_edges[tuple(sorted((v, w)))].pop(0) for w in embedding[v])
        for v in white
    ]

    return PermutationGroupElement(sigma0), PermutationGroupElement(sigma1)


def constellation_to_drawing(sigma0, sigma1):
    r"""
    Takes a pair of permutations representing the constellation around 0
    and 1 and returns a dictionary for the embedding of the drawing.

    INPUT:

    - ``sigma0`` -- a permutation giving the constellation over 0

    - ``sigma1`` -- a permutation giving the constellation over 1

    OUTPUT: a graph dictionary giving a cyclic ordering of the edges
    around each vertex

    REFERENCES:

        [Bon2018] J. Bond, On the Computation and Composition of Belyi Maps and Dessins d'Enfants, Purdue University, 2018.
    """
    try:
        max_moved = max(max(sigma0.dict()), max(sigma1.dict()))
    except AttributeError:
        from .utils import coerce_perm

        sigma0, sigma1 = coerce_perm(sigma0, "sage"), coerce_perm(sigma1, "sage")
        max_moved = max(max(sigma0.dict()), max(sigma1.dict()))
    Sn = SymmetricGroup(max_moved)
    sigma0, sigma1 = Sn(sigma0), Sn(sigma1)

    # give each cycle a vertex label:
    constellation_over_0 = list(enumerate(sigma0.cycle_tuples(singletons=True)))
    constellation_over_1 = list(
        enumerate(sigma1.cycle_tuples(singletons=True), start=len(constellation_over_0))
    )
    # for each edge, find the vertex over 0/1 which it is incident to:
    incident_to_which_0 = {
        edge: vertex for vertex, edges in constellation_over_0 for edge in edges
    }
    incident_to_which_1 = {
        edge: vertex for vertex, edges in constellation_over_1 for edge in edges
    }
    # given a vertex v over 0 and an edge e around v, find the vertex
    # over 1 which edge e is incident to; track the edge ordering as well
    graph_dict = {
        vertex: [(incident_to_which_1[edge], edge) for edge in edges]
        for vertex, edges in constellation_over_0
    }
    graph_dict.update(
        {
            vertex: [(incident_to_which_0[edge], edge) for edge in edges]
            for vertex, edges in constellation_over_1
        }
    )
    return graph_dict


# excessively shortened version (untested):
# def constellation_to_drawing(*perms):
#    vertices_over = [[],[]]; incident_to_which = [[],[]]; graph_dict = {}
#    for i in [0, 1]:
#        vertices_over[i] = list( enumerate( perms[i].cycle_tuples(
#                    singletons=True), start=(i*len(vertices_over[0])) ) )
#        incident_to_which[i] = {edge:vertex for vertex,edges in vertices_over[i]
#                                                for edge in edges}
#    for i in [0, 1]:
#        graph_dict.update({vertex: [incident_to_which[i][edge]
#                    for edge in edges] for vertex,edges in vertices_over[i]})
#    return graph_dict

# graph_dict dict comprehension from above is equivalent to:
#
# graph_dict = {}
# for vertex,edges in one_vertex_constellation:
#    graph_dict[vertex] = []
#    for edge in edges:
#        graph_dict[vertex].append(incident_zero_vertex[edge])
