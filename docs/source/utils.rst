.. nodoctest

Utilities
=========

.. automodule:: dessins.utils
   :members:
   :undoc-members:
   :show-inheritance:
