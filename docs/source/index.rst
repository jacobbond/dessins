=================
Dessins d'Enfants
=================

.. mermaid::

    graph LR;
        id1(( )) --- id7(( ));
        id2(( )) --- id7;
        id3(( )) --- id7;
        id7 --- id5(( ));
        id7 --- id6(( )) --- id8(( )) --- id9(( ));
        id7 --- id4(( ));
        id8 --- id10(( ));
        classDef over0 fill:#000,stroke-width:2px,stroke:#777;
        classDef over1 fill:#fff,stroke-width:2px,stroke:#777;
        class id7,id8 over0;
        class id1,id2,id3,id4,id5,id6,id9,id10 over1;

The :code:`dessins` project (https://gitlab.com/dessins/dessins) is a library for
the SageMath open-source mathematics software system (https://www.sagemath.org/)
aimed at working with dessins d'enfants and |Belyi| maps.  The library
facilitates conversions between the different representations of a
dessin d'enfant, including (genus 0) |Belyi| maps,
3-constellations, and bipartite graphs.  Also included are additional
tools for computations involving 3-constellations, such as computing
the 3-constellation of a composition of |Belyi| maps,
determining equivalence of 3-constellations, or counting the number
of 3-constellations with a given degree or genus.

.. |Belyi| unicode:: Bely U+012D .. Belyi

.. mermaid::

    flowchart TD;
        A[Passport] -->|Double Coset\nGeneration| B[3-Constellation];
        B -->|Cycle Type| A;
        A -->|Solve Multivariate\nSystem| C["Bely&#x12D Map\n(Genus 0)"];
        C -->|Lifting of Loops| B;
        B --> D[Bipartite Graph];
        D --> B;

------------
Installation
------------

Using :code:`dessins` requires SageMath (see https://doc.sagemath.org/html/en/installation/ for installation). Afterwards, :code:`dessins` can be installed by downloading this repository and running

.. code:: bash

    sage -pip install -e .

from within the repository.

Alternatively, :code:`dessins` can be installed directly from GitLab with

.. code:: bash

    sage -pip install git+https://gitlab.com/dessins/dessins.git@develop

or to ensure the latest changes are included, updated with `--force-reinstall`

.. code:: bash

    sage -pip install --force-reinstall --no-deps git+https://gitlab.com/dessins/dessins.git@develop

The installation can be verified with (current version is `0.0.2`)

.. code:: bash
    
    sage -c "import dessins; print(dessins.__version__)"

-----
Usage
-----

.. code:: python

    sage: from dessins import *

Passports
---------

.. code:: python

    sage: p = Passport([ [3,1], [2,2], [3,1] ])
    sage: list(p.constellations())
    [[(2,3,4), (1,2)(3,4), (1,2,3)]]

3-Constellations
----------------
The :class:`Constellation3<.constellations.constellation.Constellation3>` class for 3-constellations extends Sage's built-in :class:`Constellation` class, including functionality specific to dessins d'enfants.

.. code:: python

    sage: c = Constellation3("(1,2,3)", "(1,2)(3,4)")
    sage: isomorph = c.isomorphism( next(p.constellations()) ); isomorph
    (1,3,2,4)
    sage: c ** isomorph == next(p.constellations())
    True

Drawings
--------

.. code:: python

    sage: dr = c.drawing(); dr
    {0: [(2, 1), (2, 2), (3, 3)], 1: [(3, 4)], 2: [(0, 1), (0, 2)], 3: [(0, 3), (1, 4)]}
    sage: dr.constellation()
    ((1,2,3), (1,2)(3,4), (1,4,3))
    sage: dr.plot()

.. mermaid::

    graph LR;
        id0((0)) --- id2((2));
        id0 --- id2;
        id0 --- id3((3));
        id1((1)) --- id3;
        classDef over0 fill:red,stroke-width:2px;
        classDef over1 fill:blue,stroke-width:2px;
        class id0,id1 over0;
        class id2,id3 over1;

|Belyi| Maps
------------
.. code:: python

    sage: b = BelyiMap((x+1)^3*(x+9)/(64*x))
    sage: c = b.constellation(); c
    ((2,3,4), (1,2)(3,4), (1,2,3))
    sage: c.belyi_map(d=3, h=100)
    The polynomial identity coming from the ramification behavior has the form
    (a0 + x)^3*(a1 + x) - (c0 + x)*k == (b1*x + x^2 + b0)^2
    Which variable should be substituted for first? c0
    What value should be substituted for c0? 0
    Which variable should be substituted for second? a0
    What value should be substituted for a0? 1

    Found 1 potential Belyi maps (some may be repeated or the result of parasitic solutions).

    Finding the coefficients of the Belyi map having approximate coefficients
    [(a1, 9.00000), (b0, -3.00000), (b1, 6.00000), (k, 64.0000)]

    Finding an algebraic relation for 9.00000000,
    -3.00000000, 6.00000000, 64.0000000

    Found 1 Galois orbits.
    Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/
    x on Projective Space of dimension 1 over Rational Field

Monodromy of Compositions
-------------------------
.. code:: python

    sage: p = Passport([4,1], [4,1], [2,2,1])
    sage: beta = p.belyi_maps(d=5, h=500, subs_dict={var("a0"): 0, var("a1"): -1})[0]
    ...
    Found 1 Galois orbits.
    sage: gamma = BelyiMap(x**2 / (x**2 - 1), "y**2 = x**3 - x")
    sage: c1 = beta(gamma).constellation().canonical(); c1
    ((1,2,4,8,14,19,20,15,9,11,5,10,16,18,13,3)(6,7,12,17), (1,3,6,5)(4,9,15,12)(7,13,14,8)(10,17,20,16), (2,5,11,4)(3,7)(6,10)(8,12)(13,18,20,19)(15,17))
    sage: c2 = compose_dessins(
    ....:     beta.constellation(),
    ....:     beta.extending_pattern(),
    ....:     gamma.constellation(),
    ....: ).canonical(); c2
    ((1,2,4,8,14,19,20,15,9,11,5,10,16,18,13,3)(6,7,12,17), (1,3,6,5)(4,9,15,12)(7,13,14,8)(10,17,20,16), (2,5,11,4)(3,7)(6,10)(8,12)(13,18,20,19)(15,17))
    sage: c1 == c2
    True
    sage: c3 = Constellation3(
    ....:     *beta.extended_monodromy().apply_to_extended_monodromy(
    ....:         gamma.monodromy_representation()
    ....:     ).GeneratorsOfGroup()
    ....: ).canonical()
    sage: c1 == c3
    True

Other Examples
--------------
Determining whether two |Belyi| maps correspond to the same dessin by finding canonical representatives of their 3-constellations:

.. code:: python

    sage: b1 = BelyiMap( (x+9)*(x+1)^3/(64*x) )
    sage: b2 = BelyiMap( 256*x/(256*x^4 - 768*x^3 + 480*x^2 + 144*x + 9) )
    sage: c1 = b1.constellation(); c2 = b2.constellation()
    sage: equivalence = c1.flexible_equivalence(c2)
    sage: equivalence(c1) == c2
    True

Counting the number of occurences of monodromy groups among dessins with a given degree and genus, in this case degree 6 and genus 1:

.. code:: python

    sage: from collection import Counter
    sage: Counter(
    ....:     c.monodromy_group().StructureDescription()
    ....:         for p in passports_of_degree_and_genus(6, 1)
    ....:             for c in Passport(p).constellations()
    ....: )
    Counter({"A6": 25, "S6": 24, "S5": 6, "A5": 4, "C2 x S4": 2,
    "(S3 x S3) : C2": 2, "S3 x S3": 1, "C2 x A4": 1, "C6": 1,
    "C3 x S3": 1, "A4": 1, "S4": 1, "(C3 x C3) : C4": 1})

-------
Contact
-------

For questions, issues, or comments, contact information can be found `here <https://gitlab.com/jacobbond>`_.

-------
Dessins 
-------

.. toctree::
   :maxdepth: 1

   belyi_maps/index.rst
   constellations/index.rst
   counting
   dessin
   drawing
   passport
   utils

------------------
Indices and Tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
