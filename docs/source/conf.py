# sample documentation build configuration file,
# (from https://github.com/sagemath/sage_sample/blob/master/docs/source/conf.py)
# inspired by slabbe configuration file created by sphinx-quickstart
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

# General information about the project.
project = "Dessins d'Enfants"
copyright = "2023, Jacob Bond"
package_name = "dessins"
package_folder = "../../"
authors = "Jacob Bond"

import os
import sys

# import six

# from sage.env import SAGE_DOC_SRC, SAGE_DOC, SAGE_SRC

try:
    import sage.all  # noqa: F401
except ImportError:
    raise RuntimeError(
        "To build the documentation, you must be inside a Sage shell (first, run the command 'sage -sh' in a shell)"
    )

import importlib

from IPython.lib.lexers import IPyLexer, IPythonConsoleLexer
from sage.env import MATHJAX_DIR, SAGE_DOC_SRC
from sage.features import PythonModule
from sage.misc.latex_macros import sage_mathjax_macros

# Load configuration shared with sage.misc.sphinxify
# from sage.misc.sagedoc_conf import *
from sphinx import highlighting

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.insert(0, os.path.abspath(package_folder))
# sys.path.append(os.path.join(SAGE_SRC, "sage_setup", "docbuild", "ext"))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    # "sage_autodoc",            ## Not available on conda-forge sage!
    # "sage_package.sphinx",
    "sphinx.ext.doctest",
    "sphinx.ext.coverage",
    "sphinx.ext.extlinks",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.linkcode",
    "sphinx_autodoc_typehints",
    "sphinx_copybutton",
    "matplotlib.sphinxext.plot_directive",
    # "sphinxcontrib.bibtex",
    "sphinxcontrib.mermaid",
]

### from Sage src/doc/common/conf.py
# This code is executed before each ".. PLOT::" directive in the Sphinx
# documentation. It defines a 'sphinx_plot' function that displays a Sage object
# through matplotlib, so that it will be displayed in the HTML doc.
plot_html_show_source_link = False
plot_pre_code = """
def sphinx_plot(graphics, **kwds):
    import matplotlib.image as mpimg
    from sage.misc.temporary_file import tmp_filename
    import matplotlib.pyplot as plt
    ## Option handling is taken from Graphics.save
    try:
        from sage.plot.multigraphics import GraphicsArray
    except ImportError:
        from sage.plot.graphics import GraphicsArray
    options = dict()
    if not isinstance(graphics, GraphicsArray):
        options.update(graphics.SHOW_OPTIONS)
        options.update(graphics._extra_kwds)
    options.update(kwds)
    dpi = options.pop('dpi', None)
    transparent = options.pop('transparent', None)
    fig_tight = options.pop('fig_tight', None)
    figsize = options.pop('figsize', None)
    ## figsize handling is taken from Graphics.matplotlib()
    if figsize is not None and not isinstance(figsize, (list, tuple)):
        # in this case, figsize is a number and should be positive
        try:
            figsize = float(figsize) # to pass to mpl
        except TypeError:
            raise TypeError("figsize should be a positive number, not {0}".format(figsize))
        if figsize > 0:
            default_width, default_height=rcParams['figure.figsize']
            figsize=(figsize, default_height*figsize/default_width)
        else:
            raise ValueError("figsize should be positive, not {0}".format(figsize))

    if figsize is not None:
        # then the figsize should be two positive numbers
        if len(figsize) != 2:
            raise ValueError("figsize should be a positive number "
                             "or a list of two positive numbers, not {0}".format(figsize))
        figsize = (float(figsize[0]),float(figsize[1])) # floats for mpl
        if not (figsize[0] > 0 and figsize[1] > 0):
            raise ValueError("figsize should be positive numbers, "
                             "not {0} and {1}".format(figsize[0],figsize[1]))

    plt.figure(figsize=figsize)
    if isinstance(graphics, GraphicsArray):
        ## from GraphicsArray.save
        figure = plt.gcf()
        rows = graphics.nrows()
        cols = graphics.ncols()
        for i, g in enumerate(graphics):
            subplot = figure.add_subplot(rows, cols, i + 1)
            g_options = copy(options)
            g_options.update(g.SHOW_OPTIONS)
            g_options.update(g._extra_kwds)
            g_options.pop('dpi', None)
            g_options.pop('transparent', None)
            g_options.pop('fig_tight', None)
            g.matplotlib(figure=figure, sub=subplot, **g_options)
    else:
        figure = graphics.matplotlib(figure=plt.gcf(), figsize=figsize, **options)
    plt.tight_layout(pad=0)
    plt.margins(0)
    plt.show()

from sage.all_cmdline import *
"""

plot_html_show_formats = False
plot_formats = ["svg", "pdf", "png"]

# Add any paths that contain templates here, relative to this directory.
# templates_path = ['_templates']
templates_path = [os.path.join(SAGE_DOC_SRC, "common", "templates"), "_templates"]

# The suffix of source filenames.
source_suffix = ".rst"

# The encoding of source files.
# source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = "index"

source_repository = "https://gitlab.com/dessins/dessins/"
source_branch = "develop"

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
# language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
# today = ''
# Else, today_fmt is used as the format for a strftime call.
# today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = []

# The reST default role (used for this markup: `text`) to use for all
# documents.
default_role = "code"

# If true, '()' will be appended to :func: etc. cross-reference text.
# add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
# add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
# show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
# pygments_style = "sphinx"

# Default lexer to use when highlighting code blocks, using the IPython
# console lexers. 'ipycon' is the IPython console, which is what we want
# for most code blocks: anything with "sage:" prompts. For other IPython,
# like blocks which might appear in a notebook cell, use 'ipython'.
highlighting.lexers["ipycon"] = IPythonConsoleLexer(
    in1_regex=r"sage: ", in2_regex=r"[.][.][.][.]: "
)
highlighting.lexers["ipython"] = IPyLexer()
highlight_language = "ipycon"

# A list of ignored prefixes for module index sorting.
# modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
# keep_warnings = False

# https://sphinx-copybutton.readthedocs.io/en/latest/use.html
copybutton_prompt_text = r"sage: |[.][.][.][.]: |\$ "
copybutton_prompt_is_regexp = True
copybutton_exclude = (
    ".linenos, .c1"  # exclude single comments (in particular, # optional!)
)
copybutton_only_copy_prompt_lines = True


# https://www.sphinx-doc.org/en/master/usage/extensions/linkcode.html
def linkcode_resolve(domain, info):
    from urllib.parse import quote

    from sage.misc.sageinspect import sage_getsourcelines

    if domain != "py":
        return None
    if info["module"]:
        m = importlib.import_module(info["module"])
        filename = quote(info["module"].replace(".", "/"))
        if m.__file__.endswith("py"):
            filename += ".py"
        else:
            filename += ".pyx"
        if "fullname" in info:
            fullname = info["fullname"]
            obj = m
            try:
                for attr in fullname.split("."):
                    obj = getattr(obj, attr)
                lineno = sage_getsourcelines(obj)[-1]
            except Exception:  # catch all
                return None
            anchor = f"#L{lineno}"
        else:
            anchor = ""
        return f"{source_repository}blob/develop/{filename}{anchor}"
    return None


# Options for HTML output
# -----------------------

# Add any paths that contain custom themes here, relative to this directory.
html_theme_path = ["../themes"]

# html_static_path defined here and imported in the actual configuration file
# conf.py read by Sphinx was the cause of subtle bugs in builders (see #30418 for
# instance). Hence now html_common_static_path contains the common paths to static
# files, and is combined to html_static_path in each conf.py file read by Sphinx.
# html_common_static_path = ["../static"]
html_static_path = ["../static"]

if PythonModule("furo").is_present():
    # Sphinx theme "furo" does not permit an extension. Do not attempt to make
    # a "sage-furo" theme.
    html_theme = "furo"

    # Theme options are theme-specific and customize the look and feel of
    # a theme further.  For a list of options available for each theme,
    # see the documentation.
    html_theme_options = {
        "light_css_variables": {
            "color-brand-primary": "#0f0fff",
            "color-brand-content": "#0f0fff",
        },
        "light_logo": "logo_dessins_black.svg",
        "dark_logo": "logo_dessins_white.svg",
    }

    # The name of the Pygments (syntax highlighting) style to use. This
    # overrides a HTML theme's corresponding setting.
    pygments_style = "sphinx"
    pygments_dark_style = "monokai"

    # Add siderbar/home.html to the default sidebar.
    html_sidebars = {
        "**": [
            "sidebar/scroll-start.html",
            "sidebar/brand.html",
            "sidebar/search.html",
            "sidebar/home.html",
            "sidebar/navigation.html",
            "sidebar/ethical-ads.html",
            "sidebar/scroll-end.html",
            "sidebar/variant-selector.html",
        ]
    }

    # These paths are either relative to html_static_path
    # or fully qualified paths (eg. https://...)
    html_css_files = [
        "custom-furo.css",
        #        "custom-jupyter-sphinx.css",
        "custom-codemirror-monokai.css",
    ]

    # html_js_files = [
    #     "jupyter-sphinx-furo.js",
    # ]

    # A list of paths that contain extra templates (or templates that overwrite
    # builtin/theme-specific templates). Relative paths are taken as relative
    # to the configuration directory.
    templates_path = ["../templates-furo"] + templates_path

else:
    # Sage default Sphinx theme.
    #
    # See the directory doc/common/themes/sage-classic/ for files comprising
    # the custom theme.
    html_theme = "sage-classic"

    html_theme_options = {}

# HTML style sheet. This overrides a HTML theme's corresponding setting.
# html_style = 'default.css'

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
# html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
# html_short_title = None

# The name of an image file (within the static path) to place at the top of
# the sidebar.
# html_logo = 'sagelogo-word.ico'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = "../static/favicon.ico"

# Configure MathJax
# https://docs.mathjax.org/en/latest/options/input/tex.html
mathjax3_config = {
    "tex": {
        # Add custom sage macros
        # http://docs.mathjax.org/en/latest/input/tex/macros.html
        "macros": sage_mathjax_macros(),
        # Add $...$ as possible inline math
        # https://docs.mathjax.org/en/latest/input/tex/delimiters.html#tex-and-latex-math-delimiters
        "inlineMath": [["$", "$"], ["\\(", "\\)"]],
        # Increase the limit the size of the string to be processed
        # https://docs.mathjax.org/en/latest/options/input/tex.html#option-descriptions
        "maxBuffer": 50 * 1024,
        # Use colorv2 extension instead of built-in color extension
        # https://docs.mathjax.org/en/latest/input/tex/extensions/autoload.html#tex-autoload-options
        # https://docs.mathjax.org/en/latest/input/tex/extensions/colorv2.html#tex-colorv2
        "autoload": {"color": [], "colorv2": ["color"]},
    },
}

# TODO: can local MathJax be used instead of a CDN?
if True:  # os.environ.get("SAGE_USE_CDNS", "no") == "yes":
    mathjax_path = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"
else:
    mathjax_path = os.path.join(MATHJAX_DIR, "tex-chtml.js")

# A list of glob-style patterns that should be excluded when looking for source
# files. They are matched against the source file names relative to the
# source directory, using slashes as directory separators on all platforms.
exclude_patterns = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
# html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
# html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
# html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
# html_additional_pages = {}

# If false, no module index is generated.
# html_use_modindex = True

# A list of prefixes that are ignored for sorting the Python module index ( if
# this is set to ['foo.'], then foo.bar is shown under B, not F). Works only
# for the HTML builder currently.
modindex_common_prefix = ["sage."]

# If false, no index is generated.
# html_use_index = True

# If true, the index is split into individual pages for each letter.
html_split_index = True

# If true, the reST sources are included in the HTML build as _sources/<name>.
# html_copy_source = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
# html_use_opensearch = ''

# If nonempty, this is the file name suffix for HTML files (e.g. ".xhtml").
# html_file_suffix = ''

# Output file base name for HTML help builder.

# # -- Options for LaTeX output ---------------------------------------------

# latex_elements = {
#     # # The paper size ('letterpaper' or 'a4paper').
#     # 'papersize': 'letterpaper',
#     # # The font size ('10pt', '11pt' or '12pt').
#     # 'pointsize': '10pt',
#     # # Additional stuff for the LaTeX preamble.
#     "preamble": "",
# }

# # Grouping the document tree into LaTeX files. List of tuples
# # (source start file, target name, title,
# #  author, documentclass [howto, manual, or own class]).
# latex_documents = [
#     (
#         "index",
#         package_name + ".tex",
#         "Documentation of " + six.text_type(package_name),
#         authors,
#         "manual",
#     ),
# ]

# # The name of an image file (relative to this directory) to place at the top of
# # the title page.
# # latex_logo = None

# # For "manual" documents, if this is true, then toplevel headings are parts,
# # not chapters.
# # latex_use_parts = False

# # If true, show page references after internal links.
# # latex_show_pagerefs = False

# # If true, show URL addresses after external links.
# # latex_show_urls = False

# # Documents to append as an appendix to all manuals.
# # latex_appendices = []

# # If false, no module index is generated.
# # latex_domain_indices = True


# # -- Options for manual page output ---------------------------------------

# # One entry per manual page. List of tuples
# # (source start file, name, description, authors, manual section).
# man_pages = [
#     (
#         "index",
#         package_name,
#         six.text_type(package_name) + " documentation",
#         [authors],
#         1,
#     )
# ]

# # If true, show URL addresses after external links.
# # man_show_urls = False


# # -- Options for Texinfo output -------------------------------------------

# # Grouping the document tree into Texinfo files. List of tuples
# # (source start file, target name, title, author,
# #  dir menu entry, description, category)
# texinfo_documents = [
#     (
#         "index",
#         package_name,
#         six.text_type(package_name) + " documentation",
#         authors,
#         package_name,
#         project,
#         "Miscellaneous",
#     ),
# ]

# # Documents to append as an appendix to all manuals.
# # texinfo_appendices = []

# # If false, no module index is generated.
# # texinfo_domain_indices = True

# # How to display URL addresses: 'footnote', 'no', or 'inline'.
# # texinfo_show_urls = 'footnote'

# # If true, do not generate a @detailmenu in the "Top" node's menu.
# # texinfo_no_detailmenu = False

# # -- Options copied from Sagemath conf.py file -------------------------------

# # We use MathJax to build the documentation unless the environment
# # variable SAGE_DOC_MATHJAX is set to "no" or "False".  (Note that if
# # the user does not set this variable, then the script sage-env sets
# # it to "True".)

# if (
#     os.environ.get("SAGE_DOC_MATHJAX", "no") != "no"
#     and os.environ.get("SAGE_DOC_MATHJAX", "no") != "False"
# ):
#     extensions.append("sphinx.ext.mathjax")
#     mathjax_path = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js"

#     from sage.misc.latex_macros import sage_mathjax_macros

#     # this is broken for now
#     # html_theme_options['mathjax_macros'] = sage_mathjax_macros()
#     ## from pkg_resources import Requirement, working_set
#     ## sagenb_path = working_set.find(Requirement.parse('sagenb')).location
#     ## mathjax_relative = os.path.join('sagenb','data','mathjax')
#     ## # It would be really nice if sphinx would copy the entire mathjax directory,
#     ## # (so we could have a _static/mathjax directory), rather than the contents of the directory
#     ## mathjax_static = os.path.join(sagenb_path, mathjax_relative)
#     ## html_static_path.append(mathjax_static)
#     ## exclude_patterns=['**/'+os.path.join(mathjax_relative, i) for i in ('docs', 'README*', 'test',
#     ##                                                                     'unpacked', 'LICENSE')]
#     # from sage.env import SAGE_LOCAL, SAGE_SHARE
#     # html_static_path.append(SAGE_LOCAL + "/lib/mathjax")  # conda
#     # html_static_path.append(SAGE_SHARE + "/mathjax")  # sage distribution
# else:
#     extensions.append("sphinx.ext.imgmath")

# import pathlib

# from pygments.formatters.latex import LatexFormatter

# # This is to make the verbatim font smaller;
# # Verbatim environment is not breaking long lines
# from sphinx.highlighting import PygmentsBridge

# # print(f"HTML static path: {pathlib.Path(html_static_path[0]).resolve()}")
# # print(f"Current directory: {pathlib.Path('.').resolve()}")


# class CustomLatexFormatter(LatexFormatter):
#     def __init__(self, **options):
#         super(CustomLatexFormatter, self).__init__(**options)
#         self.verboptions = r"formatcom=\footnotesize"


# PygmentsBridge.latex_formatter = CustomLatexFormatter

# latex_elements[
#     "preamble"
# ] += r"""
# % One-column index
# \makeatletter
# \renewenvironment{theindex}{
#   \chapter*{\indexname}
#   \markboth{\MakeUppercase\indexname}{\MakeUppercase\indexname}
#   \setlength{\parskip}{0.1em}
#   \relax
#   \let\item\@idxitem
# }{}
# \makeatother
# \renewcommand{\ttdefault}{txtt}
# """

# #####################################################
# # add LaTeX macros for Sage

# from sage.misc.latex_macros import sage_latex_macros

# try:
#     pngmath_latex_preamble  # check whether this is already defined
# except NameError:
#     pngmath_latex_preamble = ""

# for macro in sage_latex_macros():
#     # used when building latex and pdf versions
#     latex_elements["preamble"] += macro + "\n"
#     # used when building html version
#     pngmath_latex_preamble += macro + "\n"


# import sage.all

# ## The following is needed on conda-forge sagemath
# from sage.repl.user_globals import initialize_globals

# my_globs = dict()
# initialize_globals(sage.all, my_globs)
