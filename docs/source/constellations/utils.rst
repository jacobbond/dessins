.. nodoctest

Utilities
=========

.. automodule:: dessins.constellations.utils
   :members:
   :undoc-members:
   :show-inheritance: