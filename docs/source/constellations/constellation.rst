.. nodoctest

Constellation
=============

.. automodule:: dessins.constellations.constellation
   :members:
   :undoc-members:
   :show-inheritance:
