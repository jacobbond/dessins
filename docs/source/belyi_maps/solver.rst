.. nodoctest

Solver
======

.. automodule:: dessins.belyi_maps.solver
   :members:
   :undoc-members:
   :show-inheritance:
