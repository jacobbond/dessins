.. nodoctest

Monodromy
=========

.. automodule:: dessins.belyi_maps.monodromy
   :members:
   :undoc-members:
   :show-inheritance:
