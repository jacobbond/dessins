.. nodoctest

Extending Pattern
=================

.. automodule:: dessins.belyi_maps.extending_pattern
   :members:
   :undoc-members:
   :show-inheritance:
