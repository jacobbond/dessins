.. nodoctest

Plotting Belyi Maps
===================

.. automodule:: dessins.belyi_maps.plot
   :members:
   :undoc-members:
   :show-inheritance:
