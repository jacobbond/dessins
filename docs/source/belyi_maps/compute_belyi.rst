.. nodoctest

Computing Belyi Maps
====================

.. automodule:: dessins.belyi_maps.compute_belyi
   :members:
   :undoc-members:
   :show-inheritance:
