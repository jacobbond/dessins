.. nodoctest

Algebraic Relation
==================

.. automodule:: dessins.belyi_maps.algebraic_relation
   :members:
   :undoc-members:
   :show-inheritance:
