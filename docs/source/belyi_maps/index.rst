==========
Belyi Maps
==========

----------
Belyi Maps
----------

.. toctree::
   :maxdepth: 1

   belyi_map
   compute_belyi
   algebraic_relation
   solver
   lifting
   monodromy
   extending_pattern
   plot

------------------
Indices and Tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`