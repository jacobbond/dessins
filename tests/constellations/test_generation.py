import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from dessins import passport_to_constellations


class TestPassportToConstellations:
    t1 = [6]
    t2 = [2, 2, 1, 1]
    t3 = [3, 2, 1]

    def test_invariant_to_permutation_of_inputs(self):
        c1 = list(passport_to_constellations(self.t1, self.t2, self.t3))
        c2 = list(passport_to_constellations(self.t1, self.t3, self.t2))
        c3 = list(passport_to_constellations(self.t2, self.t1, self.t3))
        c4 = list(passport_to_constellations(self.t2, self.t3, self.t1))
        c5 = list(passport_to_constellations(self.t3, self.t1, self.t2))
        # c6 = list(
        #     passport_to_constellations(
        #         self.t3, self.t2, self.t1
        #     )
        # )

        assert sorted([c.braid_action(1).canonical() for c in c2]) == sorted(
            [c.canonical() for c in c1]
        )
        assert sorted([c.braid_action(0).canonical() for c in c3]) == sorted(
            [c.canonical() for c in c1]
        )
        assert sorted(
            [c.braid_action(1).braid_action(0).canonical() for c in c4]
        ) == sorted([c.canonical() for c in c1])
        assert sorted(
            [c.braid_action(0).braid_action(1).canonical() for c in c5]
        ) == sorted([c.canonical() for c in c1])
        # assert sorted([c.braid_action(2).canonical() for c in c6]) == sorted(
        #     [c.canonical() for c in c1]
        # )

    def test_invariant_to_permutation_of_inputs_conjugacy(self):
        c1 = list(
            passport_to_constellations(
                self.t1, self.t2, self.t3, algorithm="conjugates"
            )
        )
        c2 = list(
            passport_to_constellations(
                self.t1, self.t3, self.t2, algorithm="conjugates"
            )
        )
        c3 = list(
            passport_to_constellations(
                self.t2, self.t1, self.t3, algorithm="conjugates"
            )
        )
        c4 = list(
            passport_to_constellations(
                self.t2, self.t3, self.t1, algorithm="conjugates"
            )
        )
        c5 = list(
            passport_to_constellations(
                self.t3, self.t1, self.t2, algorithm="conjugates"
            )
        )
        # c6 = list(
        #     passport_to_constellations(
        #         self.t3, self.t2, self.t1, algorithm="conjugates"
        #     )
        # )

        assert sorted([c.braid_action(1).canonical() for c in c2]) == sorted(
            [c.canonical() for c in c1]
        )
        assert sorted([c.braid_action(0).canonical() for c in c3]) == sorted(
            [c.canonical() for c in c1]
        )
        assert sorted(
            [c.braid_action(1).braid_action(0).canonical() for c in c4]
        ) == sorted([c.canonical() for c in c1])
        assert sorted(
            [c.braid_action(0).braid_action(1).canonical() for c in c5]
        ) == sorted([c.canonical() for c in c1])
        # assert sorted([c.braid_action(2).canonical() for c in c6]) == sorted(
        #     [c.canonical() for c in c1]
        # )
