import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.libs.gap.libgap import libgap

from dessins import is_simultaneous_conjugates


class TestSimultaneousConjugates:
    def test_is_simultaneous_conjugates(self):
        assert is_simultaneous_conjugates(
            [libgap.eval("()")] * 2,
            [libgap.eval("()")] * 2,
        )
