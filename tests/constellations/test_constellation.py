import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.groups.perm_gps.constructor import PermutationGroupElement
from sage.libs.gap.libgap import libgap

from dessins import Constellation3, drawing


class TestConstellation:
    c = Constellation3(
        PermutationGroupElement("(1,2,3)"),
        PermutationGroupElement("(1,2)(3,4)"),
    )

    def test_constellation_constructor(self):
        assert self.c._sigmas[0] == libgap.eval("(1,2,3)")
        assert self.c._sigmas[1] == libgap.eval("(1,2)(3,4)")
        assert self.c._sigmas[2] == libgap.eval("(1,4,3)")

    def test_constellation_to_drawing(self):
        dr = drawing.constellation_to_drawing(self.c[0], self.c[1])
        assert dr == {
            0: [(2, 1), (2, 2), (3, 3)],
            1: [(3, 4)],
            2: [(0, 1), (0, 2)],
            3: [(0, 3), (1, 4)],
        }
