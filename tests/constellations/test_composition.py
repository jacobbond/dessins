import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

# from sage.libs.gap.libgap import libgap
from sage.all import libgap
from sage.calculus.var import var

from dessins import BelyiMap, Constellation3
from dessins.belyi_maps.extending_pattern import extending_pattern
from dessins.belyi_maps.monodromy import F2, ID, A, B, monodromy


class TestExtendedMonodromy:
    x = var("x")

    beta = BelyiMap(x**3)

    f1 = BelyiMap(-27 * (x**3 - x**2) / 4)
    f4 = BelyiMap(27 * x**2 * (x - 1) / ((3 * x - 1) ** 3))
    alpha = BelyiMap(x**2)

    delta = Constellation3("(1,2)", "()")

    # fix an edge labelling for beta:
    for pt, label in beta.edge_labelling().items():
        if pt[0].imag() > 0:
            beta.edge_labelling()[pt] = 1
        elif pt[0].imag() < 0:
            beta.edge_labelling()[pt] = 2
        else:
            beta.edge_labelling()[pt] = 0

    beta_monodromy = beta.constellation()
    gamma = BelyiMap(3 * x**2 - 2 * x**3)

    def test_apply_monodromy(self):
        beta_pattern = self.beta.extending_pattern()
        monodromy_representation = self.beta_monodromy.monodromy_representation()
        after_application = beta_pattern.apply_monodromy_representation(
            monodromy_representation
        )
        monodromy_id = self.beta_monodromy.monodromy_group().One()
        for pt, label in self.beta.edge_labelling().items():
            if pt[0].imag() > 0:
                assert (
                    after_application[0][label]
                    == self.beta_monodromy.constellation()[0]
                )
                assert after_application[1][label] == monodromy_id
            elif pt[0].imag() < 0:
                assert after_application[0][label] == monodromy_id
                assert after_application[1][label] == monodromy_id
            else:
                assert after_application[0][label] == monodromy_id
                assert (
                    after_application[1][label]
                    == self.beta_monodromy.constellation()[1]
                )

    def test_extended_monodromy(self):
        beta_monodromy_gp = self.beta.constellation().monodromy_group()
        wr = libgap.WreathProduct(F2, beta_monodromy_gp)
        emon_beta = self.beta.extended_monodromy()
        assert wr.ListWreathProductElement(
            emon_beta.extended_monodromy_group.GeneratorsOfGroup()[0]
        ) == [ID, A, ID, libgap.eval("(1,2,3)")]
        assert wr.ListWreathProductElement(
            emon_beta.extended_monodromy_group.GeneratorsOfGroup()[1]
        ) == [B, ID, ID, libgap.eval("()")]

    def test_apply_to_wreath(self):
        emon_beta = self.beta.extended_monodromy()
        mon_beta_gamma = emon_beta.apply_to_extended_monodromy(
            self.gamma.constellation().monodromy_representation()
        )
        assert mon_beta_gamma.IdGroup().sage() == [648, 705]
        mon_ext_composed = emon_beta.apply_to_monodromy_extending(
            self.gamma.constellation().monodromy_representation()
        )
        assert mon_ext_composed.IdGroup().sage() == [216, 162]

    def test_monodromy_composition(self):
        beta_pattern = extending_pattern(self.beta)
        beta_monodromy = monodromy(self.beta)
        gamma_monodromy = monodromy(self.gamma)

        from dessins.constellations.composition import compose_dessins

        compose_dessins(
            beta_monodromy,
            beta_pattern,
            gamma_monodromy,
        )

        composed = compose_dessins(
            self.f1.constellation(),
            self.f1.extending_pattern(),
            self.alpha.constellation(),
        )

        # https://beta.lmfdb.org/Belyi/6T11/6/2.2.1.1/4.1.1/a/
        assert Constellation3(*composed).is_flexibly_equivalent(
            Constellation3("(1,5,3,4,2,6)", "(1,5)(2,4)")
        )

        # since delta is the dessin of x**2 and
        # f4(x**2) = 27*x**4*(x+1)*(x-1)/(27*(x+1/sqrt(3))**3*(x-1/sqrt(3))**3)
        # f4(x**2) - 1 = -9*(x-1/3)*(x+1/3)/(27*(x+1/sqrt(3))**3*(x-1/sqrt(3))**3)
        # f4(x**2) has passport [[4, 1, 1], [4, 1, 1], [3, 3] and must be
        # https://beta.lmfdb.org/Belyi/6T8/4.1.1/4.1.1/3.3/a/
        composed = compose_dessins(
            self.f4.constellation(),
            self.f4.extending_pattern(),
            self.delta.constellation(),
        )
        assert Constellation3(*composed).is_isomorphic(
            Constellation3("(1,5,4,2)", "(1,3,4,6)")
        )
        assert self.delta == self.alpha.constellation()
        assert (
            BelyiMap(self.f4.function()(x=self.alpha.function()))
            .constellation()
            .canonical()
            == Constellation3(*composed).canonical()
        )
