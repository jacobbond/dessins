import itertools as it
import sys
from collections import Counter
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

# from sage.groups.perm_gps.permgroup_named import SymmetricGroup
from sage.all import SymmetricGroup

from dessins import counting


class TestCounting:
    def test_passports_of_degree(self):
        assert list(counting.passports_of_degree(1)) == [([1], [1], [1])]
        assert list(counting.passports_of_degree(3)) == [
            ([1, 1, 1], [3], [3]),
            ([1, 2], [1, 2], [3]),
            ([3], [3], [3]),
        ]

    def test_passports_of_degree_and_genus(self):
        assert list(counting.passports_of_degree_and_genus(1, 0)) == [([1], [1], [1])]
        assert list(counting.passports_of_degree_and_genus(6, 2)) == [
            ([3, 3], [6], [6]),
            ([2, 4], [6], [6]),
            ([1, 5], [6], [6]),
        ]

    def test_count_constellations_of_degree(self):
        assert counting.count_constellations_of_degree(1) == Counter({0: 1})
        assert counting.count_constellations_of_degree(2) == Counter({0: 1})
        assert counting.count_constellations_of_degree(3) == Counter({0: 2, 1: 1})
        assert counting.count_constellations_of_degree(4) == Counter({0: 6, 1: 2})
        assert counting.count_constellations_of_degree(5) == Counter(
            {0: 14, 1: 9, 2: 4}
        )

    def test_count_constellations_of_degree_and_genus(self):
        assert counting.count_constellations_of_degree_and_genus(1, 0) == 1
        assert counting.count_constellations_of_degree_and_genus(6, 2) == 16


class TestClassMultiplicationCoefficient:
    def test_class_mult_coeff(self):
        for pair in it.product(
            SymmetricGroup(6).conjugacy_classes_iterator(), repeat=2
        ):
            for conj_class in SymmetricGroup(6).conjugacy_classes_iterator():
                rep = conj_class.representative()
                class_mult_coeff = counting.class_multiplication_coefficient(
                    pair[0].representative(),
                    pair[1].representative(),
                    rep,
                    sym_size=6,
                )
                count = sum(1 for elt1, elt2 in it.product(*pair) if elt1 * elt2 == rep)
                assert class_mult_coeff == count

    def test_class_mult_coeff_sn(self):
        for pair in it.product(
            SymmetricGroup(6).conjugacy_classes_iterator(), repeat=2
        ):
            for conj_class in SymmetricGroup(6).conjugacy_classes_iterator():
                rep = conj_class.representative()
                class_mult_coeff = counting.class_mult_coeff_sn(
                    pair[0].representative().cycle_type(),
                    pair[1].representative().cycle_type(),
                    conj_class.representative().cycle_type(),
                )
                count = sum(1 for elt1, elt2 in it.product(*pair) if elt1 * elt2 == rep)
                assert class_mult_coeff == count


class TestNNNCount:
    # fmt: off
    # from https://oeis.org/A058026
    phi_values = (
            1, 0, 1, 0, 3, 0, 5, 0, 3, 0, 9, 0, 11, 0, 3, 0, 15, 0, 17,
            0, 5, 0, 21, 0, 15, 0, 9, 0, 27, 0, 29, 0, 9, 0, 15, 0, 35,
            0, 11, 0, 39, 0, 41, 0, 9, 0, 45, 0, 35, 0
        )
    nnn_values = (
        1, 0, 1, 0, 4, 0, 30, 0, 900, 0, 54990, 0, 5263764, 0, 726485868,
        0, 136750260720, 0, 33696703714374, 0, 10532043325452570
    )
    # fmt: on

    def test_schemmel_phi(self):
        for idx, val in enumerate(self.phi_values, start=1):
            assert counting.schemmel_phi(idx) == val

    def test_nnn_count(self):
        for idx, val in enumerate(self.nnn_values, start=1):
            assert counting.num_dessins_type_nnn(idx) == val
