import sys
from pathlib import Path

import pytest

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var
from sage.rings.infinity import Infinity

from dessins import Constellation3, Passport


class TestPassport:
    p = Passport([[3, 1], [2, 2], [3, 1]])

    def test_passport_constructor(self):
        assert self.p._pass == [[1, 3], [2, 2], [1, 3]]
        p2 = Passport([3, 1], [2, 2], [3, 1])
        assert p2._pass == [[1, 3], [2, 2], [1, 3]]

        # input not well formed:
        with pytest.raises(ValueError):
            Passport([[[1, 3], [2, 2], [1, 3], [2, 2]]])
        with pytest.raises(ValueError):
            Passport([[1, 3]])
        with pytest.raises(ValueError):
            Passport([1, 3])
        with pytest.raises(ValueError):
            Passport([[3, 1]], [2, 2], [3, 1])

        # negative cycle lengths or non-uniform degree:
        with pytest.raises(ValueError):
            Passport([-1], [-1], [-1])
        with pytest.raises(ValueError):
            Passport([1], [2], [3])
        with pytest.raises(ValueError):
            Passport([3, 1], [2, 2], [3, 2])
        with pytest.raises(ValueError):
            Passport([-1, -3], [2, 2], [3, 1])
        with pytest.raises(ValueError):
            Passport([3, 1], [2, 2], [5, -1])

    def test_passport_eq(self):
        p2 = Passport([1, 3], [2, 2], [1, 3])
        assert self.p == p2
        assert self.p == [[3, 1], [2, 2], [3, 1]]
        assert self.p == [[1, 3], [2, 2], [1, 3]]
        assert not self.p == [[2, 3], [1, 3], [3, 1]]

    def test_passport_neq(self):
        p2 = Passport([1, 3], [2, 2], [1, 3])
        assert not self.p != p2
        assert not self.p != [[3, 1], [2, 2], [3, 1]]
        assert not self.p != [[1, 3], [2, 2], [1, 3]]
        assert self.p != [[2, 3], [1, 3], [3, 1]]

    def test_passport_getitem(self):
        p2 = Passport([3, 2], [4, 1], [5])
        assert p2[0] == [2, 3]
        assert p2[1] == [1, 4]
        assert p2[2] == [5]
        assert p2[Infinity] == [5]
        assert p2[0:1] == [[2, 3]]
        assert p2[1:2] == [[1, 4]]
        assert p2[2:3] == [[5]]
        assert p2[2:Infinity] == []
        assert p2[0:2] == [[2, 3], [1, 4]]
        assert p2[1:3] == [[1, 4], [5]]
        assert p2[1:Infinity] == [[1, 4]]
        assert p2[0:Infinity] == [[2, 3], [1, 4]]
        assert p2[1:] == [[1, 4], [5]]
        assert p2[0:] == [[2, 3], [1, 4], [5]]
        assert p2[0::2] == [[2, 3], [5]]
        assert p2[Infinity::-1] == [[5], [1, 4], [2, 3]]
        with pytest.raises(IndexError):
            p2[4]

    def test_passport_constellations(self):
        p = Passport([5, 1], [4, 2], [4, 2])
        from_lmfdb = [
            Constellation3(
                "(1,5,3,6,4)", "(1,2,3,4)(5,6)", "(1,3,6,2)(4,5)"
            ).canonical(),
            Constellation3(
                "(1,5,2,6,4)", "(1,2,3,4)(5,6)", "(1,3,2,6)(4,5)"
            ).canonical(),
            Constellation3(
                "(1,5,4,2,6)", "(1,2,3,4)(5,6)", "(1,5,4,6)(2,3)"
            ).canonical(),
            Constellation3(
                "(1,2,3,6,4)", "(1,2,3,4)(5,6)", "(1,3)(2,4,5,6)"
            ).canonical(),
        ]
        assert len(list(p.constellations())) == len(from_lmfdb)
        assert set(c.canonical() for c in p.constellations()) == set(from_lmfdb)

    def test_passport_belyi_maps(self):
        # with pytest.raises(NotImplementedError):
        assert all(
            b.passport() == self.p
            for b in self.p.belyi_maps(
                5, 10**5, subs_dict={var("a0"): 0, var("c0"): 1}
            )
        )
