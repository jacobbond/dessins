import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.graphs.bipartite_graph import BipartiteGraph

from dessins import Constellation3, drawing


class TestDrawing:
    d = {
        1: [(2, 1), (2, 2), (3, 3)],
        2: [(1, 1), (1, 2)],
        3: [(1, 3), (4, 4)],
        4: [(3, 4)],
    }
    c = Constellation3("(1,2,3)", "(1,2)(3,4)")

    def test_drawing_constructor(self):
        # TODO: can this be constructed as a class variable?
        g = BipartiteGraph({v1: [v2 for v2, _ in self.d[v1]] for v1 in self.d})

        dr1 = drawing.Drawing(self.d)
        dr2 = drawing.Drawing(g)
        assert dr1 == dr2

    def test_drawing_to_constellation(self):
        dr = drawing.Drawing(self.d)
        assert dr.constellation() == self.c

    def test_constellation_to_drawing(self):
        assert self.c.drawing() == drawing.Drawing(self.d)

    # def test_positive_genus_drawings(self):
    #     assert False
