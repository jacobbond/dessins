import sys
from pathlib import Path

import pytest

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var
from sage.graphs.bipartite_graph import BipartiteGraph
from sage.groups.perm_gps.constructor import PermutationGroupElement
from sage.libs.gap.element import GapElement
from sage.libs.gap.libgap import libgap

from dessins import Constellation3, belyi_maps, dessin, drawing, passport


class TestDessin:
    x = var("x")
    b = belyi_maps.BelyiMap((x + 9) * (x + 1) ** 3 / (64 * x))
    c = Constellation3(
        libgap.eval("(1,2,3)"), libgap.eval("(1,2)(3,4)"), libgap.eval("(1,4,3)")
    )
    dr = drawing.Drawing(
        {
            0: [(1, 4)],
            1: [(0, 4), (2, 3)],
            2: [(1, 3), (3, 1), (3, 2)],
            3: [(2, 1), (2, 2)],
        }
    )

    d1 = dessin.Dessin(b)
    d2 = dessin.Dessin(c)
    d3 = dessin.Dessin(dr)
    group1 = [d1, d2, d3]

    def test_dessin_constructor(self):
        assert self.group1[0]._belyi._map == self.b._map
        assert self.group1[1]._constellation.constellation() == self.c.constellation()
        assert self.group1[2]._drawing._dict == self.dr._dict
        assert all(di._genus == 0 for di in self.group1)
        assert all(di._degree == 4 for di in self.group1)

        with pytest.raises(TypeError):
            dessin.Dessin(["(1,2,3)", "(1,2)(3,4)"])

    def test_dessin_eq(self):
        assert self.d1 == self.d2
        assert self.d2 == self.d1
        assert self.d2 == self.d3
        assert self.d3 == self.d2
        assert self.d1 == self.d3
        assert self.d3 == self.d1

    def test_dessin_repr(self):
        assert (
            repr(self.d1)
            == "Dessin with Belyi map defined by (1/64*x^4 + 3/16*x^3 + 15/32*x^2 + 7/16*x + 9/64)/x on Projective Space of dimension 1 over Rational Field"
        )
        assert (
            repr(self.d2) == "Dessin with constellation ((1,2,3), (1,2)(3,4), (1,4,3))"
        )
        assert (
            repr(self.d3)
            == "Dessin with drawing {0: [(1, 4)], 1: [(0, 4), (2, 3)], 2: [(1, 3), (3, 1), (3, 2)], 3: [(2, 1), (2, 2)]}"
        )

    def test_dessin_passport(self):
        p = passport.Passport([3, 1], [2, 2], [3, 1])

        assert all(di.passport() == p for di in self.group1)

    def test_dessin_constellation(self):
        canonical = Constellation3("(2,3,4)", "(1,2)(3,4)", "(1,2,3)")

        assert all(di.constellation().canonical() == canonical for di in self.group1)

        # t = var("t")
        # b4 = belyi_maps.BelyiMap(
        #     4 * t / 9, curve=t**2 + (2 * x**2 - 4 * x + 9) * t + x**4
        # )
        # d4 = dessin.Dessin(b4)
        # with pytest.raises(ValueError):
        #     d4.constellation()
        d5 = dessin.Dessin(self.c)
        del d5._constellation
        with pytest.raises(AttributeError):
            d5.constellation()

    def test_dessin_drawing(self):
        assert all(
            di.drawing()._graph.is_isomorphic(
                BipartiteGraph({0: [1], 1: [0, 2], 2: [1, 3, 3], 3: [2, 2]})
            )
            for di in self.group1
        )

    def test_dessin_belyi(self):
        assert self.d1._belyi._map == self.b._map

        with pytest.raises(ValueError):
            self.d2.belyi_map()

        belyi = self.d2.belyi_map(
            d=5, h=10**2, subs_dict={var("a0"): 1, var("c0"): 0}
        )
        assert belyi.constellation() == self.b.constellation()
        assert belyi.function() == self.b.function()

        d4 = dessin.Dessin(Constellation3("(1,2,3,4)", "(1,3,4,2)"))
        with pytest.raises(NotImplementedError):
            d4.belyi_map()

    def test_dessin_monodromy_group(self):
        assert all(
            di.monodromy_group().IdGroup() == libgap.eval("[ 12, 3 ]")
            for di in self.group1
        )

        d4 = dessin.Dessin(
            Constellation3(
                PermutationGroupElement("(1,2,3)"),
                PermutationGroupElement("(1,2)(3,4)"),
            )
        )
        assert isinstance(d4.monodromy_group(), GapElement)

    def test_dessin_combinatorial_orbit(self):
        orbit = [
            Constellation3(
                libgap.eval("(1,2,3)"),
                libgap.eval("(1,2)(3,4)"),
            ).canonical()
        ]

        assert all(
            set([c.canonical() for c in di.combinatorial_orbit()]) == set(orbit)
            for di in self.group1
        )

    def test_dessin_degree(self):
        assert self.d1.degree == 4
        assert self.d2.degree == 4
        assert self.d3.degree == 4

        c4 = Constellation3("(1,2,3,8)(4,5,6,7)", "(1,4,3,6)(2,5,8,7)")
        assert dessin.Dessin(c4).degree == 8

    def test_dessin_genus(self):
        assert self.d1.genus == 0
        assert self.d2.genus == 0
        assert self.d3.genus == 0

        c4 = Constellation3("(1,6,2,3,4,5)", "(1,3,6,4,2,5)")
        assert dessin.Dessin(c4).genus == 2


# class TestAgainstLMFDBData:
#     try:
#         requests.get(
#             f"https://beta.lmfdb.org/api/belyi_passports/?_format=json&_offset=0"
#         )

#         connected = True

#         passports = sum(
#             (
#                 requests.get(
#                     f"https://beta.lmfdb.org/api/belyi_passports/?_format=json&_offset={str(idx)}"
#                 ).json()["data"]
#                 for idx in range(0, 1100, 100)
#             ),
#             start=[],
#         )

#     except requests.exceptions.ConnectionError:
#         connected = False
#     # passports0 = requests.get(
#     #    "https://beta.lmfdb.org/api/belyi_passports/?_format=json&_offset=0"
#     # ).json()["data"]
#     # passports1 = requests.get(
#     #    "https://beta.lmfdb.org/api/belyi_passports/?_format=json&_offset=100"
#     # ).json()["data"]

#     def test_passports(self):
#         assert self.connected
#         for passport in self.passports:
#             p = Passport(passport["lambdas"])
#             assert p.genus == passport["g"]
#             assert p.degree == passport["deg"]
#             assert p.passport == [sorted(t) for t in passport["lambdas"]]
#             # assert p.num_constellations() == passport["pass_size"]

#             # TransitiveIdentification won't work:
#             if passport["group"] == "1T1":
#                 continue
#             lmfdb_constellations = [
#                 Constellation3(*[PermutationGroupElement(perm) for perm in c])
#                 for c in passport["triples"]
#             ]
#             assert {
#                 c.canonical()
#                 for c in p.constellations()
#                 if f"{c.monodromy_group().NrMovedPoints()}T{c.monodromy_group().TransitiveIdentification()}"
#                 == passport["group"]
#             } == {c.canonical() for c in lmfdb_constellations}
