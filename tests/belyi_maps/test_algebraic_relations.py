import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.misc.functional import sqrt

from dessins.belyi_maps import algebraic_relation as ar


class TestAlgebraicApproximation:
    def test_alg_rel(self):
        rel = ar.AlgebraicApproximation(sqrt(5).n(250), 250)
        assert rel.algebraic_relation(5, 250)(sqrt(5)) == 0

        # a bug where x^2 is returned (see https://gitlab.com/jacobbond/mydessins/-/issues/24)
        rel = ar.algebraic_relation(-50000, 200, 6, 100)
        assert rel is None
