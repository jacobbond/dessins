# existing plotting using preimages (no u(t) modifier) fails for
#  beta(gamma) where beta is defined by
# p = d.Passport([4, 1], [4, 1], [2, 2, 1])
# belyi = p.belyi_maps(d=5, h=500, subs_dict={var("a0"): 0, var("a1"): -1})[0]
# beta = [
#     b
#     for b in belyi.conjugates()
#     if b.embedding().real() < 0 and b.embedding().imag() > 0
# ][0]
# and gamma = BelyiMap(3 * x**2 - 2 * x**3)

# plotting fails for because Infinity is on the path, so finding
# neighbors fails as points towards infinity become further spaced
# xi = d.BelyiMap(27 * x**2 * (x - 1) ** 2 / (4 * (x**2 - x + 1) ** 3))
