import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

import numpy as np
from sage.calculus.var import var
from sage.rings.imaginary_unit import I

import dessins as d


def edges_to_array(edges):
    to_array = [[[t, complex(pt[0])] for t, pt in edge] for edge in edges]
    return np.array(to_array)


class TestLiftToPreimages:
    x = var("x")
    gamma = d.BelyiMap(3 * x**2 - 2 * x**3)

    def test_repeated_points(self):
        # gamma has a repeated root at x=1
        lifting = edges_to_array(
            d.belyi_maps.lifting._lift_to_preimages(
                self.gamma, var("t"), [1, 5], pts_per_path=10, prec=53
            )
        )
        actual = np.array(
            [
                [
                    (1, -0.500000000000000),
                    [1.4, -0.580104744817134500],
                    [1.7999999999999998, -0.647389747159635100],
                    [2.2, -0.706124575001791700],
                    [2.6, -0.758658592698784500],
                    [3.0, -0.806443932358772400],
                    [3.4, -0.850450276370117400],
                    [3.8, -0.891362039808875800],
                    [4.2, -0.929682910596852900],
                    [4.6, -0.965795895438756900],
                    [5.0, -1.00000000000000],
                ],
                [
                    (1, 1.00000000000000),
                    [1.4, 1.0400523724085670 - 0.35351053001624360j],
                    [
                        1.7999999999999998,
                        1.07369487357981800 - 0.487213940600316300j,
                    ],
                    [2.2, 1.1030622875008960 - 0.58399689024637760j],
                    [2.6, 1.12932929634939200 - 0.661941152770292300j],
                    [3.0, 1.1532219661793860 - 0.72807747616430870j],
                    [3.4, 1.17522513818505900 - 0.785994250431321400j],
                    [3.8, 1.195681019904438 - 0.8378044189223620j],
                    [4.2, 1.21484145529842600 - 0.884870000982922100j],
                    [4.6, 1.2328979477193780 - 0.92812617962959670j],
                    [5.0, 1.25000000000000000 - 0.968245836551854200j],
                ],
                [
                    (1, 1.00000000000000),
                    [1.4, 1.0400523724085670 + 0.35351053001624360j],
                    [
                        1.7999999999999998,
                        1.07369487357981800 + 0.487213940600316300j,
                    ],
                    [2.2, 1.1030622875008960 + 0.58399689024637760j],
                    [2.6, 1.12932929634939200 + 0.661941152770292300j],
                    [3.0, 1.1532219661793860 + 0.72807747616430870j],
                    [3.4, 1.17522513818505900 + 0.785994250431321400j],
                    [3.8, 1.195681019904438 + 0.8378044189223620j],
                    [4.2, 1.21484145529842600 + 0.884870000982922100j],
                    [4.6, 1.2328979477193780 + 0.92812617962959670j],
                    [5.0, 1.25000000000000000 + 0.968245836551854200j],
                ],
            ],
            dtype=np.complex128,
        )

        assert np.allclose(lifting, actual)


class TestPath:
    p = d.Path(0, [0, 0])
    for idx in range(1, 11):
        p.append(idx / 10, [idx / 10 + idx * I / 10, idx / 10 - idx * I / 10])

    def test_path_initialization(self):
        assert self.p.start_time == 0
        assert self.p.end_time == 1
        assert np.all(self.p.start_point == np.array([0, 0]))
        assert np.all(self.p.end_point == np.array([1 + I, 1 - I]))
        assert np.all(
            self.p.times == np.array([idx / 10 for idx in range(11)], dtype=np.float64)
        )
        assert np.all(
            self.p.points
            == np.array([[idx * (1 + I) / 10, idx * (1 - I) / 10] for idx in range(11)])
        )

        # new_p = d.Path(0, [0, 0], length=20)
        # assert new_p.times.shape == (20,)
        # assert new_p.points.shape == (20, 2)

    def test_nearest_to_path_end(self):
        points1 = np.array([[idx * 0.1, idx * 0.1] for idx in range(11)])
        points2 = np.array([[-0.1 * idx, -0.1 * idx] for idx in range(11)])
        assert np.all(self.p.nearest_to_path_end(points1) == np.array([1, 1]))
        assert np.all(self.p.nearest_to_path_end(points2) == np.array([0, 0]))

    def test_path_getitem(self):
        assert self.p[3][0] == 3 / 10
        assert np.all(self.p[3][1] == np.array([3 * (1 + I) / 10, 3 * (1 - I) / 10]))
        assert self.p[7][0] == 7 / 10
        assert np.all(self.p[7][1] == np.array([7 * (1 + I) / 10, 7 * (1 - I) / 10]))

    def test_path_setitem(self):
        self.p[0] = (-1, np.array([-1 + I, -1 - I]))
        assert self.p.start_time == -1
        assert np.all(self.p.start_point == np.array([-1 + I, -1 - I]))
        assert self.p.times[0] == -1
        assert np.all(self.p.points[0, :] == np.array([-1 + I, -1 - I]))
        self.p[-1] = (2, np.array([2 + I, 2 - I]))
        assert self.p.end_time == 2
        assert np.all(self.p.end_point == np.array([2 + I, 2 - I]))
        assert self.p.times[-1] == 2
        assert np.all(self.p.points[-1, :] == np.array([2 + I, 2 - I]))
