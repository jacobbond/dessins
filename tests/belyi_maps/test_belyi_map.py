import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var
from sage.misc.sage_eval import sage_eval
from sage.rings.fraction_field import FractionField
from sage.rings.function_field.constructor import FunctionField
from sage.rings.number_field.number_field import NumberField
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.rational_field import QQ

from dessins import belyi_maps

# test _compute_edges on xi = d.BelyiMap(27*x**2*(x-1)**2/(4*(x**2-x+1)**3))


class TestBelyiMap:
    def test_belyi_map_constructor(self):
        from sage.schemes.elliptic_curves.constructor import EllipticCurve
        from sage.schemes.projective.projective_space import ProjectiveSpace

        x = var("x")
        y = var("y")

        b0_dict = {
            "_map_vars": PolynomialRing(QQ, "x").gens(),
            "_map": -1 / 64 * (x - 8) * x**3 / (x + 1),
            "_base_field": QQ,
            "_base_field_gen": QQ(1),
            "_embedding": QQ.hom(QQ),
            "_curve": ProjectiveSpace(QQ, 1),
            "_curve_poly": None,
            "_curve_vars": tuple([]),
            "_genus0": True,
            "_map_parent": FractionField(PolynomialRing(QQ, "x")),
            "_curve_parent": PolynomialRing(QQ, []),
            "_belyi_parent": FractionField(PolynomialRing(QQ, "x")),
        }
        b0 = belyi_maps.BelyiMap(-1 / 64 * (x - 8) * x**3 / (x + 1))
        assert b0_dict == b0.__dict__
        b0 = belyi_maps.BelyiMap(-1 / 64 * (x - 8) * x**3 / (x + 1), verify=True)

        b1_dict = {
            "_map_vars": PolynomialRing(QQ, "x,y").gens()[:1],
            "_map": x**2 / (x**2 - 1),
            "_base_field": QQ,
            "_base_field_gen": QQ(1),
            "_embedding": QQ.hom(QQ),
            "_curve": EllipticCurve([0, 0, 0, -1, 0]),
            "_curve_poly": y**2 - x**3 + x,
            "_curve_vars": PolynomialRing(QQ, "x,y").gens(),
            "_genus0": False,
            "_map_parent": FractionField(PolynomialRing(QQ, "x")),
            "_curve_parent": PolynomialRing(QQ, "x,y"),
            "_belyi_parent": FractionField(PolynomialRing(QQ, "x,y")),
        }
        b1 = belyi_maps.BelyiMap(x**2 / (x**2 - 1), "y**2=x**3-x")
        assert b1_dict == b1.__dict__
        b1 = belyi_maps.BelyiMap(x**2 / (x**2 - 1), y**2 - x**3 + x)
        assert b1_dict == b1.__dict__
        b1 = belyi_maps.BelyiMap(x**2 / (x**2 - 1), EllipticCurve([0, 0, 0, -1, 0]))
        assert b1_dict == b1.__dict__
        b1 = belyi_maps.BelyiMap(
            x**2 / (x**2 - 1), EllipticCurve([0, 0, 0, -1, 0]), QQ
        )
        assert b1_dict == b1.__dict__
        b1 = belyi_maps.BelyiMap(x**2 / (x**2 - 1), y**2 - x**3 + x, QQ)
        assert b1_dict == b1.__dict__

        R = PolynomialRing(QQ, "T")
        K = NumberField(R([1, 0, 1]), "nu")
        nu = K.gen()
        # S = PolynomialRing(K, "x")
        # x = S.gen()
        K0 = FunctionField(K, "x")
        x = K0.gen()
        R = PolynomialRing(K0, "y")
        y = R.gen()
        b2_curve_str = "y^2=x^6+4/5*x^5+1/250*(-234*nu+3)*x^4+1/1250*(477*nu+391)*x^3+1/250000*(-51084*nu-16187)*x^2+1/5000*(117*nu+249)*x+1/125000*(-423*nu-576)"
        b2_map_str = "(1/1445*(-297*nu+304)*x^4+1/1445*(-297*nu+304)*x^3+1/170*(-27*nu-11)*x^2+1/144500*(-4149*nu-132)*x+1/115600*(279*nu-198))/(x^7+1/850*(-1323*nu+56)*x^6+1/722500*(70056*nu-119217)*x^5+1/1445000*(-751653*nu+107296)*x^4+1/2890000*(190512*nu-221669)*x^3+1/144500000*(-9426249*nu-152712)*x^2+1/289000000*(1785672*nu-944811)*x+1/72250000000*(-167746491*nu-69210288))*y+(1/1445*(-297*nu+304)*x^7+1/7225*(-2079*nu+2128)*x^6+1/722500*(-234297*nu-66696)*x^5+1/722500*(-96957*nu-15701)*x^4+1/2890000*(39501*nu-40432)*x^3+1/72250000*(-2294901*nu-333018)*x^2+1/3400000*(10773*nu-4536)*x+1/36125000000*(-84194883*nu-36038169))/(x^7+1/850*(-1323*nu+56)*x^6+1/722500*(70056*nu-119217)*x^5+1/1445000*(-751653*nu+107296)*x^4+1/2890000*(190512*nu-221669)*x^3+1/144500000*(-9426249*nu-152712)*x^2+1/289000000*(1785672*nu-944811)*x+1/72250000000*(-167746491*nu-69210288))"
        # this used to raise a ValueError because there was no supplied embedding, but now if
        # the map can be coerced into CC, BelyiMap will use CC.hom(CC) as the embedding
        # with pytest.raises(ValueError):
        #     belyi_maps.BelyiMap(
        #         sage_eval(b2_map_str, locals={"nu": nu, "x": x, "y": y}),
        #         b2_curve_str,
        #     )
        belyi_maps.BelyiMap(
            sage_eval(b2_map_str, locals={"nu": nu, "x": x, "y": y}),
            b2_curve_str,
            embedding=1j,
        )
