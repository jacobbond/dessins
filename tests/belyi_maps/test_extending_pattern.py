import sys
from pathlib import Path

import pytest

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

# check if this is being imported by a Sage or Python script
if ".py" in sys.argv[0] or ".sage" in sys.argv[0] or "pytest" in sys.argv[0]:
    import sage.all  # noqa: F401

from sage.calculus.var import var

from dessins import BelyiMap
from dessins.belyi_maps.extending_pattern import extending_pattern
from dessins.belyi_maps.monodromy import ID, A, B


class TestExtendingPattern:
    x = var("x")
    mu = ((x - 1) ** 2 / (-2 * x + 1)).function(x)
    alpha = BelyiMap(((1 - mu(x)) ** 2))

    # from https://arxiv.org/abs/math/0304489
    f1 = -27 * (x**3 - x**2) / 4
    f2 = -2 * x**3 + 3 * x**2
    f3 = (x**3 + 3 * x**2) / 4
    f4 = 27 * x**2 * (x - 1) / ((3 * x - 1) ** 3)
    f5 = x**2 * (x - 1) / (x - 4 / 3) ** 3

    b1, b2, b3, b4, b5 = [BelyiMap(f) for f in [f1, f2, f3, f4, f5]]

    xi = BelyiMap((27 * x**2 * (x - 1) ** 2 / (4 * (x**2 - x + 1) ** 3)))

    beta = BelyiMap(x**3)

    # fix an edge labelling for beta:
    for pt, label in beta.edge_labelling().items():
        if pt[0].imag() > 0:
            beta.edge_labelling()[pt] = 1
        elif pt[0].imag() < 0:
            beta.edge_labelling()[pt] = 2
        else:
            beta.edge_labelling()[pt] = 0

    # fix edge labellings for b_i:
    for b_i in [b1, b2, b3, b4, b5, xi]:
        edge_pts = list(sorted(b_i.edge_labelling().keys()))
        for pt, label in b_i.edge_labelling().items():
            b_i.edge_labelling()[pt] = edge_pts.index(pt)

    beta_monodromy = beta.constellation()
    gamma = BelyiMap(3 * x**2 - 2 * x**3)

    def test_extending_pattern(self):
        assert list(extending_pattern(self.alpha)) == [
            [A, ID, ID, ID],
            [ID, ID, B, ID],
        ]

        beta_pattern = extending_pattern(self.beta)
        for pt, label in self.beta.edge_labelling().items():
            if pt[0].imag() > 0:
                assert beta_pattern[0][label] == A
                assert beta_pattern[1][label] == ID
            elif pt[0].imag() < 0:
                assert beta_pattern[0][label] == ID
                assert beta_pattern[1][label] == ID
            else:
                assert beta_pattern[0][label] == ID
                assert beta_pattern[1][label] == B

        assert list(extending_pattern(self.b1)) == [[A, ID, B], [ID, ID, ID]]
        assert list(extending_pattern(self.b2)) == [[A, ID, ID], [ID, B, ID]]
        assert list(extending_pattern(self.b3)) == [[ID, A, ID], [ID, ID, B]]
        assert list(extending_pattern(self.b4)) == [
            [A, ID, B],
            [ID, ID, B**-1 * A**-1],
        ]
        assert list(extending_pattern(self.b5)) == [
            [A, ID, B],
            [B**-1 * A**-1, ID, ID],
        ]

        assert list(extending_pattern(self.xi)) == [
            [ID, A, ID, B, ID, B**-1 * A**-1],
            [ID] * 6,
        ]

    def test_extending_pattern_algorithms_equivalent(self):
        assert extending_pattern(
            self.alpha, algorithm="preimages", cache=False
        ) == extending_pattern(self.alpha, algorithm="odes", cache=False)
        assert extending_pattern(
            self.beta, algorithm="preimages", cache=False
        ) == extending_pattern(self.beta, algorithm="odes", cache=False)
        assert extending_pattern(
            self.b1, algorithm="preimages", cache=False
        ) == extending_pattern(self.b1, algorithm="odes", cache=False)
        assert extending_pattern(
            self.b2, algorithm="preimages", cache=False
        ) == extending_pattern(self.b2, algorithm="odes", cache=False)
        assert extending_pattern(
            self.b3, algorithm="preimages", cache=False
        ) == extending_pattern(self.b3, algorithm="odes", cache=False)
        assert extending_pattern(
            self.b4, algorithm="preimages", cache=False
        ) == extending_pattern(self.b4, algorithm="odes", cache=False)
        assert extending_pattern(
            self.b5, algorithm="preimages", cache=False
        ) == extending_pattern(self.b5, algorithm="odes", cache=False)
        assert extending_pattern(
            self.xi, algorithm="preimages", cache=False
        ) == extending_pattern(self.xi, algorithm="odes", cache=False)

    def test_not_dynamical(self):
        x = var("x")
        with pytest.raises(TypeError):
            extending_pattern(BelyiMap(-1 / 64 * (x - 8) * x**3 / (x + 1)))

    def test_get_item_out_of_bounds(self):
        with pytest.raises(ValueError):
            extending_pattern(self.alpha, cache=False)[2]

    def test_repr(self):
        assert (
            repr(extending_pattern(self.alpha, cache=False))
            == "([a, <identity ...>, <identity ...>, <identity ...>], [<identity ...>, <identity ...>, b, <identity ...>])"
        )
