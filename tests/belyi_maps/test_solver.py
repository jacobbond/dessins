import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var
from sage.misc.functional import n

from dessins.belyi_maps import solver


class TestAlgebraicSolver:
    def test_singular_solver(self):
        sols = solver.Solver(var("x") ** 2 - 1, var("x")).solve()
        assert len(sols) == 2
        assert (n(1),) in sols
        assert (n(-1),) in sols
