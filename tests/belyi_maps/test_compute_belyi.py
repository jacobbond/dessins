import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var

from dessins import Passport


class TestComputeBelyi:
    def test_find_k(self):
        # previously raised a division by 0 error in _find_k due to
        # cancellations not being carried out after substitution
        # of coefficients:
        Passport([4, 1, 1, 1], [4, 2, 1], [5, 2]).belyi_maps(
            d=10, h=10**3, subs_dict={var("a0"): 0, var("b0"): 1}
        )
