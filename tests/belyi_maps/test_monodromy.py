import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var

from dessins import BelyiMap, Constellation3
from dessins.belyi_maps.monodromy import monodromy


class TestMonodromy:
    x = var("x")
    mu = ((x - 1) ** 2 / (-2 * x + 1)).function(x)
    beta = BelyiMap(((1 - mu(x)) ** 2))

    nu_3 = BelyiMap((x**3 + 1) ** 2 / (x**3 - 1) ** 2)
    bT = BelyiMap(x**3 * (x**3 + 8) ** 3 / (x**6 - 20 * x**3 - 8) ** 2)

    def test_monodromy(self):
        assert monodromy(self.beta) == Constellation3("(1, 3, 2, 4)", "(3, 4)")

        # previously failed due to a Singular error, due in part to small imaginary
        # component in numerical approximation of e^(2*pi*I):
        assert (
            monodromy(self.nu_3).canonical()
            == Constellation3("(1,2)(3,4)(5,6)", "(1,3,5)(2,6,4)").canonical()
        )
        # fails due to a Singular error, seems to struggle to handle degree 12
        # Belyi maps; try with ODE algorithm:
        monodromy(self.bT, algorithm="odes")

    def test_monodromy_algorithms_equivalent(self):
        assert monodromy(self.nu_3, algorithm="preimages") == monodromy(
            self.nu_3, algorithm="odes"
        )
        assert monodromy(self.beta, algorithm="preimages") == monodromy(
            self.beta, algorithm="odes"
        )
        # assert monodromy(self.bT, algorithm="preimages") == monodromy(self.bT, algorithm="odes")
