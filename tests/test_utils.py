import sys
from math import ceil
from pathlib import Path

import pytest

sys.path.append(str(Path(__file__).parent.joinpath("../dessins").resolve()))

from sage.calculus.var import var
from sage.groups.perm_gps.permgroup_element import PermutationGroupElement
from sage.groups.perm_gps.permgroup_named import SymmetricGroup
from sage.libs.gap.libgap import libgap
from sage.rings.infinity import Infinity

import dessins


class TestCoercePerm:
    gap_perm_id = libgap.eval("()")
    sage_perm_id = PermutationGroupElement("()", SymmetricGroup(3))
    gap_perm_nonid = libgap.eval("(1,2,3)")
    sage_perm_nonid = PermutationGroupElement("(1,2,3)", SymmetricGroup(3))

    def test_coerce_gap_to_gap(self):
        coerced = dessins.utils.coerce_perm(self.gap_perm_id, "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm(self.gap_perm_nonid, "gap")
        assert coerced == self.gap_perm_nonid

    def test_coerce_sage_to_gap(self):
        coerced = dessins.utils.coerce_perm(self.sage_perm_id, "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm(self.sage_perm_nonid, "gap")
        assert coerced == self.gap_perm_nonid

    def test_coerce_gap_to_sage(self):
        coerced = dessins.utils.coerce_perm(self.gap_perm_id, "sage")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm(self.gap_perm_nonid, "sage")
        assert coerced == self.gap_perm_nonid

    def test_coerce_sage_to_sage(self):
        coerced = dessins.utils.coerce_perm(self.sage_perm_id, "sage")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm(self.sage_perm_nonid, "sage")
        assert coerced == self.gap_perm_nonid

    def test_coerce_from_string(self):
        coerced = dessins.utils.coerce_perm("(1,2,3)", "gap")
        assert coerced == self.gap_perm_nonid
        coerced = dessins.utils.coerce_perm("(1,2,3)", "sage")
        assert coerced == self.sage_perm_nonid
        coerced = dessins.utils.coerce_perm("()", "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm("()", "sage")
        assert coerced == self.sage_perm_id
        coerced = dessins.utils.coerce_perm("(1)(2)(3)(4)", "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm("(1)(2)(3)(4)", "sage")
        assert coerced == self.sage_perm_id
        coerced = dessins.utils.coerce_perm("(1,)", "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm("(1,)", "sage")
        assert coerced == self.sage_perm_id
        coerced = dessins.utils.coerce_perm("(1)", "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm("(1)", "sage")
        assert coerced == self.sage_perm_id
        coerced = dessins.utils.coerce_perm("(4)(1,2,3)", "gap")
        assert coerced == self.gap_perm_nonid
        coerced = dessins.utils.coerce_perm("(4)(1,2,3)", "sage")
        assert coerced == self.sage_perm_nonid

    def test_coerce_from_list(self):
        coerced = dessins.utils.coerce_perm([2, 3, 1], "gap")
        assert coerced == self.gap_perm_nonid
        coerced = dessins.utils.coerce_perm([2, 3, 1], "sage")
        assert coerced == self.sage_perm_nonid
        coerced = dessins.utils.coerce_perm([1, 2, 3, 4], "gap")
        assert coerced == self.gap_perm_id
        coerced = dessins.utils.coerce_perm([1, 2, 3, 4], "sage")
        assert coerced == self.sage_perm_id


class TestConsume:
    def test_consume(self):
        i1 = iter(range(10))
        dessins.utils.consume(i1)
        assert list(i1) == []
        i2 = iter(range(10))
        dessins.utils.consume(i2, 7)
        assert list(i2) == [7, 8, 9]


class TestGapRange:
    def test_gap_range(self):
        assert dessins.utils.gap_range(0, 0).sage() == [0]
        assert dessins.utils.gap_range(0, 5).sage() == list(range(6))
        assert dessins.utils.gap_range(2, -1).sage() == []
        G1 = libgap.Group(libgap.eval("(1,2,3)"))
        G2 = libgap.Group(libgap.eval("(1,3)"))
        assert G1.IsTransitive(dessins.utils.gap_range(1, 3))
        assert not G2.IsTransitive(dessins.utils.gap_range(1, 3))


class TestGenericPermutation:
    def test_generic_permutation_identity(self):
        assert dessins.utils.generic_permutation([1]) == libgap.eval("()")
        assert dessins.utils.generic_permutation([1] * 5) == libgap.eval("()")

    def test_generic_permutation_singletons(self):
        assert dessins.utils.generic_permutation([2, 3, 4, 1, 1, 1, 1]) == libgap.eval(
            "(5,6)(7,8,9)(10,11,12,13)"
        )

    def test_generic_permutation_reorder(self):
        assert dessins.utils.generic_permutation([2, 3, 4]) == libgap.eval(
            "(1,2)(3,4,5)(6,7,8,9)"
        )
        assert dessins.utils.generic_permutation([2, 4, 3]) == libgap.eval(
            "(1,2)(3,4,5,6)(7,8,9)"
        )
        assert dessins.utils.generic_permutation([3, 2, 4]) == libgap.eval(
            "(1,2,3)(4,5)(6,7,8,9)"
        )


class TestUniqueConstellationIterator:
    def test_unique_constellation_iterator(self):
        from dessins.constellations.generation import passport_to_constellations
        from dessins.utils import UniqueConstellationIterator

        c_1 = list(
            passport_to_constellations(
                [6, 2], [5, 3], [7, 1], algorithm="conjugates", unique=False
            )
        )
        c_2 = list(passport_to_constellations([6, 2], [5, 3], [7, 1]))
        assert len(c_1) > len(c_2)
        assert len(list(UniqueConstellationIterator(c_1))) == len(c_2)


class TestFindMoebius:
    # TODO: add tests that include complex numbers
    def test_find_moebius(self):
        from dessins.utils import find_moebius

        z = var("z")
        assert find_moebius([0, 1, 2], [0, 1, 2]) == z
        assert find_moebius([0, 1, 2], [1, 0, -1]) == 1 - z
        assert find_moebius([0, 1, Infinity], [0, 1, Infinity]) == z
        assert find_moebius([0, 1, Infinity], [0, Infinity, 1]) == z / (z - 1)
        assert find_moebius([0, 1, Infinity], [1, 0, Infinity]) == 1 - z
        # assert find_moebius([0, 1, Infinity], [1, Infinity, 0]) ==
        assert find_moebius([0, 1, Infinity], [Infinity, 0, 1]) == (z - 1) / z
        # assert find_moebius([0, 1, Infinity], [Infinity, 1, 0]) ==

    def test_find_moebius_permutation_invariance(self):
        import math

        from dessins.utils import find_moebius

        assert find_moebius(
            [3, 17.5, math.pi], [27.4, math.e, Infinity]
        ) == find_moebius([3, math.pi, 17.5], [27.4, Infinity, math.e])
        assert find_moebius([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius(
            [3.14159, 3, 17.5], [12, 27.4, 2.71828]
        )
        assert find_moebius([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius(
            [3.14159, 17.5, 3], [12, 2.71828, 27.4]
        )
        assert find_moebius([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius(
            [3, 17.5, 3.14159], [27.4, 2.71828, 12]
        )
        assert find_moebius([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius(
            [3, 3.14159, 17.5], [27.4, 12, 2.71828]
        )
        assert find_moebius([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius(
            [17.5, 3.14159, 3], [2.71828, 12, 27.4]
        )
        assert find_moebius([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius(
            [17.5, 3, 3.14159], [2.71828, 27.4, 12]
        )

    def test_find_moebius_input_validation(self):
        with pytest.raises(ValueError):
            dessins.utils.find_moebius([0, 1], [2, 3])


class TestFindMoebius2:
    # TODO: add tests that include complex numbers
    def test_find_moebius2(self):
        from dessins.utils import find_moebius2

        z = var("z")
        assert find_moebius2([0, 1, 2], [0, 1, 2]) == z
        assert find_moebius2([0, 1, 2], [1, 0, -1]) == 1 - z
        assert find_moebius2([0, 1, Infinity], [0, 1, Infinity]) == z
        assert find_moebius2([0, 1, Infinity], [0, Infinity, 1]) == z / (z - 1)
        assert find_moebius2([0, 1, Infinity], [1, 0, Infinity]) == 1 - z
        # assert find_moebius([0, 1, Infinity], [1, Infinity, 0]) ==
        assert find_moebius2([0, 1, Infinity], [Infinity, 0, 1]) == (z - 1) / z
        # assert find_moebius([0, 1, Infinity], [Infinity, 1, 0]) ==

    def test_find_moebius2_permutation_invariance(self):
        from dessins.utils import find_moebius2

        assert find_moebius2(
            [3, 17.5, 3.14159], [27.4, 2.71828, Infinity]
        ) == find_moebius2([3, 3.14159, 17.5], [27.4, Infinity, 2.71828])
        assert find_moebius2([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius2(
            [3.14159, 3, 17.5], [12, 27.4, 2.71828]
        )
        assert find_moebius2([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius2(
            [3.14159, 17.5, 3], [12, 2.71828, 27.4]
        )
        assert find_moebius2([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius2(
            [3, 17.5, 3.14159], [27.4, 2.71828, 12]
        )
        assert find_moebius2([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius2(
            [3, 3.14159, 17.5], [27.4, 12, 2.71828]
        )
        assert find_moebius2([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius2(
            [17.5, 3.14159, 3], [2.71828, 12, 27.4]
        )
        assert find_moebius2([3, 17.5, 3.14159], [27.4, 2.71828, 12]) == find_moebius2(
            [17.5, 3, 3.14159], [2.71828, 27.4, 12]
        )

    def test_find_moebius2_input_validation(self):
        with pytest.raises(ValueError):
            dessins.utils.find_moebius2([0, 1], [2, 3])


class TestBitsAndDigits:
    def test_digits_to_bits(self):
        with pytest.raises(ValueError):
            dessins.utils.digits_to_bits(0)
        with pytest.raises(ValueError):
            dessins.utils.digits_to_bits(-1)

        assert ceil(dessins.utils.digits_to_bits(1)) == 4
        assert ceil(dessins.utils.digits_to_bits(2)) == 7
        assert ceil(dessins.utils.digits_to_bits(3)) == 10

    def test_bits_to_digits(self):
        with pytest.raises(ValueError):
            dessins.utils.bits_to_digits(0)
        with pytest.raises(ValueError):
            dessins.utils.bits_to_digits(-1)

        assert ceil(dessins.utils.bits_to_digits(3)) == 1
        assert ceil(dessins.utils.bits_to_digits(6)) == 2
        assert ceil(dessins.utils.bits_to_digits(9)) == 3


class TestEnlist:
    def test_enlist(self):
        assert dessins.utils.enlist(1) == [1]
        assert dessins.utils.enlist([1]) == [1]
        assert dessins.utils.enlist(range(5)) == [0, 1, 2, 3, 4]
        assert dessins.utils.enlist({1: 2, 3: 4}) == [{1: 2, 3: 4}]
